--liquibase formatted sql
--This is the baseline SQL file to create the foundation schema.
--changeset authorName:create_foundation_schema context:schema splitStatements:false runOnChange:false

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.7 (Debian 13.7-1.pgdg110+1)
-- Dumped by pg_dump version 14.2

-- Started on 2023-03-14 12:55:44

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 44662)
-- Name: plims; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA plims;


--
-- TOC entry 201 (class 1259 OID 16410)
-- Name: activity_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.activity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 202 (class 1259 OID 16412)
-- Name: agent_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.agent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 264 (class 1259 OID 53002)
-- Name: role_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 270 (class 1259 OID 61218)
-- Name: auth_role; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.auth_role (
    role_id integer DEFAULT nextval('plims.role_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(245),
    description text,
    registered_by integer NOT NULL,
    registered_at timestamp without time zone DEFAULT now(),
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    is_deleted boolean DEFAULT false
);


--
-- TOC entry 269 (class 1259 OID 61198)
-- Name: task_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 271 (class 1259 OID 61383)
-- Name: auth_task; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.auth_task (
    task_id integer DEFAULT nextval('plims.task_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(245),
    description text,
    task_link text,
    is_finished boolean DEFAULT false,
    role_id integer,
    registered_by integer NOT NULL,
    registered_at timestamp without time zone DEFAULT now(),
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    is_deleted boolean DEFAULT false
);


--
-- TOC entry 203 (class 1259 OID 16414)
-- Name: user_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 204 (class 1259 OID 16416)
-- Name: auth_user; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.auth_user (
    user_id integer DEFAULT nextval('plims.user_id_seq'::regclass) NOT NULL,
    username character varying(255),
    firstname character varying(45) DEFAULT NULL::character varying,
    lastname character varying(45) DEFAULT NULL::character varying,
    auth_key character varying(32),
    password_hash character varying(255),
    password_reset_token character varying(255),
    email character varying(100) NOT NULL,
    status smallint,
    role integer,
    verification_token character varying(245),
    created_at integer,
    created_by character varying(45),
    updated_at integer,
    updated_by character varying(45),
    deleted_at integer,
    deleted_by character varying(45),
    userid character varying(255),
    photo character varying(255) DEFAULT 'avatar.png'::character varying,
    role_1 character varying(255) DEFAULT 'CIP staff'::character varying,
    role_2 character varying(255)
);


--
-- TOC entry 272 (class 1259 OID 61406)
-- Name: auth_user_by_role_institution_laboratory; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.auth_user_by_role_institution_laboratory (
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    institution_id integer,
    laboratory_id integer,
    registered_by integer NOT NULL,
    registered_at timestamp without time zone DEFAULT now(),
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    is_deleted boolean DEFAULT false
);


--
-- TOC entry 205 (class 1259 OID 16427)
-- Name: user_refresh_tokenid_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.user_refresh_tokenid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 206 (class 1259 OID 16429)
-- Name: auth_user_refresh_tokens; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.auth_user_refresh_tokens (
    user_refresh_tokenid integer DEFAULT nextval('plims.user_refresh_tokenid_seq'::regclass) NOT NULL,
    urf_userid integer NOT NULL,
    urf_token character varying(1000) NOT NULL,
    urf_ip character varying(50) NOT NULL,
    urf_user_agent character varying(1000) NOT NULL,
    urf_created timestamp with time zone DEFAULT now()
);


--
-- TOC entry 207 (class 1259 OID 16437)
-- Name: bsns_activity; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_activity (
    activity_id integer DEFAULT nextval('plims.activity_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(145),
    description character varying(245),
    details character varying(245),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    activity_type_id integer,
    activity_datasource_id integer,
    activity_property_id integer,
    activity_group_id integer
);


--
-- TOC entry 208 (class 1259 OID 16444)
-- Name: bsns_activity_by_essay; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_activity_by_essay (
    activity_id integer NOT NULL,
    essay_id integer NOT NULL,
    num_order integer,
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    is_template boolean DEFAULT false NOT NULL
);


--
-- TOC entry 209 (class 1259 OID 16448)
-- Name: bsns_agent; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_agent (
    agent_id integer DEFAULT nextval('plims.agent_id_seq'::regclass) NOT NULL,
    short_name character varying(145),
    long_name character varying(145),
    description character varying(245),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    agent_type_id integer,
    agent_group_id integer
);


--
-- TOC entry 210 (class 1259 OID 16455)
-- Name: bsns_agent_condition; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_agent_condition (
    num_order integer NOT NULL,
    essay_id integer NOT NULL,
    crop_id integer NOT NULL,
    request_type_id integer NOT NULL,
    composite_sample_type_id integer NOT NULL,
    request_process_essay_type_id integer NOT NULL,
    agent_id integer NOT NULL,
    reprocess_flag bit(1),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    repetition integer,
    code character varying(20),
    serial integer,
    is_reprocessed boolean DEFAULT true,
    laboratory_id integer NOT NULL
);


--
-- TOC entry 259 (class 1259 OID 44764)
-- Name: bsns_agent_sample_reviewer; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_agent_sample_reviewer (
    request_process_essay_id integer NOT NULL,
    composite_sample_id integer NOT NULL,
    agent_id integer NOT NULL,
    num_order integer,
    is_distributed boolean DEFAULT true NOT NULL,
    is_processing boolean DEFAULT false NOT NULL,
    is_reprocessing boolean DEFAULT false NOT NULL,
    support_master_id integer,
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    is_deleted boolean DEFAULT false NOT NULL
);


--
-- TOC entry 211 (class 1259 OID 16459)
-- Name: composite_sample_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.composite_sample_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 212 (class 1259 OID 16461)
-- Name: bsns_composite_sample; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_composite_sample (
    composite_sample_id integer DEFAULT nextval('plims.composite_sample_id_seq'::regclass) NOT NULL,
    composite_sample_type_id integer NOT NULL,
    composite_sample_group_id integer,
    crop_id integer,
    num_order integer,
    short_name character varying(50),
    material_qty integer,
    material_unit_qty integer,
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    long_name character varying(150),
    reading_data_type_id integer,
    material_group_id integer
);


--
-- TOC entry 213 (class 1259 OID 16465)
-- Name: composite_sample_group_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.composite_sample_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 214 (class 1259 OID 16467)
-- Name: bsns_composite_sample_group; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_composite_sample_group (
    composite_sample_group_id integer DEFAULT nextval('plims.composite_sample_group_id_seq'::regclass) NOT NULL,
    year_code integer,
    sequential_code integer,
    composite_sample_group_code character varying(45),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15)
);


--
-- TOC entry 260 (class 1259 OID 44814)
-- Name: consolidate_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.consolidate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 261 (class 1259 OID 44916)
-- Name: bsns_consolidated_information; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_consolidated_information (
    consolidate_id integer DEFAULT nextval('plims.consolidate_id_seq'::regclass) NOT NULL,
    request_id integer,
    num_order integer,
    general_sample_id integer,
    general_sample_name character varying(100),
    simple_sample_id integer,
    simple_sample_name character varying(100),
    indiviual_sample_id integer,
    indiviual_sample_name character varying(100),
    final_decision boolean,
    automatic_decision boolean,
    final_result boolean,
    release_status_id integer
);


--
-- TOC entry 262 (class 1259 OID 45101)
-- Name: bsns_consolidated_result; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_consolidated_result (
    request_id integer,
    composite_sample_id integer,
    essay_id integer,
    agent_id integer,
    text_result text,
    text_sample text,
    sample_order text,
    assay_class text,
    assay_name text,
    agent_name text
);


--
-- TOC entry 215 (class 1259 OID 16471)
-- Name: cost_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.cost_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 216 (class 1259 OID 16473)
-- Name: bsns_cost; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_cost (
    cost_id integer DEFAULT nextval('plims.cost_id_seq'::regclass) NOT NULL,
    code character varying(45),
    description character varying(245),
    cost_value double precision,
    finance_period integer,
    status character varying(15),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    cost_currency_id integer
);


--
-- TOC entry 217 (class 1259 OID 16477)
-- Name: crop_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.crop_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 218 (class 1259 OID 16479)
-- Name: bsns_crop; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_crop (
    crop_id integer DEFAULT nextval('plims.crop_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(45),
    description character varying(245),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    institution_id integer
);


--
-- TOC entry 219 (class 1259 OID 16483)
-- Name: essay_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.essay_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 220 (class 1259 OID 16485)
-- Name: bsns_essay; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_essay (
    essay_id integer DEFAULT nextval('plims.essay_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(145),
    description character varying(245),
    details character varying(245),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    essay_cost_id integer,
    essay_type_id integer,
    essay_location_id integer,
    abbreviation character varying(10),
    style_class text
);


--
-- TOC entry 221 (class 1259 OID 16492)
-- Name: bsns_essay_by_workflow; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_essay_by_workflow (
    essay_id integer NOT NULL,
    workflow_id integer NOT NULL,
    num_order integer,
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15)
);


--
-- TOC entry 266 (class 1259 OID 53114)
-- Name: institution_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.institution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 267 (class 1259 OID 53116)
-- Name: bsns_institution; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_institution (
    institution_id integer DEFAULT nextval('plims.institution_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(245),
    description text,
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    is_deleted boolean
);


--
-- TOC entry 263 (class 1259 OID 52991)
-- Name: laboratory_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.laboratory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 265 (class 1259 OID 53094)
-- Name: bsns_laboratory; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_laboratory (
    laboratory_id integer DEFAULT nextval('plims.laboratory_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(245),
    description text,
    request_prefix character varying(10) NOT NULL,
    institution_id integer NOT NULL,
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    is_deleted boolean
);


--
-- TOC entry 222 (class 1259 OID 16495)
-- Name: location_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 223 (class 1259 OID 16497)
-- Name: bsns_location; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_location (
    location_id integer DEFAULT nextval('plims.location_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(245),
    description character varying(245),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15)
);


--
-- TOC entry 224 (class 1259 OID 16504)
-- Name: material_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 225 (class 1259 OID 16506)
-- Name: bsns_material; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_material (
    material_id integer DEFAULT nextval('plims.material_id_seq'::regclass) NOT NULL,
    composite_sample_id integer,
    composite_sample_type_id integer,
    composite_sample_group_id integer,
    crop_id integer,
    material_type_id integer,
    material_unit_id integer,
    num_order integer,
    composite_sample_order integer,
    material_unit_qty integer,
    short_name character varying(50),
    long_name character varying(50),
    observation character varying(245),
    details character varying(245),
    origin character varying(50),
    grafting_order integer,
    material_code_a character varying(145),
    material_code_b character varying(145),
    material_code_c character varying(145),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    material_group_id integer,
    material_status_id integer,
    historical_data_version integer,
    seed_name text,
    seed_code text,
    package_label text,
    package_code text,
    germplasm_name text,
    source_database_id integer
);


--
-- TOC entry 226 (class 1259 OID 16513)
-- Name: material_group_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.material_group_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 227 (class 1259 OID 16515)
-- Name: bsns_material_group; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_material_group (
    material_group_id integer DEFAULT nextval('plims.material_group_seq'::regclass) NOT NULL,
    number_index integer,
    group_name character varying(50),
    material_qty integer,
    material_unit_qty integer,
    sample_group_name character varying(50),
    num_order_ini integer,
    material_name_ini character varying(50),
    num_order_end integer,
    material_name_end character varying(50),
    composite_sample_group_id integer,
    material_details text
);


--
-- TOC entry 258 (class 1259 OID 17539)
-- Name: bsns_multi_request_event; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_multi_request_event (
    support_master_id integer NOT NULL,
    request_id integer,
    agent_id integer,
    num_order_id integer,
    request_process_essay_status_id integer
);


--
-- TOC entry 228 (class 1259 OID 16522)
-- Name: parameter_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 229 (class 1259 OID 16524)
-- Name: bsns_parameter; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_parameter (
    parameter_id integer DEFAULT nextval('plims.parameter_id_seq'::regclass) NOT NULL,
    code character varying(45),
    singularity character varying(45),
    entity character varying(45),
    short_name character varying(45),
    long_name character varying(145),
    description character varying(245),
    class_details character varying(245),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    number_value double precision,
    text_value text,
    institution_id integer
);


--
-- TOC entry 268 (class 1259 OID 61177)
-- Name: bsns_parameter_by_institution_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.bsns_parameter_by_institution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 230 (class 1259 OID 16531)
-- Name: reading_data_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.reading_data_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 231 (class 1259 OID 16533)
-- Name: bsns_reading_data; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_reading_data (
    request_id integer,
    num_order_id integer,
    essay_id integer,
    activity_id integer,
    agent_id integer,
    symptom_id integer,
    support_order_id integer,
    support_cell_position integer,
    grafting_number_id integer,
    historical_data_version integer,
    composite_sample_group_id integer,
    composite_sample_id integer,
    composite_sample_type_id integer,
    crop_id integer,
    workflow_id integer,
    reading_data_section_id integer,
    reading_data_type_id integer,
    observation text,
    material_evidence character varying(500),
    auxiliary_result character varying(245),
    number_result double precision,
    text_result character varying(245),
    primary_order_num integer,
    secondary_order_num integer,
    tertiary_order_num integer,
    registered_by integer,
    registered_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    number_of_seeds_tested integer,
    support_master_id integer,
    reading_data_id integer DEFAULT nextval('plims.reading_data_seq'::regclass) NOT NULL,
    request_process_essay_id integer,
    request_process_essay_agent_id integer,
    request_process_essay_activity_id integer,
    request_process_essay_activity_sample_id integer,
    support_id integer,
    is_deleted boolean DEFAULT false NOT NULL,
    reprocess boolean
);


--
-- TOC entry 232 (class 1259 OID 16541)
-- Name: request_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 233 (class 1259 OID 16543)
-- Name: bsns_request; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_request (
    request_id integer DEFAULT nextval('plims.request_id_seq'::regclass) NOT NULL,
    year_code integer,
    sequential_code integer,
    request_code character varying(45),
    details character varying(245),
    diagnostic character varying(245),
    agent_qty integer,
    material_qty integer,
    total_cost double precision,
    request_type_id integer,
    request_status_id integer,
    request_payment_id integer,
    pay_number character varying(45),
    requested_user_id integer,
    owner_user_id integer,
    registered_by integer,
    registered_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    sample_qty integer,
    material_unit_qty integer,
    is_deleted boolean DEFAULT false NOT NULL,
    laboratory_id integer,
    institution_id integer
);


--
-- TOC entry 234 (class 1259 OID 16551)
-- Name: bsns_request_detail; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_request_detail (
    request_id integer NOT NULL,
    bar_id character varying(255),
    st_id character varying(255),
    plims_id character varying(255),
    request_program character varying(255),
    request_sub_program character varying(255),
    identification_material character varying(255),
    seed_distribution character varying(255),
    seed_sent_by character varying(255),
    icc_seed_health_testing character varying(255),
    certification character varying(255),
    cost_shipment_paid_by character varying(255),
    seed_purpose character varying(255),
    shipping_date_desired character varying(255),
    consignee_name character varying(255),
    request_position character varying(255),
    request_department character varying(255),
    request_institution character varying(255),
    request_street character varying(255),
    request_city character varying(255),
    request_country character varying(255),
    request_phone character varying(255),
    request_email character varying(255),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    requested_user integer,
    consignee_user integer,
    recipient_name character varying(255),
    recipient_institution character varying(255),
    recipient_address character varying(255),
    recipient_country character varying(255),
    recipient_program character varying(255),
    recipient_account character varying(255),
    sender_name character varying(255),
    sender_institution character varying(255),
    sender_address character varying(255),
    sender_country character varying(255),
    sender_program character varying(255),
    sender_account character varying(255),
    international_phytosanitary_certificate character varying(255),
    quarantine_custody character varying(255),
    requirements_sheet_folio character varying(255),
    total_shipping_weight character varying(255),
    crop_id integer
);


--
-- TOC entry 235 (class 1259 OID 16557)
-- Name: request_documentation_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.request_documentation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 236 (class 1259 OID 16559)
-- Name: bsns_request_documentation; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_request_documentation (
    request_documentation_id integer DEFAULT nextval('plims.request_documentation_id_seq'::regclass) NOT NULL,
    path_documentation character varying(900),
    document_name character varying(450),
    document_extension character varying(450),
    request_id integer,
    file_type_id integer,
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    num_order_id integer,
    essay_id integer,
    support_master_id integer,
    support_order_id integer,
    activity_id integer,
    agent_id integer
);


--
-- TOC entry 237 (class 1259 OID 16566)
-- Name: bsns_request_process; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_request_process (
    request_id integer NOT NULL,
    num_order_id integer NOT NULL,
    crop_id integer,
    workflow_id integer,
    composite_sample_group_id integer,
    start_date timestamp without time zone,
    finish_date timestamp without time zone,
    check_date timestamp without time zone,
    essay_qty integer,
    agent_qty integer,
    material_qty integer,
    sample_qty integer,
    essay_detail text,
    agent_detail text,
    material_detail text,
    sample_detail text,
    payment_percentage double precision,
    sub_total_cost double precision,
    request_process_status_id integer,
    request_process_material_id integer,
    request_process_payment_id integer,
    registered_by integer,
    registered_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    child_crop character varying(360),
    child_crop_other character varying(360),
    request_process_other character varying(45),
    registration_out_of_the_system character varying(45),
    material_unit_qty integer,
    start_by integer,
    finish_by integer,
    check_by integer,
    is_deleted boolean DEFAULT false NOT NULL
);


--
-- TOC entry 238 (class 1259 OID 16573)
-- Name: request_process_essay_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.request_process_essay_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 239 (class 1259 OID 16575)
-- Name: bsns_request_process_essay; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_request_process_essay (
    request_id integer NOT NULL,
    num_order_id integer NOT NULL,
    essay_id integer NOT NULL,
    composite_sample_type_id integer NOT NULL,
    crop_id integer,
    workflow_id integer,
    start_date timestamp without time zone,
    finish_date timestamp without time zone,
    check_date timestamp without time zone,
    agent_qty integer,
    agent_detail text,
    sub_total_cost_essay double precision,
    registered_by integer,
    registered_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    num_order integer,
    sample_qty integer,
    sample_detail text,
    request_process_essay_status_id integer,
    request_process_essay_type_id integer,
    activity_qty integer,
    activity_detail text,
    observation text,
    request_process_essay_root_id integer,
    support_master_id integer,
    request_process_essay_id integer DEFAULT nextval('plims.request_process_essay_seq'::regclass) NOT NULL,
    start_by integer,
    finish_by integer,
    check_by integer,
    is_deleted boolean DEFAULT false NOT NULL,
    code character varying(20),
    code_root character varying(20)
);


--
-- TOC entry 240 (class 1259 OID 16583)
-- Name: request_process_essay_activity_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.request_process_essay_activity_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 241 (class 1259 OID 16585)
-- Name: bsns_request_process_essay_activity; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_request_process_essay_activity (
    request_id integer NOT NULL,
    num_order_id integer NOT NULL,
    essay_id integer NOT NULL,
    activity_id integer NOT NULL,
    composite_sample_type_id integer NOT NULL,
    crop_id integer,
    workflow_id integer,
    request_process_essay_activity_status_id integer,
    num_order integer,
    start_date timestamp without time zone,
    finish_date timestamp without time zone,
    check_date timestamp without time zone,
    control_location_id integer,
    control_user_id integer,
    registered_by integer,
    registered_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    request_process_essay_activity_id integer DEFAULT nextval('plims.request_process_essay_activity_seq'::regclass) NOT NULL,
    request_process_essay_id integer,
    start_by integer,
    finish_by integer,
    check_by integer,
    is_deleted boolean DEFAULT false NOT NULL
);


--
-- TOC entry 242 (class 1259 OID 16590)
-- Name: request_process_essay_sample_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.request_process_essay_sample_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 243 (class 1259 OID 16592)
-- Name: bsns_request_process_essay_activity_sample; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_request_process_essay_activity_sample (
    request_process_essay_activity_sample_id integer DEFAULT nextval('plims.request_process_essay_sample_seq'::regclass) NOT NULL,
    request_id integer,
    num_order_id integer,
    essay_id integer,
    composite_sample_id integer,
    composite_sample_type_id integer,
    crop_id integer,
    workflow_id integer,
    num_order integer,
    request_process_essay_activity_sample_status_id integer,
    deleted_by integer,
    deleted_at timestamp without time zone,
    available boolean DEFAULT true NOT NULL,
    text_value character varying(300),
    number_of_seeds integer,
    description_value character varying(100),
    request_process_essay_activity_id integer,
    registered_by integer,
    registered_at timestamp without time zone,
    activity_id integer,
    composite_sample_group_id integer,
    start_by integer,
    finish_by integer,
    check_by integer,
    start_date timestamp without time zone,
    finish_date timestamp without time zone,
    check_date timestamp without time zone,
    historical_data_version integer,
    is_deleted boolean DEFAULT false NOT NULL,
    request_process_essay_id integer
);


--
-- TOC entry 244 (class 1259 OID 16598)
-- Name: request_process_essay_agent_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.request_process_essay_agent_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 245 (class 1259 OID 16600)
-- Name: bsns_request_process_essay_agent; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_request_process_essay_agent (
    request_id integer NOT NULL,
    num_order_id integer NOT NULL,
    essay_id integer NOT NULL,
    agent_id integer NOT NULL,
    composite_sample_type_id integer NOT NULL,
    crop_id integer,
    workflow_id integer,
    num_order integer,
    init_position integer,
    end_position integer,
    init_content integer,
    end_content integer,
    registered_by integer,
    registered_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    repetition integer,
    request_process_essay_agent_id integer DEFAULT nextval('plims.request_process_essay_agent_seq'::regclass) NOT NULL,
    request_process_essay_id integer,
    is_deleted boolean DEFAULT false NOT NULL
);


--
-- TOC entry 246 (class 1259 OID 16605)
-- Name: bsns_status; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_status (
    request_id integer NOT NULL,
    request_status_id integer NOT NULL,
    number_index integer,
    status_date timestamp without time zone,
    status_by integer,
    observation character varying(500)
);


--
-- TOC entry 247 (class 1259 OID 16611)
-- Name: support_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.support_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 248 (class 1259 OID 16613)
-- Name: bsns_support; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_support (
    support_order_id integer NOT NULL,
    request_id integer NOT NULL,
    num_order_id integer NOT NULL,
    essay_id integer NOT NULL,
    activity_id integer NOT NULL,
    composite_sample_type_id integer NOT NULL,
    crop_id integer,
    workflow_id integer,
    support_type_id integer,
    support_columns integer,
    support_rows integer,
    support_code character varying(45),
    total_slots integer,
    used_slots integer,
    available_slots integer,
    ctrl_qty integer,
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    support_id integer DEFAULT nextval('plims.support_seq'::regclass) NOT NULL,
    request_process_essay_activity_id integer,
    request_process_essay_id integer
);


--
-- TOC entry 249 (class 1259 OID 16617)
-- Name: support_master_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.support_master_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 250 (class 1259 OID 16619)
-- Name: bsns_support_master; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_support_master (
    support_master_id integer DEFAULT nextval('plims.support_master_id_seq'::regclass) NOT NULL,
    support_code character varying(150),
    detail character varying(900),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15)
);


--
-- TOC entry 251 (class 1259 OID 16626)
-- Name: symptom_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.symptom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 252 (class 1259 OID 16628)
-- Name: bsns_symptom; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_symptom (
    symptom_id integer DEFAULT nextval('plims.symptom_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(145),
    description character varying(245),
    details character varying(245),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15)
);


--
-- TOC entry 253 (class 1259 OID 16635)
-- Name: workflow_id_seq; Type: SEQUENCE; Schema: plims; Owner: -
--

CREATE SEQUENCE plims.workflow_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 254 (class 1259 OID 16637)
-- Name: bsns_workflow; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_workflow (
    workflow_id integer DEFAULT nextval('plims.workflow_id_seq'::regclass) NOT NULL,
    short_name character varying(45),
    long_name character varying(145),
    description character varying(245),
    details character varying(245),
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15),
    repetition integer
);


--
-- TOC entry 255 (class 1259 OID 16644)
-- Name: bsns_workflow_by_crop; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.bsns_workflow_by_crop (
    workflow_id integer NOT NULL,
    crop_id integer NOT NULL,
    num_order integer,
    registered_by integer,
    registered_at timestamp without time zone,
    updated_by integer,
    updated_at timestamp without time zone,
    deleted_by integer,
    deleted_at timestamp without time zone,
    status character varying(15)
);


--
-- TOC entry 256 (class 1259 OID 16647)
-- Name: mail_composition; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.mail_composition (
    id integer,
    "notificationId" integer,
    header character varying(45),
    body character varying(245),
    "registeredBy" character varying(45),
    "registeredAt" timestamp without time zone,
    "updatedBy" character varying(45),
    "updatedAt" timestamp without time zone,
    "deletedBy" character varying(45),
    "deletedAt" timestamp without time zone,
    status character varying(255)
);


--
-- TOC entry 257 (class 1259 OID 16653)
-- Name: mail_notification; Type: TABLE; Schema: plims; Owner: -
--

CREATE TABLE plims.mail_notification (
    id integer,
    subject character varying(245),
    "from" character varying(245),
    "to" character varying(245),
    code character varying(45),
    description character varying(245),
    details character varying(245),
    "registeredBy" character varying(45),
    "registeredAt" timestamp without time zone,
    "updatedBy" character varying(45),
    "updatedAt" timestamp without time zone,
    "deletedBy" character varying(45),
    "deletedAt" timestamp without time zone,
    status character varying(255),
    "fromUserId" integer,
    "toUserId" integer
);


--
-- TOC entry 3711 (class 0 OID 61218)
-- Dependencies: 270
-- Data for Name: auth_role; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.auth_role VALUES (1, 'SADM', 'SUPER_ADMIN', 'Administrador del sistema', 1, '2023-02-09 18:24:00.683372', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_role VALUES (2, 'IADM', 'INSTITUTION_ADMIN', 'Administrador de la institucion', 1, '2023-02-09 18:24:00.683372', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_role VALUES (3, 'LADM', 'LABORATORY_ADMIN', 'Administrador del laboratorio', 1, '2023-02-09 18:24:00.683372', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_role VALUES (4, 'NUSR', 'NORMAL_USER', 'Usuario normal', 1, '2023-02-09 18:24:00.683372', NULL, NULL, NULL, NULL, false);


--
-- TOC entry 3712 (class 0 OID 61383)
-- Dependencies: 271
-- Data for Name: auth_task; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.auth_task VALUES (1, 'CHANGE_PASS', 'cambiar contraseña', 'cambiar la contraseña, recordar que debe tener al menos 5 caracteres', 'update-pass', true, 1, 1, '2023-02-09 19:05:30.510948', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (4, 'MANAGE_INSTITUTION', 'manejo de institucion', 'accciones como agregar, editar o dar de baja una entidad INSITUCION', 'manage-institution', false, 1, 1, '2023-02-15 12:23:32.688375', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (5, 'MANAGE_LABORATORY', 'manejo de laboratorio', 'accciones como agregar, editar,  o dar de baja una entidad LABORATORIO', 'manage-laboratory', false, 2, 1, '2023-02-15 12:23:32.688375', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (16, 'MANAGE_AGENT', 'manejo de agentes gestionados por el super admin', 'accciones como agregar, editar o dar de baja una agente (AGENT)', '/configuration/agent/index', false, 3, 1, '2023-02-26 21:03:10.328075', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (17, 'MANAGE_LABORATORY', 'manejo de laboratorio', 'accciones como agregar, editar,  o dar de baja una entidad LABORATORIO', 'manage-laboratory', false, 1, 1, '2023-03-13 15:17:17.524576', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (6, 'MANAGE_USER', 'manejo de usuario por super-admin', 'accciones como agregar, editar,  o dar de baja un USER', 'manage-user', false, 1, 1, '2023-02-15 18:06:35.2489', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (7, 'MANAGE_USER', 'manejo de usuario por admin institucional', 'accciones como agregar, editar,  o dar de baja un USER', 'manage-user', false, 2, 1, '2023-02-15 18:06:35.2489', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (8, 'MANAGE_USER', 'manejo de usuario por admin de laboratorio', 'accciones como agregar, editar,  o dar de baja un USER', 'manage-user', false, 3, 1, '2023-02-15 18:06:35.2489', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (18, 'MANAGE_CROP', 'manejo de cultivo por admin de institution', 'accciones como agregar, editar,  o dar de baja un CROP', '/configuration/crop/index', false, 1, 1, '2023-03-13 17:35:50.180809', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (9, 'MANAGE_CROP', 'manejo de cultivo por admin de institution', 'accciones como agregar, editar,  o dar de baja un CROP', '/configuration/crop/index', false, 2, 1, '2023-02-22 16:42:38.189287', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (10, 'MANAGE_PARAMETER', 'manejo de parametros del sistema por super admin ', 'accciones como agregar, editar,  o dar de baja un PARAMETER', '/configuration/manage-configuration/index', false, 1, 1, '2023-02-22 17:01:41.767531', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (11, 'MANAGE_PARAMETER', 'manejo de parametros del sistema por institution admin', 'accciones como agregar, editar,  o dar de baja un PARAMETER por institucion', '/configuration/manage-configuration/index', false, 2, 1, '2023-02-22 18:33:41.739395', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (12, 'MANAGE_CONDITION', 'manejo de condiciones de cada ensayo gestonados por laboratory admin', 'accciones como agregar, editar,  o dar de baja una CONDITION por LABORATORY_ADMIN', '/configuration/manage-condition/index', false, 3, 1, '2023-02-22 18:50:29.462886', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_task VALUES (13, 'MANAGE_ACTIVITY', 'manejo de actividades gestionados por el super admin', 'accciones como agregar, editar o dar de baja una actividad (ACTIVITY)', '/configuration/activity/index', false, 1, 1, '2023-02-26 21:03:10.328075', NULL, NULL, NULL, NULL, true);
INSERT INTO plims.auth_task VALUES (14, 'MANAGE_ASSAY', 'manejo de analisis gestionados por el super admin', 'accciones como agregar, editar o dar de baja un analisis (ASSAY)', '/configuration/essay/index', false, 1, 1, '2023-02-26 21:03:10.328075', NULL, NULL, NULL, NULL, true);
INSERT INTO plims.auth_task VALUES (15, 'MANAGE_ACTIVITY_BY_ASSAY', 'manejo de actividades por analsis gestionados por el super admin', 'accciones como agregar, editar o dar de baja configuracion de actividad por analisis ACTIVITY_BY_ASSAY', '/configuration/activity-by-essay/index-manage', false, 1, 1, '2023-02-26 21:03:10.328075', NULL, NULL, NULL, NULL, true);


--
-- TOC entry 3645 (class 0 OID 16416)
-- Dependencies: 204
-- Data for Name: auth_user; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.auth_user VALUES (1, 'admin', NULL, NULL, 'j631m1R9V9fXv4JWW_DKm9qVCh-919vS', '$2y$13$RnA8S1gqfNX3rxG6Suu2MuWzxDOLoc6S5WHEzjy1UrVDDscR5MGl2', NULL, '', 10, 1, NULL, 1675965689, NULL, 1676151849, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (12, 'ivanpe834', NULL, NULL, NULL, NULL, NULL, 'ivanpe834@gmail.com', 10, 4, NULL, 1654197028, NULL, 1654197028, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (11, 'peter.jurope', NULL, NULL, NULL, NULL, NULL, 'peter.jurope@gmail.com', 10, 3, NULL, 1650903804, NULL, 1650903804, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (2, 'operator.plims', NULL, NULL, NULL, NULL, NULL, 'operator.plims@gmail.com', 10, 4, NULL, 1649376218, NULL, 1649376218, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (13, NULL, NULL, NULL, NULL, NULL, NULL, 'k.dreher@cimmyt.org', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (17, NULL, NULL, NULL, NULL, NULL, NULL, 'm.corona@cimmyt.org', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (14, NULL, NULL, NULL, NULL, NULL, NULL, 'j.l.lopez@cgiar.org', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (15, NULL, NULL, NULL, NULL, NULL, NULL, 'a.alakonya@cimmyt.org', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (16, NULL, NULL, NULL, NULL, NULL, NULL, 'r.arias@cimmyt.org', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (18, NULL, NULL, NULL, NULL, NULL, NULL, 'l.lopez@cimmyt.org', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (19, NULL, NULL, NULL, NULL, NULL, NULL, 'g.juarez@cimmyt.org', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (20, NULL, NULL, NULL, NULL, NULL, NULL, 'b.a.martinez@cimmyt.org', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (21, NULL, NULL, NULL, NULL, NULL, NULL, 'n.valencia@cimmyt.org', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (22, NULL, NULL, NULL, NULL, NULL, NULL, 'kalevleonj@gmail.com', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (23, NULL, NULL, NULL, NULL, NULL, NULL, 'valenciatn74@gmail.com', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (24, NULL, NULL, NULL, NULL, NULL, NULL, 'bennsmail@gmail.com', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (25, NULL, NULL, NULL, NULL, NULL, NULL, 'mcorona8105@gmail.com', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (26, NULL, NULL, NULL, NULL, NULL, NULL, 'seedhl2017@gmail.com', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO plims.auth_user VALUES (27, NULL, NULL, NULL, NULL, NULL, NULL, 'kate4cimmyt@gmail.com', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 3713 (class 0 OID 61406)
-- Dependencies: 272
-- Data for Name: auth_user_by_role_institution_laboratory; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (1, 1, NULL, NULL, 1, '2023-02-09 18:26:09.102609', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (14, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (15, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (16, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (18, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (19, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (20, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (21, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (22, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (23, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (24, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (25, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (26, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (27, 4, NULL, NULL, 1, '2023-02-13 16:29:52.656059', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (12, 4, 2, 2, 1, '2023-03-08 16:50:26', 1, '2023-03-10 05:17:32', NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (13, 2, 2, NULL, 1, '2023-02-13 16:29:52.656059', 1, '2023-03-14 12:52:31', NULL, NULL, false);
INSERT INTO plims.auth_user_by_role_institution_laboratory VALUES (17, 3, 2, 2, 1, '2023-02-13 16:29:52.656059', 1, '2023-03-14 12:52:53', NULL, NULL, false);


--
-- TOC entry 3647 (class 0 OID 16429)
-- Dependencies: 206
-- Data for Name: auth_user_refresh_tokens; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3648 (class 0 OID 16437)
-- Dependencies: 207
-- Data for Name: bsns_activity; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_activity VALUES (21, '1', 'Blotter Repetition1', '', '', 1, '2020-01-28 09:16:24', 11, '2020-04-07 17:26:07', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (22, '2', 'Blotter Repetition2', '', '', 1, '2020-01-28 09:27:47', 11, '2020-04-07 17:26:10', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (23, '3', 'Blotter Repetition3', '', '', 1, '2020-01-28 09:27:58', 11, '2020-04-07 17:26:14', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (24, '4', 'Blotter Repetition4', '', '', 1, '2020-01-28 09:28:07', 11, '2020-04-07 17:26:18', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (16, 'Type of symptom', 'Type of symptom', 'Type of symptom', 'Type of symptom', 1, '2019-11-27 15:48:27', 10, '2020-02-06 15:02:25', NULL, NULL, 'active', 25, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (18, 'Reaction', 'Agent reaction', 'Agent reaction', 'Agent reaction', 1, '2019-11-27 15:50:07', 10, '2020-02-06 15:02:38', NULL, NULL, 'active', 25, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (17, 'Control', 'Agent control', 'Agent control', 'Agent control', 1, '2019-11-27 15:49:48', 10, '2020-02-06 15:00:09', NULL, NULL, 'active', 25, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (25, '5', 'Blotter Repetition5', '', '', 1, '2020-01-28 09:28:16', 11, '2020-04-07 17:26:22', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (14, '1', 'Germination Repetition1', '', '', 1, '2019-11-27 15:46:53', 11, '2020-04-15 11:00:41', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (15, '2', 'Germination Repetition2', '', '', 1, '2019-11-27 15:47:19', 11, '2020-04-15 11:00:44', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (26, '3', 'Germination Repetition3', '', '', 1, '2020-02-27 14:21:44', 11, '2020-04-15 11:00:47', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (27, '4', 'Germination Repetition4', '', '', 1, '2020-02-27 14:21:57', 11, '2020-04-15 11:00:50', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (28, '5', 'Germination Repetition5', '', '', 1, '2020-02-27 14:22:07', 11, '2020-04-15 11:00:53', NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (29, 'Consolidado', 'Consolidado', '', '', 1, '2020-04-21 17:47:03', NULL, NULL, NULL, NULL, 'active', 63, 1, 61, 1);
INSERT INTO plims.bsns_activity VALUES (5, 'Evaluation 1', 'Agent absorbance evaluation 1', 'Agent absorbance evaluation 1', 'Agent absorbance evaluation 1', 1, '2019-11-27 15:39:47', 11, '2020-04-21 17:49:30', NULL, NULL, 'active', 24, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (6, 'Evaluation 2', 'Agent absorbance evaluation 2', 'Agent absorbance evaluation 2', 'Agent absorbance evaluation 2', 1, '2019-11-27 15:40:09', 11, '2020-04-21 17:49:44', NULL, NULL, 'active', 24, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (30, '6', 'Blotter Repetition6', '', '', 1, '2020-09-11 11:54:06', NULL, NULL, NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (31, '7', 'Blotter Repetition7', '', '', 1, '2020-09-11 11:57:15', NULL, NULL, NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (1, 'Results (Activity in Columns)', 'Result Agent', 'Agent', 'Agent', 1, '2019-11-27 15:34:57', 10, '2020-02-06 14:59:23', NULL, NULL, 'active', 25, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (32, '8', 'Blotter Repetition8', '', '', 1, '2020-09-11 11:57:36', NULL, NULL, NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (33, '9', 'Blotter Repetition9', '', '', 1, '2020-09-11 11:58:09', NULL, NULL, NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (34, '10', 'Blotter Repetition10', '', '', 1, '2020-09-11 11:58:24', NULL, NULL, NULL, NULL, 'active', 33, 1, 62, 1);
INSERT INTO plims.bsns_activity VALUES (35, '6', 'Germination Repetition6', '', '', 1, '2020-09-11 17:33:48', NULL, NULL, NULL, NULL, 'active', 33, NULL, 62, NULL);
INSERT INTO plims.bsns_activity VALUES (36, '7', 'Germination Repetition7', '', '', 1, '2020-09-11 17:34:05', NULL, NULL, NULL, NULL, 'active', 33, NULL, 62, NULL);
INSERT INTO plims.bsns_activity VALUES (37, '8', 'Germination Repetition8', '', '', 1, '2020-09-11 17:34:18', NULL, NULL, NULL, NULL, 'active', 33, NULL, 62, NULL);
INSERT INTO plims.bsns_activity VALUES (38, '9', 'Germination Repetition9', '', '', 1, '2020-09-11 17:34:30', NULL, NULL, NULL, NULL, 'active', 33, NULL, 62, NULL);
INSERT INTO plims.bsns_activity VALUES (39, '10', 'Germination Repetition10', '', '', 1, '2020-09-11 17:34:42', NULL, NULL, NULL, NULL, 'active', 33, NULL, 62, NULL);
INSERT INTO plims.bsns_activity VALUES (2, 'Results (Activity in Rows)', 'Blotter/Germination Result', 'Detection of agent', 'Detection of agent', 1, '2019-11-27 11:20:59', 13, '2020-09-11 17:36:49', NULL, NULL, 'active', 63, 1, 61, 1);


--
-- TOC entry 3649 (class 0 OID 16444)
-- Dependencies: 208
-- Data for Name: bsns_activity_by_essay; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_activity_by_essay VALUES (22, 3, 2, 1, '2020-01-28 09:28:43', 13, '2020-09-11 11:59:58', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (23, 3, 3, 1, '2020-01-28 09:28:43', 13, '2020-09-11 11:59:58', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (24, 3, 4, 1, '2020-01-28 09:28:43', 13, '2020-09-11 11:59:58', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (25, 3, 5, 1, '2020-01-28 09:28:43', 13, '2020-09-11 11:59:58', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (30, 3, 6, 1, '2020-09-11 11:59:58', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (31, 3, 7, 1, '2020-09-11 11:59:58', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (32, 3, 8, 1, '2020-09-11 11:59:58', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (33, 3, 9, 1, '2020-09-11 11:59:58', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (34, 3, 10, 1, '2020-09-11 11:59:58', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (15, 6, 2, 1, '2019-11-27 16:31:39', 13, '2020-09-11 17:37:52', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (26, 6, 3, 1, '2020-02-27 14:25:25', 13, '2020-09-11 17:37:52', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (27, 6, 4, 1, '2020-02-27 14:25:25', 13, '2020-09-11 17:37:52', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (28, 6, 5, 1, '2020-02-27 14:25:25', 13, '2020-09-11 17:37:52', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (35, 6, 6, 1, '2020-09-11 17:37:52', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (36, 6, 7, 1, '2020-09-11 17:37:52', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (37, 6, 8, 1, '2020-09-11 17:37:52', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (38, 6, 9, 1, '2020-09-11 17:37:52', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (39, 6, 10, 1, '2020-09-11 17:37:52', NULL, NULL, NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (6, 5, 2, 1, '2019-11-27 16:31:17', 11, '2020-04-21 17:47:26', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (21, 3, 1, 1, '2020-01-28 09:16:44', 13, '2020-09-11 11:59:58', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (18, 8, 1, 1, '2019-11-27 16:32:30', 6, '2020-01-16 13:56:13', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (5, 5, 1, 1, '2019-11-27 16:31:17', 11, '2020-04-21 17:47:26', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (14, 6, 1, 1, '2019-11-27 16:31:39', 13, '2020-09-11 17:37:52', NULL, NULL, 'active', false);
INSERT INTO plims.bsns_activity_by_essay VALUES (2, 3, 11, 1, '2019-11-27 16:30:14', 13, '2020-09-11 11:59:58', NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (2, 6, 11, 1, '2020-04-07 17:38:28', 13, '2020-09-11 17:37:53', NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (29, 5, 3, 1, '2020-04-21 17:47:26', NULL, NULL, NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (2, 2, 1, 1, '2019-11-27 16:29:51', 11, '2020-04-13 12:12:48', 11, '2020-04-13 12:12:48', 'disabled', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (2, 7, 1, 1, '2019-11-27 16:31:54', 11, '2020-04-13 16:15:26', 11, '2020-04-13 16:15:26', 'disabled', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (2, 4, 1, 1, '2019-11-27 16:30:28', 11, '2020-04-14 09:59:51', 11, '2020-04-14 09:59:51', 'disabled', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (1, 9, 1, 1, '2020-07-17 16:46:56', NULL, NULL, NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (1, 2, 1, 1, '2020-03-27 09:16:22', 11, '2020-04-13 12:12:48', NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (1, 7, 1, 1, '2020-03-27 09:17:48', 11, '2020-04-13 16:15:26', NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (1, 4, 1, 1, '2020-03-27 09:17:30', 11, '2020-04-14 09:59:51', NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (1, 1, 1, 1, '2019-11-27 16:25:58', NULL, NULL, NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (1, 11, 1, 1, '2022-06-22 18:09:20', NULL, NULL, NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (1, 12, 1, 1, '2022-06-22 18:09:42', NULL, NULL, NULL, NULL, 'active', true);
INSERT INTO plims.bsns_activity_by_essay VALUES (1, 13, 1, 1, '2022-06-22 18:10:07', NULL, NULL, NULL, NULL, 'active', true);


--
-- TOC entry 3650 (class 0 OID 16448)
-- Dependencies: 209
-- Data for Name: bsns_agent; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_agent VALUES (32, 'Nigrospora oryzae', 'Nigrospora oryzae', 'Nigrospora oryzae', 1, '2019-11-27 16:06:37', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (35, 'Other', 'Other', 'Other', 1, '2019-11-27 16:07:08', 10, '2020-04-02 16:56:22', NULL, NULL, 'active', 58, 69);
INSERT INTO plims.bsns_agent VALUES (33, 'Stenocarpella macrospora', 'Stenocarpella macrospora', 'Stenocarpella macrospora', 1, '2019-11-27 16:06:46', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (34, 'Stenocarpella maydis', 'Stenocarpella maydis', 'Stenocarpella maydis', 1, '2019-11-27 16:06:58', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (95, 'Fusarium oxysporum', 'Fusarium oxysporum', 'Fusarium oxysporum', 1, '2020-03-03 08:20:53', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (99, 'Rhynchosporium secalis', 'Rhynchosporium secalis', 'Rhynchosporium secalis', 1, '2020-03-03 08:21:40', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (18, 'Aspergillus spp.', 'Aspergillus spp.', 'Aspergillus spp.', 1, '2019-11-27 16:04:02', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (26, 'Curvularia spp.', 'Curvularia spp.', 'Curvularia spp.', 1, '2019-11-27 16:05:35', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (84, 'Alternaria spp.', 'Alternaria spp.', 'Alternaria spp.', 1, '2020-03-03 08:18:12', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (88, 'Drechslera graminea', 'Drechslera graminea', 'Drechslera graminea', 1, '2020-03-03 08:18:57', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (96, 'Fusarium poae', 'Fusarium poae', 'Fusarium poae', 1, '2020-03-03 08:21:06', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (100, 'Septoria nodorum', 'Septoria nodorum', 'Septoria nodorum', 1, '2020-03-03 08:21:49', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (81, 'Sclerophthora rayssiae', 'Sclerophthora rayssiae', 'Sclerophthora rayssiae', 1, '2020-02-03 16:01:44', 1, '2022-06-06 17:13:28', NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (21, 'Bipolaris sorokiniana', 'Bipolaris sorokiniana', 'Bipolaris sorokiniana', 1, '2019-11-27 16:04:28', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (22, 'Botryodiplodia theogromae', 'Botryodiplodia theogromae', 'Botryodiplodia theogromae', 1, '2019-11-27 16:04:37', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (23, 'Cephalosporium maydis', 'Cephalosporium maydis', 'Cephalosporium maydis', 1, '2019-11-27 16:04:46', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (24, 'Cephalosporium spp.', 'Cephalosporium spp.', 'Cephalosporium spp.', 1, '2019-11-27 16:04:57', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (27, 'Exserohilum rostratum', 'Exserohilum rostratum', 'Exserohilum rostratum', 1, '2019-11-27 16:05:43', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (28, 'Fusarium culmorum', 'Fusarium culmorum', 'Fusarium culmorum', 1, '2019-11-27 16:05:54', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (31, 'Macrophomina phaseolina', 'Macrophomina phaseolina', 'Macrophomina phaseolina', 1, '2019-11-27 16:06:27', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (19, 'Bipolaris maydis', 'Bipolaris maydis', 'Bipolaris maydis', 1, '2019-11-27 16:04:10', 13, '2020-07-10 16:29:47', NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (20, 'Bipolaris zeicola', 'Bipolaris zeicola', 'Bipolaris zeicola', 1, '2019-11-27 16:04:20', 13, '2020-07-10 16:30:01', NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (91, 'Fusarium avenaceum (Giberella avenaceae)', 'Fusarium avenaceum (Giberella avenaceae)', 'Fusarium avenaceum (Giberella avenaceae)', 1, '2020-03-03 08:19:35', 13, '2020-07-10 16:33:03', NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (29, 'Fusarium graminearum (Giberella zeae)', 'Fusarium graminearum (Giberella zeae)', 'Fusarium graminearum (Giberella zeae)', 1, '2019-11-27 16:06:05', 13, '2020-07-10 16:34:01', NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (30, 'Fusarium verticilloides (Giberella m.)', 'Fusarium verticilloides (Giberella moniliformis)', 'Fusarium verticilloides (Giberella moniliformis)', 1, '2019-11-27 16:06:15', 13, '2020-07-10 16:34:51', NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (25, 'Colletotrichum graminicola (Glomerella graminicola)', 'Colletotrichum graminicola (Glomerella graminicola)', 'Colletotrichum graminicola (Glomerella graminicola)', 1, '2019-11-27 16:05:26', 13, '2020-07-10 17:04:18', NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (89, 'Drechslera teres', 'Drechslera teres', 'Drechslera teres', 1, '2020-03-03 08:19:11', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (97, 'Fusarium sambucinum', 'Fusarium sambucinum', 'Fusarium sambucinum', 1, '2020-03-03 08:21:17', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (101, 'Septoria tritici', 'Septoria tritici', 'Septoria tritici', 1, '2020-03-03 08:21:59', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (86, 'Bipolaris spicifera', 'Bipolaris spicifera', 'Bipolaris spicifera', 1, '2020-03-03 08:18:40', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (90, 'Drechslera tritici-repentis', 'Drechslera tritici-repentis', 'Drechslera tritici-repentis', 1, '2020-03-03 08:19:23', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (94, 'Fusarium nivale', 'Fusarium nivale', 'Fusarium nivale', 1, '2020-03-03 08:20:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (102, 'Bipolaris hawaiiensis', 'Bipolaris hawaiiensis', 'Bipolaris hawaiiensis', 1, '2020-03-17 10:52:09', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (103, 'Alternaria triticina', 'Alternaria triticina', 'Alternaria triticina', 1, '2020-03-17 10:52:35', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (104, 'Cephalosporium gramineum', 'Cephalosporium gramineum', 'Cephalosporium gramineum', 1, '2020-03-17 10:52:54', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (105, 'Curvularia lunata', 'Curvularia lunata', 'Curvularia lunata', 1, '2020-03-17 10:53:12', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (106, 'Phaeosphaeria avenaria', 'Phaeosphaeria avenaria', 'Phaeosphaeria avenaria', 1, '2020-03-17 10:53:30', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (107, 'Pyricularia grisea', 'Pyricularia grisea', 'Pyricularia grisea', 1, '2020-03-17 10:53:45', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (108, 'Bipolaris victoriae', 'Bipolaris victoriae', 'Bipolaris victoriae', 1, '2020-03-17 11:12:57', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (119, 'Fusarium proliferatum', 'Fusarium proliferatum', 'Fusarium proliferatum', 1, '2020-06-06 19:19:11', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (120, 'Fusarium nygamai', 'Fusarium nygamai', 'Fusarium nygamai', 1, '2020-06-06 19:19:43', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (121, 'Penicillium spp', 'Penicillium spp', 'Penicillium spp', 1, '2020-06-06 19:19:56', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (124, 'Bipolaris cynodontis', 'Bipolaris cynodontis', 'Bipolaris cynodontis', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (125, 'Cephalosporium sp.', 'Cephalosporium sp.', 'Cephalosporium sp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (126, 'Cladosporium spp.', 'Cladosporium spp.', 'Cladosporium spp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (127, 'Didymella pinodes (Mycosphaerella pinoides)', 'Didymella pinodes (Mycosphaerella pinoides)', 'Didymella pinodes (Mycosphaerella pinoides)', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (128, 'Diplodia spp.', 'Diplodia spp.', 'Diplodia spp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (129, 'Exserohilum turcicum', 'Exserohilum turcicum', 'Exserohilum turcicum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (130, 'Fusarium anthophylum', 'Fusarium anthophylum', 'Fusarium anthophylum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (131, 'Fusarium chamydosporum', 'Fusarium chamydosporum', 'Fusarium chamydosporum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (132, 'Fusarium crookwellense', 'Fusarium crookwellense', 'Fusarium crookwellense', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (133, 'Fusarium dimerum', 'Fusarium dimerum', 'Fusarium dimerum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (134, 'Fusarium equiseti', 'Fusarium equiseti', 'Fusarium equiseti', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (135, 'Fusarium pseudograminearum', 'Fusarium pseudograminearum', 'Fusarium pseudograminearum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (136, 'Fusarium sacchari', 'Fusarium sacchari', 'Fusarium sacchari', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (110, 'Acidovorax avenae subsp. avenae', 'Acidovorax avenae subsp. avenae', 'Acidovorax avenae subsp. avenae', 1, '2020-04-30 18:29:03', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (137, 'Fusarium semitectum', 'Fusarium semitectum', 'Fusarium semitectum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (138, 'Fusarium sporotrichioides', 'Fusarium sporotrichioides', 'Fusarium sporotrichioides', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (139, 'Fusarium subglutinans', 'Fusarium subglutinans', 'Fusarium subglutinans', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (140, 'Fusarium tricinctum', 'Fusarium tricinctum', 'Fusarium tricinctum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (141, 'Nigrospora spp.', 'Nigrospora spp.', 'Nigrospora spp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (142, 'Penicillium spp.', 'Penicillium spp.', 'Penicillium spp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (143, 'Pestalotiopsis spp.', 'Pestalotiopsis spp.', 'Pestalotiopsis spp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (144, 'Pyrenophora teres (Helmithosporium teres)', 'Pyrenophora teres (Helmithosporium teres)', 'Pyrenophora teres (Helmithosporium teres)', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (145, 'Sarocladium oryzae', 'Sarocladium oryzae', 'Sarocladium oryzae', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (146, 'Alternaria sp.', 'Alternaria sp.', 'Alternaria sp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (147, 'Colletotrichum graminicola', 'Colletotrichum graminicola', 'Colletotrichum graminicola', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (148, 'Curvularia lunata (Cochliobolus lunatus)', 'Curvularia lunata (Cochliobolus lunatus)', 'Curvularia lunata (Cochliobolus lunatus)', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (149, 'Curvularia sp.', 'Curvularia sp.', 'Curvularia sp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (150, 'Dilophospora alopecuri', 'Dilophospora alopecuri', 'Dilophospora alopecuri', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (151, 'Fusarium acuminatum', 'Fusarium acuminatum', 'Fusarium acuminatum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (152, 'Fusarium anthophilum', 'Fusarium anthophilum', 'Fusarium anthophilum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (153, 'Fusarium avenaceum', 'Fusarium avenaceum', 'Fusarium avenaceum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (154, 'Fusarium aywerte', 'Fusarium aywerte', 'Fusarium aywerte', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (155, 'Fusarium chlamydosporium', 'Fusarium chlamydosporium', 'Fusarium chlamydosporium', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (156, 'Fusarium compactum', 'Fusarium compactum', 'Fusarium compactum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (157, 'Fusarium graminearum', 'Fusarium graminearum', 'Fusarium graminearum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (158, 'Fusarium longipes', 'Fusarium longipes', 'Fusarium longipes', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (159, 'Fusarium nurragi', 'Fusarium nurragi', 'Fusarium nurragi', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (160, 'Fusarium scirpi', 'Fusarium scirpi', 'Fusarium scirpi', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (161, 'Fusarium solani', 'Fusarium solani', 'Fusarium solani', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (162, 'Fusarium verticilloides', 'Fusarium verticilloides', 'Fusarium verticilloides', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (163, 'Microdochium dimerum', 'Microdochium dimerum', 'Microdochium dimerum', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (164, 'Microdochium nivale (Monographella nivalis)', 'Microdochium nivale (Monographella nivalis)', 'Microdochium nivale (Monographella nivalis)', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (165, 'Phoma spp.', 'Phoma spp.', 'Phoma spp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (166, 'Pyrenophora tritici-repentis (Helmithosporium tritici-repentis)', 'Pyrenophora tritici-repentis (Helmithosporium tritici-repentis)', 'Pyrenophora tritici-repentis (Helmithosporium tritici-repentis)', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (167, 'Pyricularia sp.', 'Pyricularia sp.', 'Pyricularia sp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (168, 'Rynchosporium secalis', 'Rynchosporium secalis', 'Rynchosporium secalis', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (169, 'Septoria nodorum (Phaeosphaeria nodorum)', 'Septoria nodorum (Phaeosphaeria nodorum)', 'Septoria nodorum (Phaeosphaeria nodorum)', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (170, 'Verticillium spp.', 'Verticillium spp.', 'Verticillium spp.', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (171, 'Zymoseptoria tritici (Mycosphaerella graminicola)', 'Zymoseptoria tritici (Mycosphaerella graminicola)', 'Zymoseptoria tritici (Mycosphaerella graminicola)', 1, '2020-07-10 16:50:25', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (47, 'Number abnormal seeds', 'Abnormal seedlings', 'Abnormal seedlings', 1, '2019-11-27 16:09:32', 12, '2020-03-02 16:22:11', NULL, NULL, 'active', 60, 147);
INSERT INTO plims.bsns_agent VALUES (51, 'Number seeds without germination', 'Seeds without germination', 'Seeds without germination', 1, '2019-11-27 16:10:18', 12, '2020-03-02 16:23:18', NULL, NULL, 'active', 60, 147);
INSERT INTO plims.bsns_agent VALUES (48, 'Number normal seeds', 'Normal seedlings', 'Normal seedlings', 1, '2019-11-27 16:09:42', 12, '2020-03-02 16:22:05', NULL, NULL, 'active', 60, 147);
INSERT INTO plims.bsns_agent VALUES (50, 'Number of seedlings with symptoms', 'Number of seedlings with symptoms', 'Number of seedlings with symptoms', 1, '2019-11-27 16:10:07', 12, '2020-03-02 16:23:13', NULL, NULL, 'active', 60, 148);
INSERT INTO plims.bsns_agent VALUES (82, 'Type of symptom', 'Type of symptom', 'Type of symptom', 1, '2020-03-02 10:06:44', 12, '2020-03-02 15:01:58', NULL, NULL, 'active', 58, 148);
INSERT INTO plims.bsns_agent VALUES (3, 'Insects', 'Insects', 'Insects', 1, '2019-11-25 14:38:37', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (9, 'Tilletia caries', 'Tilletia caries', 'Tilletia caries', 1, '2019-11-27 16:02:27', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (11, 'Tilletia foetida', 'Tilletia foetida', 'Tilletia foetida', 1, '2019-11-27 16:02:49', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (12, 'Sphacelotheca cruenta', 'Sphacelotheca cruenta', 'Sphacelotheca cruenta', 1, '2019-11-27 16:02:59', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (13, 'Sphacelotheca reiliana', 'Sphacelotheca reiliana', 'Sphacelotheca reiliana', 1, '2019-11-27 16:03:07', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (15, 'Ustilago maydis', 'Ustilago maydis', 'Ustilago maydis', 1, '2019-11-27 16:03:27', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (16, 'Ustilago spp.', 'Ustilago spp.', 'Ustilago spp.', 1, '2019-11-27 16:03:37', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (59, 'Xanthomonas axonopodis', 'Xanthomonas axonopodis', 'Xanthomonas axonopodis', 1, '2019-11-27 16:12:08', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (60, 'Clavibacter michiganensis', 'Clavibacter michiganensis', 'Clavibacter michiganensis', 1, '2019-11-27 16:12:20', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (61, 'Erwinia rhapontici', 'Erwinia rhapontici', 'Erwinia rhapontici', 1, '2019-11-27 16:12:29', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (62, 'Rathayibacter iranicus', 'Rathayibacter iranicus', 'Rathayibacter iranicus', 1, '2019-11-27 16:12:42', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (63, 'Rathayibacter tritici', 'Rathayibacter tritici', 'Rathayibacter tritici', 1, '2019-11-27 16:12:59', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (65, 'Xanthomona', 'Xanthomona', 'Xanthomona', 1, '2019-11-27 16:13:22', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (78, 'Peronosclerospora philippinensis', 'Peronosclerospora philippinensis', 'Peronosclerospora philippinensis', 1, '2020-02-03 16:00:59', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (80, 'Sclerospora maydis', 'Sclerospora maydis', 'Sclerospora maydis', 1, '2020-02-03 16:01:26', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (36, 'Sclerophthora macrospora', 'Sclerophthora macrospora', 'Sclerophthora macrospora', 1, '2019-11-27 16:07:17', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (37, 'Sclerospora graminicola', 'Sclerospora graminicola', 'Sclerospora graminicola', 1, '2019-11-27 16:07:26', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (38, 'Peronosclerospora spp .', 'Peronosclerospora spp .', 'Peronosclerospora spp .', 1, '2019-11-27 16:07:38', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (39, 'Sclerophthora spp .', 'Sclerophthora spp .', 'Sclerophthora spp .', 1, '2019-11-27 16:07:51', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (52, 'Acidovorax avenae', 'Acidovorax avenae', 'Acidovorax avenae', 1, '2019-11-27 16:10:33', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (53, 'Burkholderia andropogonis', 'Burkholderia andropogonis', 'Burkholderia andropogonis', 1, '2019-11-27 16:11:09', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (54, 'Burkholderia gladioli', 'Burkholderia gladioli', 'Burkholderia gladioli', 1, '2019-11-27 16:11:17', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (55, 'Clavibacter michiganensis subsp nebraskensis', 'Clavibacter michiganensis subsp nebraskensis', 'Clavibacter michiganensis subsp nebraskensis', 1, '2019-11-27 16:11:25', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (56, 'Pantoea agglomerans', 'Pantoea agglomerans', 'Pantoea agglomerans', 1, '2019-11-27 16:11:35', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (57, 'Pseudomonas fuscovaginae', 'Pseudomonas fuscovaginae', 'Pseudomonas fuscovaginae', 1, '2019-11-27 16:11:47', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (58, 'Pseudomonas syringae', 'Pseudomonas syringae', 'Pseudomonas syringae', 1, '2019-11-27 16:11:57', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (2, 'Seed with smut', 'Seed with smut', 'Seed with smut', 1, '2019-11-25 14:38:14', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (4, 'Soil', 'Soil', 'Soil', 1, '2019-11-25 14:38:47', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (5, 'Straw', 'Straw', 'Straw', 1, '2019-11-25 14:38:55', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (67, 'Xanthomona spp', 'Xanthomona spp reaction', 'Xanthomona spp reaction', 1, '2019-11-27 16:13:42', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (66, 'Dickeya zea', 'Dickeya zeae', 'Dickeya zeae', 1, '2019-11-27 16:13:32', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (10, 'Tilletia controversa', 'Tilletia controversa', 'Tilletia controversa', 1, '2019-11-27 16:02:37', 13, '2020-06-01 20:34:02', NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (17, 'Puccinia polysora', 'Puccinia polysora', 'Puccinia polysora', 1, '2019-11-27 16:03:46', 13, '2020-06-26 11:49:54', NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (14, 'Phakopsora zeae', 'Phakopsora zeae', 'Phakopsora zeae', 1, '2019-11-27 16:03:18', 13, '2020-06-26 11:56:23', NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (6, 'Weed seed', 'Weed seed', 'Weed seed', 1, '2019-11-25 14:39:11', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (7, 'Claviceps gigantea', 'Claviceps gigantea', 'Claviceps gigantea', 1, '2019-11-25 14:39:28', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (8, 'Tilletia indica', 'Tilletia indica', 'Tilletia indica', 1, '2019-11-27 16:02:12', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (79, 'Peronosclerospora sacchari', 'Peronosclerospora sacchari', 'Peronosclerospora sacchari', 1, '2020-02-03 16:01:15', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (83, 'Germination test', 'Germination test', 'Germination test', 1, '2020-03-02 10:07:30', 12, '2020-03-02 15:01:34', NULL, NULL, 'active', 58, 69);
INSERT INTO plims.bsns_agent VALUES (49, 'Number planted seeds', 'Planted seeds', 'Planted seeds', 1, '2019-11-27 16:09:57', 12, '2020-03-02 16:23:16', NULL, NULL, 'active', 60, 69);
INSERT INTO plims.bsns_agent VALUES (64, 'Xanthomonas translucens pv undulosa', 'Xanthomonas translucens pv. undulosa', 'Xanthomonas translucens pv. undulosa', 1, '2019-11-27 16:13:12', 10, '2020-02-06 13:16:40', NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (109, 'Xanthomonas translucens', 'Xanthomonas translucens', 'Xanthomonas translucens', 1, '2020-04-13 09:50:45', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (111, 'Clavibacter michiganensis subsp. nebraskensis', 'Clavibacter michiganensis subsp. nebraskensis', 'Clavibacter michiganensis subsp. nebraskensis', 1, '2020-04-30 18:39:57', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (112, 'Clavibacter michiganensis subsp. tesselarius', 'Clavibacter michiganensis subsp. tesselarius', 'Clavibacter michiganensis subsp. tesselarius', 1, '2020-04-30 18:40:15', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (113, 'Magnaporthe oryzae Triticum', 'Magnaporthe oryzae Triticum', 'Magnaporthe oryzae Triticum', 1, '2020-04-30 18:40:32', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (114, 'Maize chlorotic mottle virus (MCMV)', 'Maize chlorotic mottle virus (MCMV)', 'Maize chlorotic mottle virus (MCMV)', 1, '2020-04-30 18:40:57', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (115, 'Pantoea stewartiiÂ  subsp. indologenes', 'Pantoea stewartiiÂ  subsp. indologenes', 'Pantoea stewartiiÂ  subsp. indologenes', 1, '2020-04-30 18:41:14', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (116, 'Pantoea stewartiiÂ  subsp. stewartii', 'Pantoea stewartiiÂ  subsp. stewartii', 'Pantoea stewartiiÂ  subsp. stewartii', 1, '2020-04-30 18:41:30', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (117, 'Sugarcane mosaic virus (SCMV)', 'Sugarcane mosaic virus (SCMV)', 'Sugarcane mosaic virus (SCMV)', 1, '2020-04-30 18:41:43', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (118, 'Xanthomonas translucens pv. cerealis', 'Xanthomonas translucens pv. cerealis', 'Xanthomonas translucens pv. cerealis', 1, '2020-04-30 18:41:58', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (122, 'Puccinia spp.', 'Puccinia spp.', 'Puccinia spp.', 1, '2020-06-26 12:23:58', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (123, 'Urocystis agropiry', 'Urocystis agropiry', 'Urocystis agropiry', 1, '2020-06-26 12:24:55', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (172, 'Gloeotinia temulenta', 'Gloeotinia temulenta', 'Gloeotinia temulenta', 1, '2020-07-17 16:55:42', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (173, 'Typhula idahoensis', 'Typhula idahoensis', 'Typhula idahoensis', 1, '2020-12-14 15:44:25', 13, '2020-12-14 15:47:21', NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (174, 'Claviceps purpurea', 'Claviceps purpurea', 'Claviceps purpurea', 1, '2020-12-14 15:44:37', 13, '2020-12-14 15:47:36', NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (1, 'Undeterminated', 'Undeterminated', 'Undeterminated', 1, '2021-09-16 17:45:37', NULL, NULL, NULL, NULL, 'active', 1, 69);
INSERT INTO plims.bsns_agent VALUES (175, 'Xanthomonas vasicola', 'Xanthomonas vasicola', 'Xanthomonas vasicola', 1, '2022-06-27 19:26:55', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (45, 'PS', 'Pantoea stewartii', 'Pantoea stewartii', 1, '2019-11-27 16:09:12', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (176, 'Patogeno de prueba', 'Patogeno de prueba para las pruebas nombre largo', 'Patogeno de prueba descripcion del patogeno de prueba', 1, '2022-12-14 16:29:36', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (177, 'agente de prueba', 'agente de prueba largo', 'descripcion del agente de prueba', 1, '2022-12-14 17:07:56', NULL, NULL, NULL, NULL, 'active', 57, 69);
INSERT INTO plims.bsns_agent VALUES (41, 'BSMV', 'Barley stripe mosaic virus', 'Barley stripe mosaic virus', 1, '2019-11-27 16:08:10', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (43, 'HPWMV', 'High Plains wheat mosaic virus', 'High Plains wheat mosaic virus', 1, '2019-11-27 16:08:52', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (44, 'MDMV', 'Maize dwarf mosaic virus', 'Maize dwarf mosaic virus', 1, '2019-11-27 16:09:03', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (46, 'SMV', 'Sugarcane Mosaic virus', 'Sugarcane Mosaic virus', 1, '2019-11-27 16:09:21', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (40, 'WSMV', 'Wheat streak mosaic virus', 'Wheat streak mosaic virus', 1, '2019-11-27 16:07:59', NULL, NULL, NULL, NULL, 'active', 59, 69);
INSERT INTO plims.bsns_agent VALUES (42, 'MCMV', 'Maize chlorotic mottle virus', 'Maize chlorotic mottle virus', 1, '2019-11-27 16:08:24', NULL, NULL, NULL, NULL, 'active', 59, 69);


--
-- TOC entry 3651 (class 0 OID 16455)
-- Dependencies: 210
-- Data for Name: bsns_agent_condition; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_agent_condition VALUES (33, 3, 4, 11, 30, 1, 159, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (34, 3, 4, 40, 31, 1, 120, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (34, 3, 4, 11, 30, 1, 120, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (35, 3, 4, 40, 31, 1, 95, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (35, 3, 4, 11, 30, 1, 95, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (36, 3, 4, 40, 31, 1, 96, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (36, 3, 4, 11, 30, 1, 96, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (37, 3, 4, 40, 31, 1, 119, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (37, 3, 4, 11, 30, 1, 119, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 4, 3, 40, 31, 1, 78, NULL, 1, '2022-06-06 17:14:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3440311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 4, 3, 40, 31, 1, 79, NULL, 1, '2022-06-06 17:14:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3440311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 4, 3, 40, 31, 1, 80, NULL, 1, '2022-06-06 17:14:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3440311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 4, 3, 40, 31, 1, 81, NULL, 1, '2022-06-06 17:14:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3440311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 8, 3, 11, 30, 1, 116, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 3, 11, 30, 1, 52, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 7, 3, 11, 30, 1, 56, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 8, 3, 40, 30, 1, 115, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 7, 3, 11, 30, 1, 53, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 7, 3, 11, 30, 1, 57, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 8, 3, 11, 30, 1, 115, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 7, 3, 11, 30, 1, 54, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 7, 3, 11, 30, 1, 58, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 8, 3, 40, 30, 1, 52, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 7, 3, 11, 30, 1, 55, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (8, 7, 3, 11, 30, 1, 175, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 8, 3, 11, 30, 1, 52, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 8, 3, 40, 30, 1, 110, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 8, 3, 11, 30, 1, 110, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 8, 3, 40, 30, 1, 60, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 8, 3, 11, 30, 1, 60, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 8, 3, 40, 30, 1, 111, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (38, 3, 4, 40, 31, 1, 97, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 8, 3, 11, 30, 1, 111, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (8, 8, 3, 40, 30, 1, 112, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (38, 3, 4, 11, 30, 1, 97, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 8, 3, 11, 30, 1, 112, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (9, 8, 3, 40, 30, 1, 114, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (9, 8, 3, 11, 30, 1, 114, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (10, 8, 3, 40, 30, 1, 117, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (39, 3, 4, 40, 31, 1, 160, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (39, 3, 4, 11, 30, 1, 160, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (40, 3, 4, 40, 31, 1, 137, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (40, 3, 4, 11, 30, 1, 137, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (41, 3, 4, 40, 31, 1, 161, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (41, 3, 4, 11, 30, 1, 161, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (42, 3, 4, 40, 31, 1, 138, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (42, 3, 4, 11, 30, 1, 138, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (43, 3, 4, 40, 31, 1, 139, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (43, 3, 4, 11, 30, 1, 139, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (44, 3, 4, 40, 31, 1, 140, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (44, 3, 4, 11, 30, 1, 140, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (45, 3, 4, 40, 31, 1, 162, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (45, 3, 4, 11, 30, 1, 162, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (46, 3, 4, 40, 31, 1, 163, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (46, 3, 4, 11, 30, 1, 163, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (47, 3, 4, 40, 31, 1, 164, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (47, 3, 4, 11, 30, 1, 164, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (48, 3, 4, 40, 31, 1, 142, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (48, 3, 4, 11, 30, 1, 142, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (49, 3, 4, 40, 31, 1, 165, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (49, 3, 4, 11, 30, 1, 165, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (50, 3, 4, 40, 31, 1, 144, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (50, 3, 4, 11, 30, 1, 144, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (51, 3, 4, 40, 31, 1, 166, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (10, 8, 3, 11, 30, 1, 117, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 8, 4, 40, 30, 1, 109, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 8, 4, 11, 30, 1, 109, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 3, 40, 30, 1, 53, NULL, 1, '2022-06-27 19:29:17', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3740301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 7, 3, 40, 30, 1, 111, NULL, 1, '2022-06-27 19:29:17', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3740301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 8, 4, 40, 30, 1, 118, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 8, 4, 11, 30, 1, 118, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 11, 31, 1, 42, NULL, 1, '2022-06-27 18:08:48', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 8, 4, 40, 30, 1, 52, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 8, 4, 11, 30, 1, 52, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 8, 4, 40, 30, 1, 110, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 8, 4, 11, 30, 1, 110, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 8, 4, 40, 30, 1, 60, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 8, 4, 11, 30, 1, 60, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 8, 4, 40, 30, 1, 112, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 8, 4, 11, 30, 1, 112, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 8, 4, 40, 30, 1, 63, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 8, 4, 11, 30, 1, 63, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (8, 8, 4, 40, 30, 1, 8, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (8, 8, 4, 11, 30, 1, 8, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (9, 8, 4, 40, 30, 1, 113, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 11, 31, 1, 44, NULL, 1, '2022-06-27 18:08:48', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 3, 3, 40, 31, 1, 18, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 3, 11, 31, 1, 46, NULL, 1, '2022-06-27 18:08:48', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 3, 3, 11, 30, 1, 18, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 5, 3, 11, 31, 1, 45, NULL, 1, '2022-06-27 18:08:48', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 3, 3, 40, 31, 1, 124, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 3, 3, 11, 30, 1, 124, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (9, 8, 4, 11, 30, 1, 113, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 3, 3, 40, 31, 1, 102, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (11, 3, 3, 11, 30, 1, 126, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (12, 3, 3, 40, 31, 1, 25, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (12, 3, 3, 11, 30, 1, 25, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 6, 3, 11, 31, 1, 47, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 6, 3, 11, 31, 1, 48, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 6, 3, 11, 31, 1, 50, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 6, 3, 11, 31, 1, 51, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 6, 3, 11, 31, 1, 82, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 6, 4, 40, 31, 1, 48, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 6, 4, 40, 31, 1, 47, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 6, 4, 40, 31, 1, 51, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 6, 4, 40, 31, 1, 50, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 6, 4, 40, 31, 1, 82, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 6, 4, 11, 31, 1, 48, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 6, 4, 11, 31, 1, 47, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 6, 4, 11, 31, 1, 51, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 6, 4, 11, 31, 1, 50, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (51, 3, 4, 11, 30, 1, 166, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (52, 3, 4, 40, 31, 1, 167, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (52, 3, 4, 11, 30, 1, 167, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (53, 3, 4, 40, 31, 1, 168, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (53, 3, 4, 11, 30, 1, 168, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (54, 3, 4, 40, 31, 1, 169, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (54, 3, 4, 11, 30, 1, 169, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (55, 3, 4, 40, 31, 1, 170, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (55, 3, 4, 11, 30, 1, 170, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (23, 3, 3, 11, 30, 1, 133, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (24, 3, 3, 40, 31, 1, 134, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (24, 3, 3, 11, 30, 1, 134, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (25, 3, 3, 40, 31, 1, 29, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (25, 3, 3, 11, 30, 1, 29, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (26, 3, 3, 40, 31, 1, 94, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (26, 3, 3, 11, 30, 1, 94, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (27, 3, 3, 40, 31, 1, 120, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (27, 3, 3, 11, 30, 1, 120, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 11, 29, 1, 42, NULL, 1, '2022-06-27 18:09:44', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 3, 3, 11, 30, 1, 102, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 11, 29, 1, 44, NULL, 1, '2022-06-27 18:09:44', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 3, 11, 29, 1, 46, NULL, 1, '2022-06-27 18:09:44', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 5, 3, 11, 29, 1, 45, NULL, 1, '2022-06-27 18:09:44', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 3, 3, 40, 31, 1, 19, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 4, 11, 30, 1, 52, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 3, 3, 11, 30, 1, 19, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 7, 4, 11, 30, 1, 58, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 7, 4, 11, 30, 1, 60, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 7, 4, 11, 30, 1, 62, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 3, 3, 40, 31, 1, 21, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 7, 4, 11, 30, 1, 61, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 3, 3, 11, 30, 1, 21, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 7, 4, 11, 30, 1, 63, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 7, 4, 11, 30, 1, 57, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4711301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 4, 40, 31, 65, 8, NULL, 1, '2022-12-14 17:21:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42403165-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 3, 3, 40, 31, 1, 86, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 2, 4, 40, 31, 65, 10, NULL, 1, '2022-12-14 17:21:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42403165-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 3, 3, 11, 30, 1, 86, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 2, 4, 40, 31, 65, 9, NULL, 1, '2022-12-14 17:21:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42403165-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 2, 4, 40, 31, 65, 16, NULL, 1, '2022-12-14 17:21:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42403165-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 3, 3, 40, 31, 1, 108, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 3, 3, 11, 30, 1, 108, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (8, 3, 3, 40, 31, 1, 20, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (8, 3, 3, 11, 30, 1, 20, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (9, 3, 3, 40, 31, 1, 23, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (9, 3, 3, 11, 30, 1, 23, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (10, 3, 3, 40, 31, 1, 125, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (10, 3, 3, 11, 30, 1, 125, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (11, 3, 3, 40, 31, 1, 126, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 4, 3, 40, 31, 1, 78, NULL, 1, '2022-06-06 17:14:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3440311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 4, 3, 40, 31, 1, 79, NULL, 1, '2022-06-06 17:14:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3440311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 4, 3, 40, 31, 1, 80, NULL, 1, '2022-06-06 17:14:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3440311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 4, 3, 40, 31, 1, 81, NULL, 1, '2022-06-06 17:14:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3440311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 4, 4, 11, 30, 1, 36, NULL, 1, '2022-12-14 17:22:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4411301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 4, 3, 11, 30, 1, 38, NULL, 1, '2022-06-27 17:53:11', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3411301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 4, 3, 11, 30, 1, 39, NULL, 1, '2022-06-27 17:53:11', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3411301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 4, 3, 11, 30, 1, 37, NULL, 1, '2022-06-27 17:53:11', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3411301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (28, 3, 3, 40, 31, 1, 95, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (28, 3, 3, 11, 30, 1, 95, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (29, 3, 3, 40, 31, 1, 96, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (16, 3, 4, 40, 31, 1, 149, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (16, 3, 4, 11, 30, 1, 149, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (26, 3, 4, 40, 31, 1, 132, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (26, 3, 4, 11, 30, 1, 132, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (27, 3, 4, 40, 31, 1, 28, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (27, 3, 4, 11, 30, 1, 28, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (28, 3, 4, 40, 31, 1, 133, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (28, 3, 4, 11, 30, 1, 133, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (56, 3, 4, 40, 31, 1, 171, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (56, 3, 4, 11, 30, 1, 171, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (13, 3, 3, 40, 31, 1, 26, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (13, 3, 3, 11, 30, 1, 26, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (14, 3, 3, 40, 31, 1, 127, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 40, 31, 1, 42, NULL, 1, '2022-06-27 18:11:13', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (14, 3, 3, 11, 30, 1, 127, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 40, 31, 1, 44, NULL, 1, '2022-06-27 18:11:13', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 3, 40, 31, 1, 46, NULL, 1, '2022-06-27 18:11:13', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 5, 3, 40, 31, 1, 45, NULL, 1, '2022-06-27 18:11:13', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (15, 3, 3, 40, 31, 1, 128, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 4, 11, 31, 1, 109, NULL, 1, '2022-06-27 19:31:38', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4711311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (15, 3, 3, 11, 30, 1, 128, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 4, 4, 11, 30, 1, 36, NULL, 1, '2022-12-14 17:22:01', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4411301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (16, 3, 3, 40, 31, 1, 27, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (16, 3, 3, 11, 30, 1, 27, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (17, 3, 3, 40, 31, 1, 129, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (17, 3, 3, 11, 30, 1, 129, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (18, 3, 3, 40, 31, 1, 130, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (18, 3, 3, 11, 30, 1, 130, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (19, 3, 3, 40, 31, 1, 91, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (19, 3, 3, 11, 30, 1, 91, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (20, 3, 3, 40, 31, 1, 131, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (20, 3, 3, 11, 30, 1, 131, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (21, 3, 3, 40, 31, 1, 132, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (21, 3, 3, 11, 30, 1, 132, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (22, 3, 3, 40, 31, 1, 28, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (22, 3, 3, 11, 30, 1, 28, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (23, 3, 3, 40, 31, 1, 133, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (29, 3, 3, 11, 30, 1, 96, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (30, 3, 3, 40, 31, 1, 119, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (30, 3, 3, 11, 30, 1, 119, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 1, 3, 40, 29, 1, 6, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 1, 4, 11, 29, 1, 2, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 1, 4, 11, 29, 1, 3, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 1, 4, 11, 29, 1, 4, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 1, 3, 40, 29, 1, 6, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 1, 4, 11, 29, 1, 2, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 1, 4, 11, 29, 1, 3, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 1, 4, 11, 29, 1, 4, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 1, 3, 11, 29, 1, 2, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 1, 3, 11, 29, 1, 3, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 1, 3, 11, 29, 1, 4, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 11, 4, 11, 30, 1, 8, NULL, 1, '2022-06-22 22:17:14', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC41111301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 11, 4, 40, 30, 1, 8, NULL, 1, '2022-06-22 22:18:41', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC41140301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 40, 29, 1, 42, NULL, 1, '2022-06-27 18:11:31', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (31, 3, 3, 40, 31, 1, 135, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 40, 29, 1, 44, NULL, 1, '2022-06-27 18:11:31', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (31, 3, 3, 11, 30, 1, 135, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 3, 40, 29, 1, 46, NULL, 1, '2022-06-27 18:11:31', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 5, 3, 40, 29, 1, 45, NULL, 1, '2022-06-27 18:11:31', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 6, 3, 11, 31, 1, 47, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (32, 3, 3, 40, 31, 1, 136, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 6, 3, 11, 31, 1, 48, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (32, 3, 3, 11, 30, 1, 136, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 6, 3, 11, 31, 1, 50, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 6, 3, 11, 31, 1, 51, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 6, 3, 11, 31, 1, 82, NULL, 1, '2022-06-27 19:02:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (33, 3, 3, 40, 31, 1, 97, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 4, 40, 30, 1, 58, NULL, 1, '2022-06-27 19:32:39', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4740301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (33, 3, 3, 11, 30, 1, 97, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 7, 4, 40, 30, 1, 60, NULL, 1, '2022-06-27 19:32:39', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4740301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (34, 3, 3, 40, 31, 1, 137, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (34, 3, 3, 11, 30, 1, 137, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (35, 3, 3, 40, 31, 1, 138, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (35, 3, 3, 11, 30, 1, 138, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (36, 3, 3, 40, 31, 1, 139, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (36, 3, 3, 11, 30, 1, 139, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (37, 3, 3, 40, 31, 1, 140, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (37, 3, 3, 11, 30, 1, 140, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (38, 3, 3, 40, 31, 1, 30, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (38, 3, 3, 11, 30, 1, 30, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (39, 3, 3, 40, 31, 1, 31, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (39, 3, 3, 11, 30, 1, 31, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (40, 3, 3, 40, 31, 1, 141, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (40, 3, 3, 11, 30, 1, 141, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (41, 3, 3, 40, 31, 1, 142, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (41, 3, 3, 11, 30, 1, 142, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 11, 31, 1, 42, NULL, 1, '2022-06-27 18:08:48', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 11, 31, 1, 44, NULL, 1, '2022-06-27 18:08:48', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 12, 4, 11, 30, 1, 8, NULL, 1, '2022-06-22 22:21:23', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC41211301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 12, 4, 40, 30, 1, 8, NULL, 1, '2022-06-22 22:23:00', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC41240301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 40, 31, 1, 41, NULL, 1, '2022-06-27 18:15:56', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4540311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 40, 31, 1, 40, NULL, 1, '2022-06-27 18:15:56', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4540311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (42, 3, 3, 40, 31, 1, 143, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 6, 4, 40, 31, 1, 48, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (42, 3, 3, 11, 30, 1, 143, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 6, 4, 40, 31, 1, 47, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 6, 4, 40, 31, 1, 51, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 6, 4, 40, 31, 1, 50, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (43, 3, 3, 40, 31, 1, 144, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 6, 4, 40, 31, 1, 82, NULL, 1, '2022-06-27 19:03:38', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (43, 3, 3, 11, 30, 1, 144, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 4, 40, 31, 1, 109, NULL, 1, '2022-06-27 19:33:19', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4740311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (44, 3, 3, 40, 31, 1, 145, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (44, 3, 3, 11, 30, 1, 145, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 3, 4, 40, 31, 1, 146, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 3, 4, 11, 30, 1, 146, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 3, 4, 40, 31, 1, 103, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 3, 4, 11, 30, 1, 103, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 3, 4, 40, 31, 1, 18, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 3, 4, 11, 30, 1, 18, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 3, 4, 40, 31, 1, 124, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 3, 4, 11, 30, 1, 124, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 3, 4, 40, 31, 1, 102, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 3, 4, 11, 30, 1, 102, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 3, 4, 40, 31, 1, 19, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 3, 4, 11, 30, 1, 19, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 3, 4, 40, 31, 1, 21, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 3, 4, 11, 30, 1, 21, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (8, 3, 4, 40, 31, 1, 86, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (8, 3, 4, 11, 30, 1, 86, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 4, 40, 31, 65, 8, NULL, 1, '2022-12-14 17:21:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42403165-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 2, 4, 40, 31, 65, 10, NULL, 1, '2022-12-14 17:21:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42403165-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 2, 4, 40, 31, 65, 9, NULL, 1, '2022-12-14 17:21:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42403165-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 2, 4, 40, 31, 65, 16, NULL, 1, '2022-12-14 17:21:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42403165-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 4, 11, 30, 65, 10, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 2, 4, 11, 30, 65, 9, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 2, 4, 11, 30, 65, 11, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 2, 4, 11, 30, 65, 122, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 2, 4, 11, 30, 65, 16, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 2, 4, 11, 30, 65, 123, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 2, 4, 11, 30, 65, 12, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 4, 11, 31, 66, 8, B'1', 1, '2020-12-23 16:13:34', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC42113166-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 3, 40, 30, 65, 13, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC32403065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 2, 3, 40, 30, 65, 15, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC32403065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 2, 3, 11, 30, 65, 13, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC32113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 2, 3, 11, 30, 65, 14, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC32113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 3, 11, 30, 65, 16, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC32113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 2, 3, 11, 30, 65, 17, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC32113065-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 6, 4, 11, 31, 1, 48, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 40, 29, 1, 41, NULL, 1, '2022-06-27 18:16:24', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4540291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 40, 29, 1, 40, NULL, 1, '2022-06-27 18:16:24', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4540291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (9, 3, 4, 40, 31, 1, 108, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 6, 4, 11, 31, 1, 47, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (9, 3, 4, 11, 30, 1, 108, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 6, 4, 11, 31, 1, 51, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 6, 4, 11, 31, 1, 50, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 6, 4, 11, 31, 1, 82, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4611311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (10, 3, 4, 40, 31, 1, 20, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (10, 3, 4, 11, 30, 1, 20, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (11, 3, 4, 40, 31, 1, 104, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (11, 3, 4, 11, 30, 1, 104, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (12, 3, 4, 40, 31, 1, 125, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (12, 3, 4, 11, 30, 1, 125, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (13, 3, 4, 40, 31, 1, 126, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (13, 3, 4, 11, 30, 1, 126, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (14, 3, 4, 40, 31, 1, 147, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (14, 3, 4, 11, 30, 1, 147, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (15, 3, 4, 40, 31, 1, 148, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (15, 3, 4, 11, 30, 1, 148, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (17, 3, 4, 40, 31, 1, 150, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (17, 3, 4, 11, 30, 1, 150, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (18, 3, 4, 40, 31, 1, 27, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (18, 3, 4, 11, 30, 1, 27, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (19, 3, 4, 40, 31, 1, 129, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (19, 3, 4, 11, 30, 1, 129, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 3, 11, 30, 1, 52, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 7, 3, 11, 30, 1, 56, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 7, 3, 11, 30, 1, 53, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 7, 3, 11, 30, 1, 57, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 7, 3, 11, 30, 1, 54, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 7, 3, 11, 30, 1, 58, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 7, 3, 11, 30, 1, 55, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 7, 3, 11, 30, 1, 175, NULL, 1, '2022-06-27 19:27:23', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 3, 40, 30, 1, 53, NULL, 1, '2022-06-27 19:29:17', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3740301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 7, 3, 40, 30, 1, 111, NULL, 1, '2022-06-27 19:29:17', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3740301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 4, 11, 30, 1, 52, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 7, 4, 11, 30, 1, 58, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 7, 4, 11, 30, 1, 60, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 7, 4, 11, 30, 1, 62, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 7, 4, 11, 30, 1, 61, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 7, 4, 11, 30, 1, 63, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 7, 4, 11, 30, 1, 57, NULL, 1, '2022-06-27 19:31:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4711301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 4, 11, 31, 1, 109, NULL, 1, '2022-06-27 19:31:38', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4711311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 4, 40, 30, 1, 58, NULL, 1, '2022-06-27 19:32:39', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4740301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 7, 4, 40, 30, 1, 60, NULL, 1, '2022-06-27 19:32:39', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4740301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 7, 4, 40, 31, 1, 109, NULL, 1, '2022-06-27 19:33:19', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4740311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (20, 3, 4, 40, 31, 1, 151, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 4, 11, 30, 65, 10, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (20, 3, 4, 11, 30, 1, 151, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 2, 4, 11, 30, 65, 9, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 11, 31, 1, 41, NULL, 1, '2022-06-27 18:17:56', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4511311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 11, 31, 1, 40, NULL, 1, '2022-06-27 18:17:56', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4511311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (21, 3, 4, 40, 31, 1, 152, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 2, 4, 11, 30, 65, 11, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (21, 3, 4, 11, 30, 1, 152, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 2, 4, 11, 30, 65, 122, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 2, 4, 11, 30, 65, 16, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 2, 4, 11, 30, 65, 123, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (22, 3, 4, 40, 31, 1, 153, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 2, 4, 11, 30, 65, 12, NULL, 1, '2022-06-27 19:40:54', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (22, 3, 4, 11, 30, 1, 153, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (23, 3, 4, 40, 31, 1, 154, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (23, 3, 4, 11, 30, 1, 154, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (24, 3, 4, 40, 31, 1, 155, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (24, 3, 4, 11, 30, 1, 155, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (25, 3, 4, 40, 31, 1, 156, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (25, 3, 4, 11, 30, 1, 156, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (29, 3, 4, 40, 31, 1, 134, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (29, 3, 4, 11, 30, 1, 134, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (30, 3, 4, 40, 31, 1, 157, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (30, 3, 4, 11, 30, 1, 157, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (31, 3, 4, 40, 31, 1, 158, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (31, 3, 4, 11, 30, 1, 158, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (32, 3, 4, 40, 31, 1, 94, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (32, 3, 4, 11, 30, 1, 94, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (33, 3, 4, 40, 31, 1, 159, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 6, 4, 11, 31, 1, 82, NULL, 1, '2022-06-27 19:03:46', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4611311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 6, 3, 40, 31, 1, 47, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 6, 3, 40, 31, 1, 48, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 6, 3, 40, 31, 1, 50, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 6, 3, 40, 31, 1, 51, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 6, 3, 40, 31, 1, 82, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3640311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 1, 3, 11, 29, 1, 5, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 1, 3, 11, 29, 1, 6, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 1, 3, 11, 29, 1, 7, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 1, 3, 40, 29, 1, 2, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 1, 3, 40, 29, 1, 3, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 1, 3, 40, 29, 1, 4, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 1, 3, 40, 29, 1, 5, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 1, 4, 11, 29, 1, 5, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 1, 4, 11, 29, 1, 6, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 1, 4, 11, 29, 1, 173, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 1, 4, 11, 29, 1, 174, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4111291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 1, 4, 40, 29, 1, 2, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 1, 4, 40, 29, 1, 3, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 1, 4, 40, 29, 1, 4, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 1, 4, 40, 29, 1, 5, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 1, 4, 40, 29, 1, 6, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 1, 4, 40, 29, 1, 174, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4140291-1', 1, false, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 3, 11, 31, 1, 46, NULL, 1, '2022-06-27 18:08:48', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 5, 3, 11, 31, 1, 45, NULL, 1, '2022-06-27 18:08:48', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 11, 29, 1, 42, NULL, 1, '2022-06-27 18:09:44', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 11, 29, 1, 44, NULL, 1, '2022-06-27 18:09:44', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 3, 11, 29, 1, 46, NULL, 1, '2022-06-27 18:09:44', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 5, 3, 11, 29, 1, 45, NULL, 1, '2022-06-27 18:09:44', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 40, 31, 1, 42, NULL, 1, '2022-06-27 18:11:13', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 40, 31, 1, 44, NULL, 1, '2022-06-27 18:11:13', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 3, 40, 31, 1, 46, NULL, 1, '2022-06-27 18:11:13', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 5, 3, 40, 31, 1, 45, NULL, 1, '2022-06-27 18:11:13', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 40, 29, 1, 42, NULL, 1, '2022-06-27 18:11:31', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 40, 29, 1, 44, NULL, 1, '2022-06-27 18:11:31', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 3, 40, 29, 1, 46, NULL, 1, '2022-06-27 18:11:31', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (33, 3, 4, 11, 30, 1, 159, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 6, 3, 40, 31, 1, 47, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 11, 29, 1, 41, NULL, 1, '2022-06-27 18:18:41', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4511291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 11, 29, 1, 40, NULL, 1, '2022-06-27 18:18:41', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4511291-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (34, 3, 4, 40, 31, 1, 120, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 6, 3, 40, 31, 1, 48, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (34, 3, 4, 11, 30, 1, 120, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 6, 3, 40, 31, 1, 50, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 6, 3, 40, 31, 1, 51, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 6, 3, 40, 31, 1, 82, NULL, 1, '2022-08-09 17:27:14', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3640311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (35, 3, 4, 40, 31, 1, 95, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (35, 3, 4, 11, 30, 1, 95, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (36, 3, 4, 40, 31, 1, 96, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (36, 3, 4, 11, 30, 1, 96, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (37, 3, 4, 40, 31, 1, 119, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (37, 3, 4, 11, 30, 1, 119, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (38, 3, 4, 40, 31, 1, 97, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (38, 3, 4, 11, 30, 1, 97, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (39, 3, 4, 40, 31, 1, 160, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (39, 3, 4, 11, 30, 1, 160, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (40, 3, 4, 40, 31, 1, 137, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (40, 3, 4, 11, 30, 1, 137, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (41, 3, 4, 40, 31, 1, 161, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (41, 3, 4, 11, 30, 1, 161, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (42, 3, 4, 40, 31, 1, 138, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (42, 3, 4, 11, 30, 1, 138, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (43, 3, 4, 40, 31, 1, 139, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (43, 3, 4, 11, 30, 1, 139, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (44, 3, 4, 40, 31, 1, 140, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 5, 3, 40, 29, 1, 45, NULL, 1, '2022-06-27 18:11:31', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 40, 31, 1, 41, NULL, 1, '2022-06-27 18:15:56', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4540311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 40, 31, 1, 40, NULL, 1, '2022-06-27 18:15:56', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4540311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 40, 29, 1, 41, NULL, 1, '2022-06-27 18:16:24', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4540291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 40, 29, 1, 40, NULL, 1, '2022-06-27 18:16:24', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4540291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 11, 31, 1, 41, NULL, 1, '2022-06-27 18:17:56', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4511311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 11, 31, 1, 40, NULL, 1, '2022-06-27 18:17:56', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4511311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 11, 29, 1, 41, NULL, 1, '2022-06-27 18:18:41', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4511291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 11, 29, 1, 40, NULL, 1, '2022-06-27 18:18:41', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4511291-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 40, 30, 1, 43, NULL, 1, '2022-06-27 17:46:45', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 40, 30, 1, 40, NULL, 1, '2022-06-27 17:46:45', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3540301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 11, 30, 1, 43, NULL, 1, '2022-06-27 17:49:11', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 11, 30, 1, 40, NULL, 1, '2022-06-27 17:49:11', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC3511301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 11, 30, 1, 42, NULL, 1, '2022-06-27 17:50:49', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4511301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 11, 30, 1, 45, NULL, 1, '2022-06-27 17:50:49', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4511301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 4, 11, 30, 1, 43, NULL, 1, '2022-06-27 17:50:49', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4511301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 40, 30, 1, 43, NULL, 1, '2022-06-27 17:51:20', NULL, NULL, NULL, NULL, 'active', 2, 'L2ACC4540301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 8, 3, 11, 30, 1, 116, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 8, 3, 40, 30, 1, 115, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 8, 3, 11, 30, 1, 115, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 8, 3, 40, 30, 1, 52, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 8, 3, 11, 30, 1, 52, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 8, 3, 40, 30, 1, 110, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 8, 3, 11, 30, 1, 110, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 8, 3, 40, 30, 1, 60, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 8, 3, 11, 30, 1, 60, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 8, 3, 40, 30, 1, 111, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 8, 3, 11, 30, 1, 111, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 8, 3, 40, 30, 1, 112, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 8, 3, 11, 30, 1, 112, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (9, 8, 3, 40, 30, 1, 114, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (9, 8, 3, 11, 30, 1, 114, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (10, 8, 3, 40, 30, 1, 117, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 8, 4, 11, 30, 1, 109, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 8, 4, 40, 30, 1, 118, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 8, 4, 11, 30, 1, 118, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 8, 4, 40, 30, 1, 52, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (44, 3, 4, 11, 30, 1, 140, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (45, 3, 4, 40, 31, 1, 162, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (45, 3, 4, 11, 30, 1, 162, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (46, 3, 4, 40, 31, 1, 163, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (46, 3, 4, 11, 30, 1, 163, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (47, 3, 4, 40, 31, 1, 164, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (47, 3, 4, 11, 30, 1, 164, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (48, 3, 4, 40, 31, 1, 142, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (48, 3, 4, 11, 30, 1, 142, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (49, 3, 4, 40, 31, 1, 165, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (49, 3, 4, 11, 30, 1, 165, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (50, 3, 4, 40, 31, 1, 144, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (50, 3, 4, 11, 30, 1, 144, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (51, 3, 4, 40, 31, 1, 166, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (51, 3, 4, 11, 30, 1, 166, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (52, 3, 4, 40, 31, 1, 167, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (52, 3, 4, 11, 30, 1, 167, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (53, 3, 4, 40, 31, 1, 168, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (53, 3, 4, 11, 30, 1, 168, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (54, 3, 4, 40, 31, 1, 169, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (54, 3, 4, 11, 30, 1, 169, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 13, 4, 11, 30, 1, 16, NULL, 1, '2022-06-22 22:24:25', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC41311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 13, 4, 40, 30, 1, 16, NULL, 1, '2022-06-22 22:25:20', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC41340301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (10, 8, 3, 11, 30, 1, 117, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 8, 4, 11, 30, 1, 52, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 8, 4, 40, 30, 1, 109, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 8, 4, 40, 30, 1, 110, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 8, 4, 11, 30, 1, 110, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 8, 4, 40, 30, 1, 60, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 8, 4, 11, 30, 1, 60, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 8, 4, 40, 30, 1, 112, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 8, 4, 11, 30, 1, 112, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 8, 4, 40, 30, 1, 63, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 8, 4, 11, 30, 1, 63, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 8, 4, 40, 30, 1, 8, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 8, 4, 11, 30, 1, 8, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (9, 8, 4, 40, 30, 1, 113, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (9, 8, 4, 11, 30, 1, 113, B'1', 1, '2020-04-30 18:54:12', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 8, 3, 40, 30, 1, 66, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 8, 3, 11, 30, 1, 66, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3811301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 8, 3, 40, 30, 1, 116, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3840301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (55, 3, 4, 40, 31, 1, 170, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (55, 3, 4, 11, 30, 1, 170, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 4, 11, 31, 66, 8, B'1', 1, '2020-12-23 16:13:34', NULL, NULL, NULL, NULL, 'active', 1, 'ACC42113166-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 3, 40, 30, 65, 13, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'ACC32403065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 9, 4, 11, 30, 1, 172, B'1', 1, '2020-07-17 15:02:21', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4911301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 2, 3, 40, 30, 65, 15, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'ACC32403065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 2, 3, 11, 30, 65, 13, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'ACC32113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 2, 3, 11, 30, 65, 14, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'ACC32113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 2, 3, 11, 30, 65, 16, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'ACC32113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 2, 3, 11, 30, 65, 17, B'1', 1, '2020-12-23 16:13:33', NULL, NULL, NULL, NULL, 'active', 1, 'ACC32113065-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 1, 3, 11, 29, 1, 2, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 1, 3, 11, 29, 1, 3, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 1, 3, 11, 29, 1, 4, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 8, 3, 40, 30, 1, 66, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 8, 3, 11, 30, 1, 66, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3811301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 8, 3, 40, 30, 1, 116, B'1', 1, '2020-04-30 18:54:11', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3840301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (23, 3, 3, 11, 30, 1, 133, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (24, 3, 3, 40, 31, 1, 134, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (24, 3, 3, 11, 30, 1, 134, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (25, 3, 3, 40, 31, 1, 29, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (25, 3, 3, 11, 30, 1, 29, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (26, 3, 3, 40, 31, 1, 94, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (26, 3, 3, 11, 30, 1, 94, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (27, 3, 3, 40, 31, 1, 120, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (27, 3, 3, 11, 30, 1, 120, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 3, 3, 40, 31, 1, 18, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 3, 3, 11, 30, 1, 18, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 3, 3, 40, 31, 1, 124, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 3, 3, 11, 30, 1, 124, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 3, 3, 40, 31, 1, 102, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (11, 3, 3, 11, 30, 1, 126, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (12, 3, 3, 40, 31, 1, 25, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (12, 3, 3, 11, 30, 1, 25, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 3, 3, 11, 30, 1, 102, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 3, 3, 40, 31, 1, 19, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 3, 3, 11, 30, 1, 19, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 3, 3, 40, 31, 1, 21, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 3, 3, 11, 30, 1, 21, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 3, 3, 40, 31, 1, 86, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 3, 3, 11, 30, 1, 86, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 3, 3, 40, 31, 1, 108, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 3, 3, 11, 30, 1, 108, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 3, 3, 40, 31, 1, 20, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 3, 3, 11, 30, 1, 20, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (9, 3, 3, 40, 31, 1, 23, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (9, 3, 3, 11, 30, 1, 23, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (10, 3, 3, 40, 31, 1, 125, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (10, 3, 3, 11, 30, 1, 125, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (11, 3, 3, 40, 31, 1, 126, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (13, 3, 3, 40, 31, 1, 26, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (13, 3, 3, 11, 30, 1, 26, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (14, 3, 3, 40, 31, 1, 127, B'1', 1, '2020-07-10 17:30:01', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (14, 3, 3, 11, 30, 1, 127, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (15, 3, 3, 40, 31, 1, 128, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (15, 3, 3, 11, 30, 1, 128, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (16, 3, 3, 40, 31, 1, 27, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (16, 3, 3, 11, 30, 1, 27, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (17, 3, 3, 40, 31, 1, 129, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (17, 3, 3, 11, 30, 1, 129, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (18, 3, 3, 40, 31, 1, 130, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (18, 3, 3, 11, 30, 1, 130, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (19, 3, 3, 40, 31, 1, 91, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (19, 3, 3, 11, 30, 1, 91, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (20, 3, 3, 40, 31, 1, 131, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (20, 3, 3, 11, 30, 1, 131, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (21, 3, 3, 40, 31, 1, 132, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (21, 3, 3, 11, 30, 1, 132, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (22, 3, 3, 40, 31, 1, 28, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (22, 3, 3, 11, 30, 1, 28, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (23, 3, 3, 40, 31, 1, 133, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (29, 3, 3, 11, 30, 1, 96, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (30, 3, 3, 40, 31, 1, 119, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (30, 3, 3, 11, 30, 1, 119, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (31, 3, 3, 40, 31, 1, 135, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (31, 3, 3, 11, 30, 1, 135, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (28, 3, 3, 40, 31, 1, 95, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (28, 3, 3, 11, 30, 1, 95, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (29, 3, 3, 40, 31, 1, 96, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (16, 3, 4, 40, 31, 1, 149, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (16, 3, 4, 11, 30, 1, 149, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (26, 3, 4, 40, 31, 1, 132, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (26, 3, 4, 11, 30, 1, 132, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (27, 3, 4, 40, 31, 1, 28, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (27, 3, 4, 11, 30, 1, 28, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (28, 3, 4, 40, 31, 1, 133, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (28, 3, 4, 11, 30, 1, 133, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (56, 3, 4, 40, 31, 1, 171, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4340311-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (56, 3, 4, 11, 30, 1, 171, B'1', 1, '2020-07-10 17:30:06', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 9, 4, 11, 30, 1, 172, B'1', 1, '2020-07-17 15:02:21', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4911301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 1, 3, 11, 29, 1, 5, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 1, 3, 11, 29, 1, 6, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 1, 3, 11, 29, 1, 7, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 1, 3, 40, 29, 1, 2, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 1, 3, 40, 29, 1, 3, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 1, 3, 40, 29, 1, 4, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 1, 3, 40, 29, 1, 5, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (32, 3, 3, 40, 31, 1, 136, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (32, 3, 3, 11, 30, 1, 136, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (33, 3, 3, 40, 31, 1, 97, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (33, 3, 3, 11, 30, 1, 97, B'1', 1, '2020-07-10 17:30:02', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (34, 3, 3, 40, 31, 1, 137, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (34, 3, 3, 11, 30, 1, 137, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (35, 3, 3, 40, 31, 1, 138, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (35, 3, 3, 11, 30, 1, 138, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (36, 3, 3, 40, 31, 1, 139, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (36, 3, 3, 11, 30, 1, 139, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (37, 3, 3, 40, 31, 1, 140, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (37, 3, 3, 11, 30, 1, 140, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (38, 3, 3, 40, 31, 1, 30, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (38, 3, 3, 11, 30, 1, 30, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (39, 3, 3, 40, 31, 1, 31, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (39, 3, 3, 11, 30, 1, 31, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (40, 3, 3, 40, 31, 1, 141, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (40, 3, 3, 11, 30, 1, 141, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (41, 3, 3, 40, 31, 1, 142, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (41, 3, 3, 11, 30, 1, 142, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (42, 3, 3, 40, 31, 1, 143, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (42, 3, 3, 11, 30, 1, 143, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (43, 3, 3, 40, 31, 1, 144, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (43, 3, 3, 11, 30, 1, 144, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (44, 3, 3, 40, 31, 1, 145, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (44, 3, 3, 11, 30, 1, 145, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC3311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 3, 4, 40, 31, 1, 146, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (1, 3, 4, 11, 30, 1, 146, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 3, 4, 40, 31, 1, 103, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (2, 3, 4, 11, 30, 1, 103, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 3, 4, 40, 31, 1, 18, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (3, 3, 4, 11, 30, 1, 18, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 3, 4, 40, 31, 1, 124, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 3, 4, 11, 30, 1, 124, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 3, 4, 40, 31, 1, 102, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (5, 3, 4, 11, 30, 1, 102, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 3, 4, 40, 31, 1, 19, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (6, 3, 4, 11, 30, 1, 19, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 3, 4, 40, 31, 1, 21, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (7, 3, 4, 11, 30, 1, 21, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 3, 4, 40, 31, 1, 86, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (8, 3, 4, 11, 30, 1, 86, B'1', 1, '2020-07-10 17:30:03', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (9, 3, 4, 40, 31, 1, 108, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (9, 3, 4, 11, 30, 1, 108, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (4, 1, 4, 11, 29, 1, 5, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 1, 4, 11, 29, 1, 6, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 1, 4, 11, 29, 1, 173, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (7, 1, 4, 11, 29, 1, 174, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4111291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 1, 4, 40, 29, 1, 2, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 1, 4, 40, 29, 1, 3, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 1, 4, 40, 29, 1, 4, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (4, 1, 4, 40, 29, 1, 5, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (5, 1, 4, 40, 29, 1, 6, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (6, 1, 4, 40, 29, 1, 174, B'0', 1, '2020-12-14 15:50:07', NULL, NULL, NULL, NULL, 'active', 1, 'ACC4140291-1', 1, false, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 11, 4, 11, 30, 1, 8, NULL, 1, '2022-06-22 22:17:14', NULL, NULL, NULL, NULL, 'active', 1, 'ACC41111301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 11, 4, 40, 30, 1, 8, NULL, 1, '2022-06-22 22:18:41', NULL, NULL, NULL, NULL, 'active', 1, 'ACC41140301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 12, 4, 11, 30, 1, 8, NULL, 1, '2022-06-22 22:21:23', NULL, NULL, NULL, NULL, 'active', 1, 'ACC41211301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 12, 4, 40, 30, 1, 8, NULL, 1, '2022-06-22 22:23:00', NULL, NULL, NULL, NULL, 'active', 1, 'ACC41240301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 13, 4, 11, 30, 1, 16, NULL, 1, '2022-06-22 22:24:25', NULL, NULL, NULL, NULL, 'active', 1, 'ACC41311301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 13, 4, 40, 30, 1, 16, NULL, 1, '2022-06-22 22:25:20', NULL, NULL, NULL, NULL, 'active', 1, 'ACC41340301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 40, 30, 1, 43, NULL, 1, '2022-06-27 17:46:45', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 40, 30, 1, 40, NULL, 1, '2022-06-27 17:46:45', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3540301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 3, 11, 30, 1, 43, NULL, 1, '2022-06-27 17:49:11', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 3, 11, 30, 1, 40, NULL, 1, '2022-06-27 17:49:11', NULL, NULL, NULL, NULL, 'active', 2, 'ACC3511301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 11, 30, 1, 42, NULL, 1, '2022-06-27 17:50:49', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4511301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 5, 4, 11, 30, 1, 45, NULL, 1, '2022-06-27 17:50:49', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4511301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 5, 4, 11, 30, 1, 43, NULL, 1, '2022-06-27 17:50:49', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4511301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 5, 4, 40, 30, 1, 43, NULL, 1, '2022-06-27 17:51:20', NULL, NULL, NULL, NULL, 'active', 2, 'ACC4540301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (1, 4, 3, 11, 30, 1, 38, NULL, 1, '2022-06-27 17:53:11', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3411301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (2, 4, 3, 11, 30, 1, 39, NULL, 1, '2022-06-27 17:53:11', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3411301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (3, 4, 3, 11, 30, 1, 37, NULL, 1, '2022-06-27 17:53:11', NULL, NULL, NULL, NULL, 'active', 1, 'ACC3411301-1', 1, true, 1);
INSERT INTO plims.bsns_agent_condition VALUES (10, 3, 4, 40, 31, 1, 20, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (10, 3, 4, 11, 30, 1, 20, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (11, 3, 4, 40, 31, 1, 104, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (11, 3, 4, 11, 30, 1, 104, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (12, 3, 4, 40, 31, 1, 125, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (12, 3, 4, 11, 30, 1, 125, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (13, 3, 4, 40, 31, 1, 126, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (13, 3, 4, 11, 30, 1, 126, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (14, 3, 4, 40, 31, 1, 147, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (14, 3, 4, 11, 30, 1, 147, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (15, 3, 4, 40, 31, 1, 148, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (15, 3, 4, 11, 30, 1, 148, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (17, 3, 4, 40, 31, 1, 150, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (17, 3, 4, 11, 30, 1, 150, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (18, 3, 4, 40, 31, 1, 27, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (18, 3, 4, 11, 30, 1, 27, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (19, 3, 4, 40, 31, 1, 129, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (19, 3, 4, 11, 30, 1, 129, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (20, 3, 4, 40, 31, 1, 151, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (20, 3, 4, 11, 30, 1, 151, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (21, 3, 4, 40, 31, 1, 152, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (21, 3, 4, 11, 30, 1, 152, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (22, 3, 4, 40, 31, 1, 153, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (22, 3, 4, 11, 30, 1, 153, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (23, 3, 4, 40, 31, 1, 154, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (23, 3, 4, 11, 30, 1, 154, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (24, 3, 4, 40, 31, 1, 155, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (24, 3, 4, 11, 30, 1, 155, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (25, 3, 4, 40, 31, 1, 156, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (25, 3, 4, 11, 30, 1, 156, B'1', 1, '2020-07-10 17:30:04', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (29, 3, 4, 40, 31, 1, 134, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (29, 3, 4, 11, 30, 1, 134, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (30, 3, 4, 40, 31, 1, 157, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (30, 3, 4, 11, 30, 1, 157, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (31, 3, 4, 40, 31, 1, 158, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (31, 3, 4, 11, 30, 1, 158, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (32, 3, 4, 40, 31, 1, 94, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (32, 3, 4, 11, 30, 1, 94, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4311301-1', 1, true, 2);
INSERT INTO plims.bsns_agent_condition VALUES (33, 3, 4, 40, 31, 1, 159, B'1', 1, '2020-07-10 17:30:05', NULL, NULL, NULL, NULL, 'active', 1, 'L2ACC4340311-1', 1, true, 2);


--
-- TOC entry 3700 (class 0 OID 44764)
-- Dependencies: 259
-- Data for Name: bsns_agent_sample_reviewer; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3653 (class 0 OID 16461)
-- Dependencies: 212
-- Data for Name: bsns_composite_sample; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3655 (class 0 OID 16467)
-- Dependencies: 214
-- Data for Name: bsns_composite_sample_group; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3702 (class 0 OID 44916)
-- Dependencies: 261
-- Data for Name: bsns_consolidated_information; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3703 (class 0 OID 45101)
-- Dependencies: 262
-- Data for Name: bsns_consolidated_result; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3657 (class 0 OID 16473)
-- Dependencies: 216
-- Data for Name: bsns_cost; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3659 (class 0 OID 16479)
-- Dependencies: 218
-- Data for Name: bsns_crop; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_crop VALUES (3, 'Maize', 'Maize', 'Zea mays L.', 1, '2019-11-27 11:05:28', 1, '2022-09-13 17:37:28', NULL, NULL, 'active', 2);
INSERT INTO plims.bsns_crop VALUES (4, 'Small grain cereals', 'Small grain cereals', 'Wheat-Triticum|Bread wheat-Triticum aestivum L.|Durum wheat-Triticum durum L.|Barley-Hordeum vulgare|Triticale-X Triticosecale W.', 1, '2019-11-28 08:14:13', 1, '2022-09-13 17:53:29', NULL, NULL, 'active', 2);


--
-- TOC entry 3661 (class 0 OID 16485)
-- Dependencies: 220
-- Data for Name: bsns_essay; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_essay VALUES (5, 'ELISA', 'ELISA', 'ELISA', 'bacteria detection|virus detection', 1, '2019-11-25 14:42:04', 1, '2022-09-13 18:47:10', NULL, NULL, 'active', NULL, 26, NULL, 'EL', 'bg-assay-el');
INSERT INTO plims.bsns_essay VALUES (3, 'Blotter and freeze paper test', 'Blotter and freeze paper test', 'Blotter and freeze paper test', 'fungal detection', 1, '2019-11-25 14:41:30', 1, '2022-09-13 18:42:22', NULL, NULL, 'active', NULL, 26, NULL, 'BF', 'bg-assay-bf');
INSERT INTO plims.bsns_essay VALUES (8, 'PCR', 'PCR', 'PCR', '', 1, '2019-11-25 14:42:55', 1, '2022-09-13 18:43:43', NULL, NULL, 'active', NULL, 26, NULL, 'PC', 'bg-assay-pc');
INSERT INTO plims.bsns_essay VALUES (7, 'Wash test for bacteria', 'Wash test for bacteria', 'Wash test for bacteria', 'bacteria detection', 1, '2019-11-25 14:42:40', 1, '2022-09-13 18:45:55', NULL, NULL, 'active', NULL, 27, NULL, 'WT', 'bg-assay-wt');
INSERT INTO plims.bsns_essay VALUES (6, 'Germination test', 'Germination test', 'Germination test', 'detection of fungi, bacteria and viruses', 1, '2019-11-25 14:42:28', 1, '2022-09-13 18:48:04', NULL, NULL, 'active', NULL, 26, NULL, 'GR', 'bg-assay-gr');
INSERT INTO plims.bsns_essay VALUES (9, 'Gloeotinia temulenta', 'Gloeotinia temulenta', 'Gloeotinia temulenta', 'fungal detection', 1, '2020-07-17 14:40:13', 1, '2022-09-13 18:49:59', NULL, NULL, 'active', NULL, 27, NULL, 'GT', 'bg-assay-gt');
INSERT INTO plims.bsns_essay VALUES (4, 'Aniline blue test', 'Aniline blue test', 'Aniline blue test', '', 1, '2019-11-25 14:41:45', 1, '2022-06-23 22:25:50', NULL, NULL, 'active', NULL, 27, NULL, 'AB', 'bg-assay-ab');
INSERT INTO plims.bsns_essay VALUES (1, 'Visual inspection', 'Visual inspection', 'Visual inspection', 'fungal detection', 1, '2019-11-25 14:40:50', 1, '2022-09-13 18:39:45', NULL, NULL, 'active', NULL, 27, NULL, 'VI', 'bg-assay-vi');
INSERT INTO plims.bsns_essay VALUES (2, 'Washing and filtration', 'Washing and filtration', 'Washing and filtration', 'fungal detection', 1, '2019-11-25 14:41:12', 1, '2022-09-13 18:40:41', NULL, NULL, 'active', NULL, 27, NULL, 'WF', 'bg-assay-wf');
INSERT INTO plims.bsns_essay VALUES (11, 'Filtration and centrifugation', 'Filtration and centrifugation', 'Filtration and centrifugation', '', 1, '2022-06-22 18:04:31', 1, '2022-12-12 17:30:09', NULL, NULL, 'active', NULL, 27, NULL, 'FC', 'bg-assay-fc');
INSERT INTO plims.bsns_essay VALUES (12, 'Endosperm hydrolysis', 'Endosperm hydrolysis', 'Endosperm hydrolysis', '', 1, '2022-06-22 18:05:09', 1, '2022-12-12 17:30:39', NULL, NULL, 'active', NULL, 27, NULL, 'EH', 'bg-assay-eh');
INSERT INTO plims.bsns_essay VALUES (13, 'Staining test', 'Staining test', 'Staining test', 'fungal detection', 1, '2022-06-22 18:05:26', 1, '2022-12-12 17:31:26', NULL, NULL, 'active', NULL, 27, NULL, 'ST', 'bg-assay-st');


--
-- TOC entry 3662 (class 0 OID 16492)
-- Dependencies: 221
-- Data for Name: bsns_essay_by_workflow; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_essay_by_workflow VALUES (1, 1, 1, 1, '2019-11-27 08:53:58', 6, '2019-11-27 10:31:27', NULL, NULL, 'active');
INSERT INTO plims.bsns_essay_by_workflow VALUES (2, 1, 2, 1, '2019-11-27 08:53:58', 6, '2019-11-27 10:31:27', NULL, NULL, 'active');
INSERT INTO plims.bsns_essay_by_workflow VALUES (3, 1, 3, 1, '2019-11-27 08:53:58', 6, '2019-11-27 10:31:27', NULL, NULL, 'active');
INSERT INTO plims.bsns_essay_by_workflow VALUES (4, 1, 4, 1, '2019-11-27 08:53:58', 6, '2019-11-27 10:31:27', NULL, NULL, 'active');
INSERT INTO plims.bsns_essay_by_workflow VALUES (5, 1, 5, 1, '2019-11-27 08:53:58', 6, '2019-11-27 10:31:27', NULL, NULL, 'active');
INSERT INTO plims.bsns_essay_by_workflow VALUES (6, 1, 6, 1, '2019-11-27 08:53:58', 6, '2019-11-27 10:31:27', NULL, NULL, 'active');
INSERT INTO plims.bsns_essay_by_workflow VALUES (7, 1, 7, 1, '2019-11-27 08:53:58', 6, '2019-11-27 10:31:27', NULL, NULL, 'active');
INSERT INTO plims.bsns_essay_by_workflow VALUES (8, 1, 8, 1, '2019-11-27 08:53:58', 6, '2019-11-27 10:31:27', NULL, NULL, 'active');


--
-- TOC entry 3708 (class 0 OID 53116)
-- Dependencies: 267
-- Data for Name: bsns_institution; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_institution VALUES (2, 'CIMMYT', 'Centro Internacional de Mejoramiento de Maíz y Trigo', 'Hola asd', 1, '2023-02-15 17:05:40', 1, '2023-02-15 17:09:25', NULL, NULL, false);
INSERT INTO plims.bsns_institution VALUES (1, 'TI', 'token-institution', 'token-institution', 1, '2023-02-15 17:05:40', NULL, NULL, NULL, NULL, false);


--
-- TOC entry 3706 (class 0 OID 53094)
-- Dependencies: 265
-- Data for Name: bsns_laboratory; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_laboratory VALUES (2, 'SH', 'Seed Health Lab', 'Lab asdasd', 'SH', 2, 1, '2023-02-21 13:43:19', NULL, NULL, NULL, NULL, false);
INSERT INTO plims.bsns_laboratory VALUES (1, 'TL', 'token-lab', 'token-lab', 'TL', 1, 1, '2023-02-21 13:43:19', NULL, NULL, NULL, NULL, false);


--
-- TOC entry 3664 (class 0 OID 16497)
-- Dependencies: 223
-- Data for Name: bsns_location; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3666 (class 0 OID 16506)
-- Dependencies: 225
-- Data for Name: bsns_material; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3668 (class 0 OID 16515)
-- Dependencies: 227
-- Data for Name: bsns_material_group; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3699 (class 0 OID 17539)
-- Dependencies: 258
-- Data for Name: bsns_multi_request_event; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3670 (class 0 OID 16524)
-- Dependencies: 229
-- Data for Name: bsns_parameter; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_parameter VALUES (14, 'pay2', 'payment', 'request', 'full', 'full payment', 'full payment', 'label label-success', 1, '2019-12-03 13:46:52', 6, '2020-01-06 15:38:35', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (15, 'pay1', 'payment', 'request', 'partial', 'partial payment', 'partial payment', 'label label-warning', 1, '2019-12-03 13:48:11', 6, '2020-01-06 15:41:39', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (13, 'pay0', 'payment', 'request', 'incomplete ', 'incomplete payment', 'incomplete payment', 'label label-danger', 1, '2019-12-03 13:46:14', 6, '2020-01-06 15:41:58', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (1, 'undt', 'undetermianted', 'undetermianted', 'undetermianted', 'undetermianted', 'undetermianted', NULL, 1, '2019-11-25 14:31:24', 6, '2019-12-02 08:48:21', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (20, 'm0', 'material', 'request_process', 'empty', 'material empty', 'material empty', NULL, 1, '2019-12-03 15:16:02', 6, '2019-12-03 15:16:31', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (19, 'm1', 'material', 'request_process', 'loaded', 'material loaded', 'material loaded', NULL, 1, '2019-12-03 15:15:13', 6, '2019-12-03 15:16:49', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (21, 'pay00', 'payment', 'request_process', 'incomplete', 'incomplete payment', 'incomplete payment', NULL, 1, '2019-12-03 15:22:21', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (22, 'pay01', 'payment', 'request_process', 'partial', 'partial payment    ', 'partial payment ', NULL, 1, '2019-12-03 15:23:12', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (23, 'pay02', 'payment', 'request_process', 'full', 'full payment', 'full payment', NULL, 1, '2019-12-03 15:23:50', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (16, '2.1', 'status', 'request_process', 'pending', 'pending', 'pending', 'label label-gray-dark', 1, '2019-12-03 15:11:43', 10, '2020-02-17 10:22:13', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (26, 'mob_app', 'type', 'essay', 'mobile app source', 'results loaded by the mobile application', 'results loaded by the mobile application', 'fa fa-mobile', 1, '2020-01-06 10:04:31', 10, '2020-02-11 11:17:33', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (27, 'web_app', 'type', 'essay', 'web app source', 'results loaded by the web application', 'results loaded by the web application', 'fa fa-laptop', 1, '2020-01-06 10:05:23', 10, '2020-02-11 11:17:50', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (17, '2.2', 'status', 'request_process', 'in process', 'in process / doing', 'in process / doing', 'label label-primary', 1, '2019-12-03 15:12:09', 10, '2020-02-17 10:22:26', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (18, '2.3', 'status', 'request_process', 'finished', 'finished / done', 'finished / done', 'label label-active', 1, '2019-12-03 15:12:43', 10, '2020-02-17 10:22:30', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (41, '3.1', 'status', 'request_process_essay', 'pending', 'pending', 'pending', 'label label-gray-active', 1, '2020-02-12 14:52:56', 10, '2020-02-17 10:22:44', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (43, '3.3', 'status', 'request_process_essay', 'finished', 'finished', 'finished', 'label label-active', 1, '2020-02-12 14:54:01', 10, '2020-02-17 10:22:51', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (42, '3.2', 'status', 'request_process_essay', 'in process', 'in process / doing', 'in process / doing', 'label label-primary', 1, '2020-02-12 14:53:34', 10, '2020-02-17 10:23:06', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (59, 'at_3', 'type', 'agent', 'single numeric result', 'single numeric result', '', '', 1, '2020-03-02 15:42:10', 12, '2020-03-02 16:09:22', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (57, 'at_1', 'type', 'agent', 'single text result', 'single text result', '', '', 1, '2020-03-02 14:57:58', 12, '2020-03-02 16:08:32', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (62, 'set', 'property', 'activity', 'set', 'set values in database', 'set values in database', '', 1, '2020-03-03 13:31:27', 10, '2020-03-26 19:53:10', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (58, 'at_2', 'type', 'agent', 'single text information', 'single text information', '', 'label label-info', 1, '2020-03-02 14:58:17', 11, '2020-04-16 09:39:15', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (25, 'act-gcga', 'type', 'activity', 'grid column group activity', 'grid column group activity', '1 grid - in column group - 1 activity', '', 1, '2019-12-12 13:50:34', 10, '2020-03-23 16:41:56', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (33, 'act-grga', 'type', 'activity', 'grid row group activity', 'grid row group activity', '1 grid - in row group - 1 activity', '', 1, '2020-01-16 10:07:42', 10, '2020-03-23 16:42:03', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (63, 'act-gaa', 'type', 'activity', 'grid all activities', 'grid all activities', '1 grid - all activities', '', 1, '2020-03-25 10:53:31', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (67, 'rdt_1', 'type', 'reading_data', 'entry', 'entry', '', '', 1, '2020-04-13 17:14:03', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (68, 'rdt_2', 'type', 'reading_data', 'positive', 'positive', 'positive', '', 1, '2020-04-13 18:00:23', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (60, 'at_4', 'type', 'agent', 'single numeric information', 'single numeric information', '', 'label label-info', 1, '2020-03-02 15:42:40', 11, '2020-04-16 09:39:20', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (70, 'rdt_3', 'type', 'reading_data', 'negative', 'negative', '', '', 1, '2020-04-20 12:06:04', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (71, 'rdt_4', 'type', 'reading_data', 'buffer', 'buffer', '', '', 1, '2020-04-21 09:11:43', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (28, '1.2', 'status', 'request', 'verified entries', 'Verified Entries', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'label label-aqua', 1, '2020-01-06 10:29:45', 10, '2020-01-29 16:14:11', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (61, 'get', 'property', 'activity', 'get', 'get values in database', 'get values in database', NULL, 1, '2020-03-03 13:30:29', 10, '2020-03-26 19:53:08', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (29, 'CS3', 'type', 'composite_sample', '3.IND', 'Individual Sample', 'Individual Composite Sample', 'fa fa-th', 1, '2020-01-08 11:29:27', 10, '2020-02-24 13:17:28', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (31, 'CS2', 'type', 'composite_sample', '2.SMP', 'Simple Composite Sample', 'Simple Composite Sample', 'fa fa-th-large', 1, '2020-01-08 11:29:32', 10, '2020-02-24 13:22:11', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (47, '2.0', 'status', 'request', 'in stock', 'In Stock', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'label label-red', 1, '2020-02-24 13:27:12', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (32, 'CS0', 'type', 'composite_sample', '0.HDR', 'composite sample header type selected', 'composite sample header type selected', 'fa fa-exclamation-circle', 1, '2020-01-14 08:44:44', 10, '2020-02-27 08:44:25', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (148, 'agr3', 'group', 'agent', 'description of symptoms', 'description of symptoms', 'The results of these agents belong to the group of symptoms and their value is measurable or descriptive.', NULL, 1, '2022-05-24 19:19:31', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (69, 'agr1', 'group', 'agent', 'pathogens and others', 'pathogens and others', 'The results of these agents belong to the group of pathogens and their value is measurable.', NULL, 1, '2020-04-15 13:58:04', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (40, 'RT2', 'type', 'request', 'introduction', 'introduction', 'Introduction', '', 1, '2020-02-04 08:31:09', 10, '2020-04-02 16:37:37', NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (56, 'ss', 'type', 'request_process_essay', 'Soil', 'Soil', NULL, NULL, 1, '2020-03-02 13:38:57', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (65, 'wn', 'type', 'request_process_essay', 'Whatman Number 1 filter paper', 'Whatman Number 1 filter paper', NULL, NULL, 1, '2020-04-11 14:57:13', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (66, 'pm', 'type', 'request_process_essay', 'Polyester mesh (15um)', 'Polyester mesh (15um)', NULL, NULL, 1, '2020-04-11 14:57:58', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (72, 'mt1', 'type', 'material', 'seed', 'seed', '', '', 1, '2020-07-03 09:36:26', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (80, 'csgc1', 'small_grain_cereal', 'crop', 'Wheat', 'Wheat', '', '', 1, '2020-07-03 14:43:05', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (81, 'csgc2', 'small_grain_cereal', 'crop', 'Bread wheat', 'Bread wheat', '', '', 1, '2020-07-03 14:43:32', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (82, 'csgc3', 'small_grain_cereal', 'crop', 'Durum wheat', 'Durum wheat', '', '', 1, '2020-07-03 14:43:53', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (83, 'csgc4', 'small_grain_cereal', 'crop', 'Barley', 'Barley', '', '', 1, '2020-07-03 14:44:12', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (84, 'csgc5', 'small_grain_cereal', 'crop', 'Triticale', 'Triticale', '', '', 1, '2020-07-03 14:44:33', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (85, 'csgc6', 'small_grain_cereal', 'crop', 'Other', 'Other', '', '', 1, '2020-07-03 14:44:53', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (86, 'pdf', 'extension', 'request_document', 'pdf', 'pdf', 'pdf', 'fa fa-file-pdf-o', 1, '2020-07-27 09:48:34', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (87, 'xlsx', 'extension', 'request_document', 'xlsx', 'xlsx', 'xlsx', 'fa fa-file-excel-o', 1, '2020-07-27 09:49:39', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (88, 'xls', 'extension', 'request_document', 'xls', 'xls', 'xls', 'fa fa-file-excel-o', 1, '2020-07-27 09:50:05', 13, '2020-07-27 09:51:19', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (89, 'docx', 'extension', 'request_document', 'docx', 'docx', 'docx', 'fa fa-file-word-o', 1, '2020-07-27 09:50:32', 13, '2020-07-27 09:51:22', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (90, 'doc', 'extension', 'request_document', 'doc', 'doc', 'doc', 'fa fa-file-word-o', 1, '2020-07-27 09:50:59', 13, '2020-07-27 09:51:27', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (92, 'ppt', 'extension', 'request_document', 'ppt', 'ppt', 'ppt', 'fa fa-file-powerpoint-o', 1, '2020-07-27 09:53:27', 13, '2020-07-27 09:53:44', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (91, 'pptx', 'extension', 'request_document', 'pptx', 'pptx', 'pptx', 'fa fa-file-powerpoint-o', 1, '2020-07-27 09:53:11', 13, '2020-07-27 09:53:49', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (93, 'png', 'extension', 'request_document', 'png', 'png', 'png', 'fa fa-file-image-o', 1, '2020-08-21 15:24:34', 13, '2020-08-21 17:06:03', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (94, 'txt', 'extension', 'request_document', 'txt', 'txt', 'txt', 'fa fa-file-text-o', 1, '2020-08-21 15:26:26', 13, '2020-08-21 17:08:32', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (95, 'jpg', 'extension', 'request_document', 'jpg', 'jpg', 'jpg', 'fa fa-file-image-o', 1, '2020-08-21 17:10:25', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (96, 'csv', 'extension', 'request_document', 'csv', 'csv', 'csv', 'fa-file-excel-o', 1, '2020-08-21 17:11:06', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (97, 'msg', 'extension', 'request_document', 'msg', 'msg', 'msg', 'fa fa-envelope-o', 1, '2020-08-21 17:14:30', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (98, 'zip', 'extension', 'request_document', 'zip', 'zip', 'zip', 'fa fa-file-archive-o', 1, '2020-08-28 16:48:25', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (99, 'rar', 'extension', 'request_document', 'rar', 'rar', 'rar', 'fa fa-file-archive-o', 1, '2020-08-28 16:48:42', 13, '2020-08-28 16:49:04', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (45, '1.7', 'status', 'request', 'request reprocess generated', 'Request Reprocess Generated', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'label label-teal-active', 1, '2020-02-17 11:04:27', 10, '2020-02-24 13:57:29', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (12, '1.1', 'status', 'request', 'request created', 'Request Created', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'label label-fuchsia', 1, '2019-12-03 11:19:39', 10, '2020-01-29 16:13:32', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (141, '4.1', 'status', 'request_process_essay_activity', 'pending', 'pending', 'pending', 'label label-gray-active', 1, '2020-02-12 14:52:56', 10, '2020-02-17 10:22:44', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (142, '4.3', 'status', 'request_process_essay_activity', 'finished', 'finished', 'finished', 'label label-active', 1, '2020-02-12 14:54:01', 10, '2020-02-17 10:22:51', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (137, 'dele', 'status', 'material', 'REMOVED MATERIAL', 'REMOVED MATERIAL', 'deleted', 'text-red strike-through', 1, '2019-12-02 09:05:13', 6, '2019-12-03 08:58:58', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (46, '1.5', 'status', 'request', 'request in process', 'Request In Process', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'label label-blue', 1, '2020-02-20 15:12:54', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (140, '1.4', 'status', 'request', 'assays generated', 'Assays Generated', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'label label-purple', 1, '2022-01-19 12:15:20.584299', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (143, '4.2', 'status', 'request_process_essay_activity', 'in process', 'in process / doing', 'in process / doing', 'label label-primary', 1, '2020-02-12 14:53:34', 10, '2020-02-17 10:23:06', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (144, '5.1', 'status', 'request_process_essay_activity_sample', 'pending', 'pending', 'pending', 'label label-gray-active', 1, '2020-02-12 14:52:56', 10, '2020-02-17 10:22:44', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (145, '5.3', 'status', 'request_process_essay_activity_sample', 'finished', 'finished', 'finished', 'label label-active', 1, '2020-02-12 14:54:01', 10, '2020-02-17 10:22:51', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (146, '5.2', 'status', 'request_process_essay_activity_sample', 'in process', 'in process / doing', 'in process / doing', 'label label-primary', 1, '2020-02-12 14:53:34', 10, '2020-02-17 10:23:06', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (139, 'norm', 'status', 'material', 'SIMPLE MATERIAL', 'SIMPLE MATERIAL', 'normal', NULL, 1, '2019-12-02 09:05:13', 6, '2019-12-03 08:58:58', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (2, 'ps1', 'singularity', 'parameter', 'entity', 'entity', 'parameter configuration', NULL, 1, '2022-06-20 16:35:06.576717', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (159, 'pe10', 'entity', 'parameter', 'request_document', 'request_document', 'parameter configuration	', NULL, 1, '2022-06-20 16:48:02', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (138, 'inst', 'status', 'material', 'MATERIAL IN STOCK', 'MATERIAL IN STOCK', 'in stock', '', 1, '2019-12-02 09:05:13', 6, '2019-12-03 08:58:58', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (154, 'pe04', 'entity', 'parameter', 'crop', 'crop', 'parameter configuration	', NULL, 1, '2022-06-20 16:46:35', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (151, 'pe01', 'entity', 'parameter', 'activity', 'activity', 'parameter configuration	', NULL, 1, '2022-06-20 16:44:43.925351', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (5, 'pe02', 'entity', 'parameter', 'agent', 'agent', 'parameter configuration', NULL, 1, '2022-06-20 16:35:24.659393', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (153, 'pe03', 'entity', 'parameter', 'composite_sample', 'composite_sample', 'parameter configuration', NULL, 1, '2022-06-20 16:46:13', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (158, 'pe09', 'entity', 'parameter', 'request', 'request', 'parameter configuration	', NULL, 1, '2022-06-20 16:47:45', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (155, 'pe05', 'entity', 'parameter', 'essay', 'essay', 'parameter configuration	', NULL, 1, '2022-06-20 16:46:50', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (156, 'pe06', 'entity', 'parameter', 'material', 'material', 'parameter configuration	', NULL, 1, '2022-06-20 16:47:15', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (152, 'pe07', 'entity', 'parameter', 'parameter', 'parameter', 'parameter configuration	', NULL, 1, '2022-06-20 16:41:23', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (157, 'pe08', 'entity', 'parameter', 'reading_data', 'reading_data', 'parameter configuration', NULL, 1, '2022-06-20 16:47:32', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (77, 'mt6', 'type', 'material', 'soils', 'soils', '', '', 1, '2020-07-03 09:38:48', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (78, 'mt7', 'type', 'material', 'straw', 'straw', '', '', 1, '2020-07-03 09:39:06', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (79, 'mt8', 'type', 'material', 'DNA', 'DNA', '', '', 1, '2020-07-03 09:39:25', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (6, 'qtty', 'type', 'material', 'quantity', 'quantity', 'quantity', '', 1, '2019-12-02 08:47:34', 13, '2020-07-03 09:44:45', NULL, NULL, 'disabled', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (160, 'pe11', 'entity', 'parameter', 'request_process', 'request_process', 'parameter configuration	', NULL, 1, '2022-06-20 16:48:14', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (161, 'pe12', 'entity', 'parameter', 'request_process_essay', 'request_process_essay', 'parameter configuration	', NULL, 1, '2022-06-20 16:48:26', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (162, 'pe13', 'entity', 'parameter', 'request_process_essay_activity', 'request_process_essay_activity', 'parameter configuration	', NULL, 1, '2022-06-20 16:48:38', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (163, 'pe14', 'entity', 'parameter', 'request_process_essay_activity_sample', 'request_process_essay_activity_sample', 'parameter configuration	', NULL, 1, '2022-06-20 16:48:59', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (164, 'pe15', 'entity', 'parameter', 'undetermianted', 'undetermianted', 'parameter configuration	', NULL, 1, '2022-06-20 16:49:20', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (165, 'ps7', 'singularity', 'parameter', 'singularity', 'singularity', 'parameter configuration', NULL, 1, '2022-06-20 16:53:00.90484', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (166, 'ps2', 'singularity', 'parameter', 'extension', 'extension', 'parameter configuration	', NULL, 1, '2022-06-20 16:53:53', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (167, 'ps3', 'singularity', 'parameter', 'group', 'group', 'parameter configuration	', NULL, 1, '2022-06-20 16:54:07', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (168, 'ps4', 'singularity', 'parameter', 'material', 'material', 'parameter configuration	', NULL, 1, '2022-06-20 16:54:20', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (169, 'ps5', 'singularity', 'parameter', 'payment', 'payment', 'parameter configuration	', NULL, 1, '2022-06-20 16:54:35', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (170, 'ps6', 'singularity', 'parameter', 'property', 'property', 'parameter configuration	', NULL, 1, '2022-06-20 16:54:47', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (171, 'ps8', 'singularity', 'parameter', 'small_grain_cereal', 'small_grain_cereal', 'parameter configuration	', NULL, 1, '2022-06-20 16:55:00', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (172, 'ps9', 'singularity', 'parameter', 'status', 'status', 'parameter configuration	', NULL, 1, '2022-06-20 16:55:12', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (173, 'ps10', 'singularity', 'parameter', 'type', 'type', 'parameter configuration	', NULL, 1, '2022-06-20 16:55:28', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (174, 'ps11', 'singularity', 'parameter', 'undetermianted', 'undetermianted', 'parameter configuration	', NULL, 1, '2022-06-20 16:55:47', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (175, 'ps12', 'singularity', 'parameter', 'unit', 'unit', 'parameter configuration	', NULL, 1, '2022-06-20 16:56:06', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (176, 'pe16', 'entity', 'parameter', 'absorbance', 'absorbance', 'parameter configuration	', NULL, 1, '2022-06-20 16:58:52', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (177, 'ps13', 'singularity', 'parameter', 'configuration', 'configuration', 'parameter configuration	', NULL, 1, '2022-06-20 16:59:24', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (39, '1.3', 'status', 'request', 'samples generated', 'Samples Generated', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'label label-yellow', 1, '2020-01-29 16:14:40', 10, '2020-01-30 09:28:30', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (24, 'act-ga', 'type', 'activity', 'grid activity', 'grid activity', '1 grid - 1 activity', NULL, 1, '2019-12-12 13:49:16', 10, '2020-03-23 16:41:45', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (147, 'agr2', 'group', 'agent', 'measurable values of symptoms', 'measurable values of symptoms', 'The results of these agents belong to the group of symptoms and their value is measurable.', NULL, 1, '2022-05-24 19:18:55', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (187, 'sc02', 'configuration', 'system', 'system_name', 'system name', 'Web application name', NULL, 1, '2022-09-15 20:42:28', NULL, NULL, NULL, NULL, 'active', NULL, 'CIMMYT - SEED HEALTH LAB', NULL);
INSERT INTO plims.bsns_parameter VALUES (186, 'sc01', 'configuration', 'system', 'system_id', 'system id', 'Web Application Id', NULL, 1, '2022-09-15 20:41:21', NULL, NULL, NULL, NULL, 'active', NULL, 'SEEDHEALTHLAB', NULL);
INSERT INTO plims.bsns_parameter VALUES (188, 'rm1', 'status', 'consolidate', 'pending', 'pending', 'pending', 'label label-gray-dark', 1, '2022-11-29 16:08:57.991235', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (189, 'rm2', 'status', 'consolidate', 'in progress', 'in progress', 'in progress', 'label label-primary', 1, '2022-11-29 16:08:57.991235', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (190, 'rm3', 'status', 'consolidate', 'finished', 'finished', 'finished', 'label label-active', 1, '2022-11-29 16:08:57.991235', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (30, 'CS1', 'type', 'composite_sample', '1.GEN', 'General Composite Sample', 'General Composite Sample', 'fa fa-stop', 1, '2020-01-08 11:29:30', 10, '2020-02-24 13:17:31', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (44, '1.8', 'status', 'request', 'request ready for release', 'Request Ready for Release', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'label label-lime-active', 1, '2022-12-05 16:24:09.986437', 10, '2022-12-05 16:24:09.986437', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (192, '1.9', 'status', 'request', 'request finished', 'Request Finished', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'label label-lime-active', 1, '2022-12-05 16:24:09.818837', 10, '2022-12-05 16:24:09.818837', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (179, 'ac2', 'configuration', 'absorbance', 'initial_row', 'initial_row', NULL, NULL, 1, '2022-06-20 18:17:57', NULL, NULL, NULL, NULL, 'active', 0, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (178, 'ac1', 'configuration', 'absorbance', 'sheet', 'sheet', 'Initial sheet in an excel document', NULL, 1, '2022-06-20 17:47:59', NULL, NULL, NULL, NULL, 'active', 0, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (181, 'ac4', 'configuration', 'absorbance', 'initial_column', 'initial_column', NULL, NULL, 1, '2022-06-20 18:27:30', NULL, NULL, NULL, NULL, 'active', 0, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (182, 'ac5', 'configuration', 'absorbance', 'last_column', 'last_column', NULL, NULL, 1, '2022-06-20 18:28:05', NULL, NULL, NULL, NULL, 'active', 11, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (180, 'ac3', 'configuration', 'absorbance', 'last_row', 'last_row', NULL, NULL, 1, '2022-06-20 18:26:31', NULL, NULL, NULL, NULL, 'active', 7, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (136, 'ctrl', 'status', 'material', 'CONTROL MATERIAL', 'CONTROL MATERIAL', 'control', 'text-orange-link', 1, '2019-12-02 09:05:13', 6, '2019-12-03 08:58:58', NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (185, 'p17', 'entity', 'parameter', 'system', 'system', 'parameter configuration	', NULL, 1, '2022-09-15 20:37:18', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL);
INSERT INTO plims.bsns_parameter VALUES (11, 'RT1', 'type', 'request', 'shipment', 'shipment distribution', 'Miscellaneous Shipment and Distribution', '', 1, '2019-12-03 08:57:09', 11, '2020-04-19 23:46:04', NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (7, 'wght', 'type', 'material', 'weight', 'weight', 'weight', '', 1, '2019-12-02 08:51:40', 13, '2020-07-03 09:45:20', NULL, NULL, 'disabled', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (73, 'mt2', 'type', 'material', 'grain', 'grain', '', '', 1, '2020-07-03 09:36:39', 13, '2020-07-03 09:37:11', NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (8, 'seed', 'unit', 'material', 'seed', 'seed', 'seed', NULL, 1, '2019-12-02 09:04:32', 6, '2019-12-10 13:40:54', NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (9, 'grams', 'unit', 'material', 'grams', 'grams', 'grams', NULL, 1, '2019-12-02 09:05:13', 6, '2019-12-03 08:58:58', NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (50, 'pp', 'type', 'request_process_essay', 'Paper', 'Paper', NULL, NULL, 1, '2020-03-02 13:34:56', 12, '2020-03-02 13:39:12', NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (74, 'mt3', 'type', 'material', 'flour', 'flour', '', '', 1, '2020-07-03 09:37:33', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (75, 'mt4', 'type', 'material', 'lyophilized tissue', 'lyophilized tissue', '', '', 1, '2020-07-03 09:38:11', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);
INSERT INTO plims.bsns_parameter VALUES (76, 'mt5', 'type', 'material', 'plants', 'plants', '', '', 1, '2020-07-03 09:38:28', NULL, NULL, NULL, NULL, 'active', NULL, NULL, 2);


--
-- TOC entry 3672 (class 0 OID 16533)
-- Dependencies: 231
-- Data for Name: bsns_reading_data; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3674 (class 0 OID 16543)
-- Dependencies: 233
-- Data for Name: bsns_request; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3675 (class 0 OID 16551)
-- Dependencies: 234
-- Data for Name: bsns_request_detail; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3677 (class 0 OID 16559)
-- Dependencies: 236
-- Data for Name: bsns_request_documentation; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3678 (class 0 OID 16566)
-- Dependencies: 237
-- Data for Name: bsns_request_process; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3680 (class 0 OID 16575)
-- Dependencies: 239
-- Data for Name: bsns_request_process_essay; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3682 (class 0 OID 16585)
-- Dependencies: 241
-- Data for Name: bsns_request_process_essay_activity; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3684 (class 0 OID 16592)
-- Dependencies: 243
-- Data for Name: bsns_request_process_essay_activity_sample; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3686 (class 0 OID 16600)
-- Dependencies: 245
-- Data for Name: bsns_request_process_essay_agent; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3687 (class 0 OID 16605)
-- Dependencies: 246
-- Data for Name: bsns_status; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3689 (class 0 OID 16613)
-- Dependencies: 248
-- Data for Name: bsns_support; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3691 (class 0 OID 16619)
-- Dependencies: 250
-- Data for Name: bsns_support_master; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3693 (class 0 OID 16628)
-- Dependencies: 252
-- Data for Name: bsns_symptom; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_symptom VALUES (1, 'Undeterminated', 'Undeterminated', 'Undeterminated', 'Undeterminated', 1, '2020-01-23 11:27:09', 10, '2020-01-23 11:27:30', NULL, NULL, 'active');


--
-- TOC entry 3695 (class 0 OID 16637)
-- Dependencies: 254
-- Data for Name: bsns_workflow; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_workflow VALUES (1, 'Complete seed analysis', 'Complete seed analysis', 'Complete seed analysis', 'Complete seed analysis', 1, '2019-11-26 11:03:51', NULL, NULL, NULL, NULL, 'active', 1);
INSERT INTO plims.bsns_workflow VALUES (2, 'Request Reprocessing', 'Request Reprocessing', 'Request Reprocessing', 'Request Reprocessing', 1, '2020-02-17 10:53:01', NULL, NULL, NULL, NULL, 'active', 1);


--
-- TOC entry 3696 (class 0 OID 16644)
-- Dependencies: 255
-- Data for Name: bsns_workflow_by_crop; Type: TABLE DATA; Schema: plims; Owner: -
--

INSERT INTO plims.bsns_workflow_by_crop VALUES (1, 3, 1, 1, '2019-11-27 11:05:45', NULL, NULL, NULL, NULL, 'active');
INSERT INTO plims.bsns_workflow_by_crop VALUES (1, 4, 1, 1, '2019-11-28 08:16:20', NULL, NULL, NULL, NULL, 'active');


--
-- TOC entry 3697 (class 0 OID 16647)
-- Dependencies: 256
-- Data for Name: mail_composition; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3698 (class 0 OID 16653)
-- Dependencies: 257
-- Data for Name: mail_notification; Type: TABLE DATA; Schema: plims; Owner: -
--



--
-- TOC entry 3719 (class 0 OID 0)
-- Dependencies: 201
-- Name: activity_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.activity_id_seq', 2, true);


--
-- TOC entry 3720 (class 0 OID 0)
-- Dependencies: 202
-- Name: agent_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.agent_id_seq', 177, true);


--
-- TOC entry 3721 (class 0 OID 0)
-- Dependencies: 268
-- Name: bsns_parameter_by_institution_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.bsns_parameter_by_institution_id_seq', 1, false);


--
-- TOC entry 3722 (class 0 OID 0)
-- Dependencies: 213
-- Name: composite_sample_group_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.composite_sample_group_id_seq', 565, true);


--
-- TOC entry 3723 (class 0 OID 0)
-- Dependencies: 211
-- Name: composite_sample_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.composite_sample_id_seq', 141087, true);


--
-- TOC entry 3724 (class 0 OID 0)
-- Dependencies: 260
-- Name: consolidate_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.consolidate_id_seq', 334, true);


--
-- TOC entry 3725 (class 0 OID 0)
-- Dependencies: 215
-- Name: cost_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.cost_id_seq', 1, false);


--
-- TOC entry 3726 (class 0 OID 0)
-- Dependencies: 217
-- Name: crop_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.crop_id_seq', 4, true);


--
-- TOC entry 3727 (class 0 OID 0)
-- Dependencies: 219
-- Name: essay_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.essay_id_seq', 14, true);


--
-- TOC entry 3728 (class 0 OID 0)
-- Dependencies: 266
-- Name: institution_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.institution_id_seq', 6, true);


--
-- TOC entry 3729 (class 0 OID 0)
-- Dependencies: 263
-- Name: laboratory_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.laboratory_id_seq', 14, true);


--
-- TOC entry 3730 (class 0 OID 0)
-- Dependencies: 222
-- Name: location_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.location_id_seq', 1, false);


--
-- TOC entry 3731 (class 0 OID 0)
-- Dependencies: 226
-- Name: material_group_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.material_group_seq', 45696, true);


--
-- TOC entry 3732 (class 0 OID 0)
-- Dependencies: 224
-- Name: material_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.material_id_seq', 170674, true);


--
-- TOC entry 3733 (class 0 OID 0)
-- Dependencies: 228
-- Name: parameter_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.parameter_id_seq', 194, true);


--
-- TOC entry 3734 (class 0 OID 0)
-- Dependencies: 230
-- Name: reading_data_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.reading_data_seq', 433776, true);


--
-- TOC entry 3735 (class 0 OID 0)
-- Dependencies: 235
-- Name: request_documentation_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.request_documentation_id_seq', 488, true);


--
-- TOC entry 3736 (class 0 OID 0)
-- Dependencies: 232
-- Name: request_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.request_id_seq', 564, true);


--
-- TOC entry 3737 (class 0 OID 0)
-- Dependencies: 240
-- Name: request_process_essay_activity_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.request_process_essay_activity_seq', 7191, true);


--
-- TOC entry 3738 (class 0 OID 0)
-- Dependencies: 244
-- Name: request_process_essay_agent_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.request_process_essay_agent_seq', 15331, true);


--
-- TOC entry 3739 (class 0 OID 0)
-- Dependencies: 242
-- Name: request_process_essay_sample_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.request_process_essay_sample_seq', 727390, true);


--
-- TOC entry 3740 (class 0 OID 0)
-- Dependencies: 238
-- Name: request_process_essay_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.request_process_essay_seq', 3307, true);


--
-- TOC entry 3741 (class 0 OID 0)
-- Dependencies: 264
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.role_id_seq', 4, true);


--
-- TOC entry 3742 (class 0 OID 0)
-- Dependencies: 249
-- Name: support_master_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.support_master_id_seq', 191, true);


--
-- TOC entry 3743 (class 0 OID 0)
-- Dependencies: 247
-- Name: support_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.support_seq', 5409, true);


--
-- TOC entry 3744 (class 0 OID 0)
-- Dependencies: 251
-- Name: symptom_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.symptom_id_seq', 1, false);


--
-- TOC entry 3745 (class 0 OID 0)
-- Dependencies: 269
-- Name: task_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.task_id_seq', 18, true);


--
-- TOC entry 3746 (class 0 OID 0)
-- Dependencies: 203
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.user_id_seq', 2, true);


--
-- TOC entry 3747 (class 0 OID 0)
-- Dependencies: 205
-- Name: user_refresh_tokenid_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.user_refresh_tokenid_seq', 181, true);


--
-- TOC entry 3748 (class 0 OID 0)
-- Dependencies: 253
-- Name: workflow_id_seq; Type: SEQUENCE SET; Schema: plims; Owner: -
--

SELECT pg_catalog.setval('plims.workflow_id_seq', 1, false);


--
-- TOC entry 3338 (class 2606 OID 61228)
-- Name: auth_role auth_role_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_role
    ADD CONSTRAINT auth_role_pk PRIMARY KEY (role_id);


--
-- TOC entry 3340 (class 2606 OID 61394)
-- Name: auth_task auth_task_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_task
    ADD CONSTRAINT auth_task_pk PRIMARY KEY (task_id);


--
-- TOC entry 3342 (class 2606 OID 61432)
-- Name: auth_user_by_role_institution_laboratory auth_user_by_role_institution_laboratory_uq; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user_by_role_institution_laboratory
    ADD CONSTRAINT auth_user_by_role_institution_laboratory_uq UNIQUE (user_id, role_id, institution_id, laboratory_id);


--
-- TOC entry 3158 (class 2606 OID 16661)
-- Name: auth_user auth_user_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user
    ADD CONSTRAINT auth_user_pk PRIMARY KEY (user_id);


--
-- TOC entry 3160 (class 2606 OID 16663)
-- Name: auth_user auth_user_un; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user
    ADD CONSTRAINT auth_user_un UNIQUE (username);


--
-- TOC entry 3162 (class 2606 OID 16665)
-- Name: auth_user auth_user_un1; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user
    ADD CONSTRAINT auth_user_un1 UNIQUE (username);


--
-- TOC entry 3168 (class 2606 OID 16667)
-- Name: bsns_activity_by_essay bsns_activity_by_essay_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_activity_by_essay
    ADD CONSTRAINT bsns_activity_by_essay_pk PRIMARY KEY (activity_id, essay_id);


--
-- TOC entry 3166 (class 2606 OID 16669)
-- Name: bsns_activity bsns_activity_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_activity
    ADD CONSTRAINT bsns_activity_pk PRIMARY KEY (activity_id);


--
-- TOC entry 3177 (class 2606 OID 61454)
-- Name: bsns_agent_condition bsns_agent_condition_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_condition
    ADD CONSTRAINT bsns_agent_condition_pk PRIMARY KEY (num_order, essay_id, crop_id, request_type_id, composite_sample_type_id, request_process_essay_type_id, agent_id, laboratory_id);


--
-- TOC entry 3170 (class 2606 OID 16673)
-- Name: bsns_agent bsns_agent_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent
    ADD CONSTRAINT bsns_agent_pk PRIMARY KEY (agent_id);


--
-- TOC entry 3189 (class 2606 OID 16675)
-- Name: bsns_composite_sample_group bsns_composite_sample_group_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_composite_sample_group
    ADD CONSTRAINT bsns_composite_sample_group_pk PRIMARY KEY (composite_sample_group_id);


--
-- TOC entry 3185 (class 2606 OID 16677)
-- Name: bsns_composite_sample bsns_composite_sample_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_composite_sample
    ADD CONSTRAINT bsns_composite_sample_pk PRIMARY KEY (composite_sample_id);


--
-- TOC entry 3332 (class 2606 OID 44921)
-- Name: bsns_consolidated_information bsns_consolidated_information_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_information
    ADD CONSTRAINT bsns_consolidated_information_pk PRIMARY KEY (consolidate_id);


--
-- TOC entry 3191 (class 2606 OID 16679)
-- Name: bsns_cost bsns_cost_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_cost
    ADD CONSTRAINT bsns_cost_pk PRIMARY KEY (cost_id);


--
-- TOC entry 3193 (class 2606 OID 16681)
-- Name: bsns_crop bsns_crop_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_crop
    ADD CONSTRAINT bsns_crop_pk PRIMARY KEY (crop_id);


--
-- TOC entry 3197 (class 2606 OID 16683)
-- Name: bsns_essay_by_workflow bsns_essay_by_workflow_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_essay_by_workflow
    ADD CONSTRAINT bsns_essay_by_workflow_pk PRIMARY KEY (essay_id, workflow_id);


--
-- TOC entry 3195 (class 2606 OID 16685)
-- Name: bsns_essay bsns_essay_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_essay
    ADD CONSTRAINT bsns_essay_pk PRIMARY KEY (essay_id);


--
-- TOC entry 3336 (class 2606 OID 53124)
-- Name: bsns_institution bsns_institution_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_institution
    ADD CONSTRAINT bsns_institution_pk PRIMARY KEY (institution_id);


--
-- TOC entry 3334 (class 2606 OID 53102)
-- Name: bsns_laboratory bsns_laboratory_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_laboratory
    ADD CONSTRAINT bsns_laboratory_pk PRIMARY KEY (laboratory_id);


--
-- TOC entry 3199 (class 2606 OID 16687)
-- Name: bsns_location bsns_location_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_location
    ADD CONSTRAINT bsns_location_pk PRIMARY KEY (location_id);


--
-- TOC entry 3209 (class 2606 OID 16689)
-- Name: bsns_material bsns_material_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT bsns_material_pk PRIMARY KEY (material_id);


--
-- TOC entry 3214 (class 2606 OID 16691)
-- Name: bsns_parameter bsns_parameter_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_parameter
    ADD CONSTRAINT bsns_parameter_pk PRIMARY KEY (parameter_id);


--
-- TOC entry 3216 (class 2606 OID 16693)
-- Name: bsns_parameter bsns_parameter_un; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_parameter
    ADD CONSTRAINT bsns_parameter_un UNIQUE (code);


--
-- TOC entry 3229 (class 2606 OID 16695)
-- Name: bsns_request_detail bsns_request_detail_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_detail
    ADD CONSTRAINT bsns_request_detail_pk PRIMARY KEY (request_id);


--
-- TOC entry 3235 (class 2606 OID 16697)
-- Name: bsns_request_documentation bsns_request_documentation_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_documentation
    ADD CONSTRAINT bsns_request_documentation_pk PRIMARY KEY (request_documentation_id);


--
-- TOC entry 3221 (class 2606 OID 16699)
-- Name: bsns_request bsns_request_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request
    ADD CONSTRAINT bsns_request_pk PRIMARY KEY (request_id);


--
-- TOC entry 3242 (class 2606 OID 16701)
-- Name: bsns_request_process bsns_request_process_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT bsns_request_process_pk PRIMARY KEY (request_id, num_order_id);


--
-- TOC entry 3330 (class 2606 OID 44772)
-- Name: bsns_agent_sample_reviewer bsns_sample_by_agent_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_sample_reviewer
    ADD CONSTRAINT bsns_sample_by_agent_pk PRIMARY KEY (composite_sample_id, agent_id);


--
-- TOC entry 3322 (class 2606 OID 16703)
-- Name: bsns_support_master bsns_support_master_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support_master
    ADD CONSTRAINT bsns_support_master_pk PRIMARY KEY (support_master_id);


--
-- TOC entry 3324 (class 2606 OID 16705)
-- Name: bsns_symptom bsns_symptom_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_symptom
    ADD CONSTRAINT bsns_symptom_pk PRIMARY KEY (symptom_id);


--
-- TOC entry 3328 (class 2606 OID 16707)
-- Name: bsns_workflow_by_crop bsns_workflow_by_crop_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_workflow_by_crop
    ADD CONSTRAINT bsns_workflow_by_crop_pk PRIMARY KEY (workflow_id, crop_id);


--
-- TOC entry 3326 (class 2606 OID 16709)
-- Name: bsns_workflow bsns_workflow_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_workflow
    ADD CONSTRAINT bsns_workflow_pk PRIMARY KEY (workflow_id);


--
-- TOC entry 3212 (class 2606 OID 16711)
-- Name: bsns_material_group material_group_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material_group
    ADD CONSTRAINT material_group_pk PRIMARY KEY (material_group_id);


--
-- TOC entry 3218 (class 2606 OID 16713)
-- Name: bsns_reading_data reading_data_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT reading_data_pk PRIMARY KEY (reading_data_id);


--
-- TOC entry 3280 (class 2606 OID 16715)
-- Name: bsns_request_process_essay_activity request_process_essay_activity_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT request_process_essay_activity_pk PRIMARY KEY (request_process_essay_activity_id);


--
-- TOC entry 3307 (class 2606 OID 16717)
-- Name: bsns_request_process_essay_agent request_process_essay_agent_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_agent
    ADD CONSTRAINT request_process_essay_agent_pk PRIMARY KEY (request_process_essay_agent_id);


--
-- TOC entry 3263 (class 2606 OID 16719)
-- Name: bsns_request_process_essay request_process_essay_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT request_process_essay_pk PRIMARY KEY (request_process_essay_id);


--
-- TOC entry 3297 (class 2606 OID 16721)
-- Name: bsns_request_process_essay_activity_sample request_process_essay_sample_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT request_process_essay_sample_pk PRIMARY KEY (request_process_essay_activity_sample_id);


--
-- TOC entry 3309 (class 2606 OID 16723)
-- Name: bsns_status status_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_status
    ADD CONSTRAINT status_pk PRIMARY KEY (request_id, request_status_id);


--
-- TOC entry 3320 (class 2606 OID 16725)
-- Name: bsns_support support_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT support_pk PRIMARY KEY (support_id);


--
-- TOC entry 3164 (class 2606 OID 16727)
-- Name: auth_user_refresh_tokens user_refresh_tokens_pk; Type: CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user_refresh_tokens
    ADD CONSTRAINT user_refresh_tokens_pk PRIMARY KEY (user_refresh_tokenid);


--
-- TOC entry 3171 (class 1259 OID 16728)
-- Name: bsns_agent_condition_agent_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_agent_condition_agent_id_idx ON plims.bsns_agent_condition USING btree (agent_id);


--
-- TOC entry 3172 (class 1259 OID 16729)
-- Name: bsns_agent_condition_composite_sample_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_agent_condition_composite_sample_type_id_idx ON plims.bsns_agent_condition USING btree (composite_sample_type_id);


--
-- TOC entry 3173 (class 1259 OID 16730)
-- Name: bsns_agent_condition_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_agent_condition_crop_id_idx ON plims.bsns_agent_condition USING btree (crop_id);


--
-- TOC entry 3174 (class 1259 OID 16731)
-- Name: bsns_agent_condition_essay_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_agent_condition_essay_id_idx ON plims.bsns_agent_condition USING btree (essay_id);


--
-- TOC entry 3175 (class 1259 OID 61455)
-- Name: bsns_agent_condition_laboratory_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_agent_condition_laboratory_id_idx ON plims.bsns_agent_condition USING btree (laboratory_id);


--
-- TOC entry 3178 (class 1259 OID 16732)
-- Name: bsns_agent_condition_request_process_essay_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_agent_condition_request_process_essay_type_id_idx ON plims.bsns_agent_condition USING btree (request_process_essay_type_id);


--
-- TOC entry 3179 (class 1259 OID 16733)
-- Name: bsns_agent_condition_request_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_agent_condition_request_type_id_idx ON plims.bsns_agent_condition USING btree (request_type_id);


--
-- TOC entry 3180 (class 1259 OID 16734)
-- Name: bsns_composite_sample_composite_sample_group_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_composite_sample_composite_sample_group_id_idx ON plims.bsns_composite_sample USING btree (composite_sample_group_id);


--
-- TOC entry 3181 (class 1259 OID 16735)
-- Name: bsns_composite_sample_composite_sample_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_composite_sample_composite_sample_type_id_idx ON plims.bsns_composite_sample USING btree (composite_sample_type_id);


--
-- TOC entry 3182 (class 1259 OID 16736)
-- Name: bsns_composite_sample_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_composite_sample_crop_id_idx ON plims.bsns_composite_sample USING btree (crop_id);


--
-- TOC entry 3183 (class 1259 OID 16737)
-- Name: bsns_composite_sample_material_group_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_composite_sample_material_group_id_idx ON plims.bsns_composite_sample USING btree (material_group_id);


--
-- TOC entry 3186 (class 1259 OID 16738)
-- Name: bsns_composite_sample_reading_data_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_composite_sample_reading_data_type_id_idx ON plims.bsns_composite_sample USING btree (reading_data_type_id);


--
-- TOC entry 3200 (class 1259 OID 16739)
-- Name: bsns_material_composite_sample_group_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_material_composite_sample_group_id_idx ON plims.bsns_material USING btree (composite_sample_group_id);


--
-- TOC entry 3201 (class 1259 OID 16740)
-- Name: bsns_material_composite_sample_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_material_composite_sample_id_idx ON plims.bsns_material USING btree (composite_sample_id);


--
-- TOC entry 3202 (class 1259 OID 16741)
-- Name: bsns_material_composite_sample_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_material_composite_sample_type_id_idx ON plims.bsns_material USING btree (composite_sample_type_id);


--
-- TOC entry 3203 (class 1259 OID 16742)
-- Name: bsns_material_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_material_crop_id_idx ON plims.bsns_material USING btree (crop_id);


--
-- TOC entry 3204 (class 1259 OID 16743)
-- Name: bsns_material_material_group_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_material_material_group_id_idx ON plims.bsns_material USING btree (material_group_id);


--
-- TOC entry 3205 (class 1259 OID 16744)
-- Name: bsns_material_material_status_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_material_material_status_id_idx ON plims.bsns_material USING btree (material_status_id);


--
-- TOC entry 3206 (class 1259 OID 16745)
-- Name: bsns_material_material_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_material_material_type_id_idx ON plims.bsns_material USING btree (material_type_id);


--
-- TOC entry 3207 (class 1259 OID 16746)
-- Name: bsns_material_material_unit_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_material_material_unit_id_idx ON plims.bsns_material USING btree (material_unit_id);


--
-- TOC entry 3210 (class 1259 OID 16747)
-- Name: bsns_material_registered_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_material_registered_by_idx ON plims.bsns_material USING btree (registered_by);


--
-- TOC entry 3226 (class 1259 OID 16748)
-- Name: bsns_request_detail_consignee_user_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_detail_consignee_user_idx ON plims.bsns_request_detail USING btree (consignee_user);


--
-- TOC entry 3227 (class 1259 OID 16749)
-- Name: bsns_request_detail_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_detail_crop_id_idx ON plims.bsns_request_detail USING btree (crop_id);


--
-- TOC entry 3230 (class 1259 OID 16750)
-- Name: bsns_request_detail_request_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_detail_request_id_idx ON plims.bsns_request_detail USING btree (request_id);


--
-- TOC entry 3231 (class 1259 OID 16751)
-- Name: bsns_request_detail_requested_user_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_detail_requested_user_idx ON plims.bsns_request_detail USING btree (requested_user);


--
-- TOC entry 3232 (class 1259 OID 16752)
-- Name: bsns_request_documentation_essay_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_documentation_essay_id_idx ON plims.bsns_request_documentation USING btree (essay_id);


--
-- TOC entry 3233 (class 1259 OID 16753)
-- Name: bsns_request_documentation_file_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_documentation_file_type_id_idx ON plims.bsns_request_documentation USING btree (file_type_id);


--
-- TOC entry 3236 (class 1259 OID 16754)
-- Name: bsns_request_documentation_request_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_documentation_request_id_idx ON plims.bsns_request_documentation USING btree (request_id);


--
-- TOC entry 3219 (class 1259 OID 16755)
-- Name: bsns_request_owner_user_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_owner_user_id_idx ON plims.bsns_request USING btree (owner_user_id);


--
-- TOC entry 3237 (class 1259 OID 16756)
-- Name: bsns_request_process_check_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_check_by_idx ON plims.bsns_request_process USING btree (check_by);


--
-- TOC entry 3238 (class 1259 OID 16757)
-- Name: bsns_request_process_composite_sample_group_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_composite_sample_group_id_idx ON plims.bsns_request_process USING btree (composite_sample_group_id);


--
-- TOC entry 3239 (class 1259 OID 16758)
-- Name: bsns_request_process_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_crop_id_idx ON plims.bsns_request_process USING btree (crop_id);


--
-- TOC entry 3281 (class 1259 OID 16759)
-- Name: bsns_request_process_essay_ac_request_process_essay_activi_idx1; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_ac_request_process_essay_activi_idx1 ON plims.bsns_request_process_essay_activity_sample USING btree (request_process_essay_activity_sample_status_id);


--
-- TOC entry 3264 (class 1259 OID 16760)
-- Name: bsns_request_process_essay_ac_request_process_essay_activi_idx2; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_ac_request_process_essay_activi_idx2 ON plims.bsns_request_process_essay_activity USING btree (request_process_essay_activity_status_id);


--
-- TOC entry 3282 (class 1259 OID 16761)
-- Name: bsns_request_process_essay_ac_request_process_essay_activit_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_ac_request_process_essay_activit_idx ON plims.bsns_request_process_essay_activity_sample USING btree (request_process_essay_activity_id);


--
-- TOC entry 3283 (class 1259 OID 16762)
-- Name: bsns_request_process_essay_activi_composite_sample_group_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activi_composite_sample_group_id_idx ON plims.bsns_request_process_essay_activity_sample USING btree (composite_sample_group_id);


--
-- TOC entry 3265 (class 1259 OID 16763)
-- Name: bsns_request_process_essay_activi_composite_sample_type_id_idx1; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activi_composite_sample_type_id_idx1 ON plims.bsns_request_process_essay_activity USING btree (composite_sample_type_id);


--
-- TOC entry 3266 (class 1259 OID 16764)
-- Name: bsns_request_process_essay_activi_request_process_essay_id_idx1; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activi_request_process_essay_id_idx1 ON plims.bsns_request_process_essay_activity USING btree (request_process_essay_id);


--
-- TOC entry 3284 (class 1259 OID 16765)
-- Name: bsns_request_process_essay_activit_composite_sample_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activit_composite_sample_type_id_idx ON plims.bsns_request_process_essay_activity_sample USING btree (composite_sample_type_id);


--
-- TOC entry 3285 (class 1259 OID 16766)
-- Name: bsns_request_process_essay_activit_request_process_essay_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activit_request_process_essay_id_idx ON plims.bsns_request_process_essay_activity_sample USING btree (request_process_essay_id);


--
-- TOC entry 3267 (class 1259 OID 16767)
-- Name: bsns_request_process_essay_activity_activity_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_activity_id_idx ON plims.bsns_request_process_essay_activity USING btree (activity_id);


--
-- TOC entry 3268 (class 1259 OID 16768)
-- Name: bsns_request_process_essay_activity_check_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_check_by_idx ON plims.bsns_request_process_essay_activity USING btree (check_by);


--
-- TOC entry 3269 (class 1259 OID 16769)
-- Name: bsns_request_process_essay_activity_control_location_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_control_location_id_idx ON plims.bsns_request_process_essay_activity USING btree (control_location_id);


--
-- TOC entry 3270 (class 1259 OID 16770)
-- Name: bsns_request_process_essay_activity_control_user_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_control_user_id_idx ON plims.bsns_request_process_essay_activity USING btree (control_user_id);


--
-- TOC entry 3271 (class 1259 OID 16771)
-- Name: bsns_request_process_essay_activity_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_crop_id_idx ON plims.bsns_request_process_essay_activity USING btree (crop_id);


--
-- TOC entry 3272 (class 1259 OID 16772)
-- Name: bsns_request_process_essay_activity_essay_id_activity_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_essay_id_activity_id_idx ON plims.bsns_request_process_essay_activity USING btree (essay_id, activity_id);


--
-- TOC entry 3273 (class 1259 OID 16773)
-- Name: bsns_request_process_essay_activity_essay_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_essay_id_idx ON plims.bsns_request_process_essay_activity USING btree (essay_id);


--
-- TOC entry 3274 (class 1259 OID 16774)
-- Name: bsns_request_process_essay_activity_finish_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_finish_by_idx ON plims.bsns_request_process_essay_activity USING btree (finish_by);


--
-- TOC entry 3275 (class 1259 OID 16775)
-- Name: bsns_request_process_essay_activity_request_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_request_id_idx ON plims.bsns_request_process_essay_activity USING btree (request_id);


--
-- TOC entry 3276 (class 1259 OID 16776)
-- Name: bsns_request_process_essay_activity_request_id_num_order_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_request_id_num_order_id_idx ON plims.bsns_request_process_essay_activity USING btree (request_id, num_order_id);


--
-- TOC entry 3286 (class 1259 OID 16777)
-- Name: bsns_request_process_essay_activity_sam_composite_sample_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sam_composite_sample_id_idx ON plims.bsns_request_process_essay_activity_sample USING btree (composite_sample_id);


--
-- TOC entry 3287 (class 1259 OID 16778)
-- Name: bsns_request_process_essay_activity_sample_activity_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sample_activity_id_idx ON plims.bsns_request_process_essay_activity_sample USING btree (activity_id);


--
-- TOC entry 3288 (class 1259 OID 16779)
-- Name: bsns_request_process_essay_activity_sample_activity_id_idx1; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sample_activity_id_idx1 ON plims.bsns_request_process_essay_activity_sample USING btree (activity_id);


--
-- TOC entry 3289 (class 1259 OID 16780)
-- Name: bsns_request_process_essay_activity_sample_check_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sample_check_by_idx ON plims.bsns_request_process_essay_activity_sample USING btree (check_by);


--
-- TOC entry 3290 (class 1259 OID 16781)
-- Name: bsns_request_process_essay_activity_sample_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sample_crop_id_idx ON plims.bsns_request_process_essay_activity_sample USING btree (crop_id);


--
-- TOC entry 3291 (class 1259 OID 16782)
-- Name: bsns_request_process_essay_activity_sample_essay_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sample_essay_id_idx ON plims.bsns_request_process_essay_activity_sample USING btree (essay_id);


--
-- TOC entry 3292 (class 1259 OID 16783)
-- Name: bsns_request_process_essay_activity_sample_finish_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sample_finish_by_idx ON plims.bsns_request_process_essay_activity_sample USING btree (finish_by);


--
-- TOC entry 3293 (class 1259 OID 16784)
-- Name: bsns_request_process_essay_activity_sample_request_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sample_request_id_idx ON plims.bsns_request_process_essay_activity_sample USING btree (request_id);


--
-- TOC entry 3294 (class 1259 OID 16785)
-- Name: bsns_request_process_essay_activity_sample_start_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sample_start_by_idx ON plims.bsns_request_process_essay_activity_sample USING btree (start_by);


--
-- TOC entry 3295 (class 1259 OID 16786)
-- Name: bsns_request_process_essay_activity_sample_workflow_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_sample_workflow_id_idx ON plims.bsns_request_process_essay_activity_sample USING btree (workflow_id);


--
-- TOC entry 3277 (class 1259 OID 16787)
-- Name: bsns_request_process_essay_activity_start_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_start_by_idx ON plims.bsns_request_process_essay_activity USING btree (start_by);


--
-- TOC entry 3278 (class 1259 OID 16788)
-- Name: bsns_request_process_essay_activity_workflow_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_activity_workflow_id_idx ON plims.bsns_request_process_essay_activity USING btree (workflow_id);


--
-- TOC entry 3298 (class 1259 OID 16789)
-- Name: bsns_request_process_essay_agent_agent_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_agent_agent_id_idx ON plims.bsns_request_process_essay_agent USING btree (agent_id);


--
-- TOC entry 3299 (class 1259 OID 16790)
-- Name: bsns_request_process_essay_agent_composite_sample_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_agent_composite_sample_type_id_idx ON plims.bsns_request_process_essay_agent USING btree (composite_sample_type_id);


--
-- TOC entry 3300 (class 1259 OID 16791)
-- Name: bsns_request_process_essay_agent_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_agent_crop_id_idx ON plims.bsns_request_process_essay_agent USING btree (crop_id);


--
-- TOC entry 3301 (class 1259 OID 16792)
-- Name: bsns_request_process_essay_agent_essay_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_agent_essay_id_idx ON plims.bsns_request_process_essay_agent USING btree (essay_id);


--
-- TOC entry 3302 (class 1259 OID 16793)
-- Name: bsns_request_process_essay_agent_request_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_agent_request_id_idx ON plims.bsns_request_process_essay_agent USING btree (request_id);


--
-- TOC entry 3303 (class 1259 OID 16794)
-- Name: bsns_request_process_essay_agent_request_id_idx1; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_agent_request_id_idx1 ON plims.bsns_request_process_essay_agent USING btree (request_id);


--
-- TOC entry 3304 (class 1259 OID 16795)
-- Name: bsns_request_process_essay_agent_request_process_essay_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_agent_request_process_essay_id_idx ON plims.bsns_request_process_essay_agent USING btree (request_process_essay_id);


--
-- TOC entry 3305 (class 1259 OID 16796)
-- Name: bsns_request_process_essay_agent_workflow_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_agent_workflow_id_idx ON plims.bsns_request_process_essay_agent USING btree (workflow_id);


--
-- TOC entry 3249 (class 1259 OID 16797)
-- Name: bsns_request_process_essay_check_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_check_by_idx ON plims.bsns_request_process_essay USING btree (check_by);


--
-- TOC entry 3250 (class 1259 OID 16798)
-- Name: bsns_request_process_essay_composite_sample_type_id_aux_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_composite_sample_type_id_aux_idx ON plims.bsns_request_process_essay USING btree (request_process_essay_root_id);


--
-- TOC entry 3251 (class 1259 OID 16799)
-- Name: bsns_request_process_essay_composite_sample_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_composite_sample_type_id_idx ON plims.bsns_request_process_essay USING btree (composite_sample_type_id);


--
-- TOC entry 3252 (class 1259 OID 16800)
-- Name: bsns_request_process_essay_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_crop_id_idx ON plims.bsns_request_process_essay USING btree (crop_id);


--
-- TOC entry 3253 (class 1259 OID 16801)
-- Name: bsns_request_process_essay_essay_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_essay_id_idx ON plims.bsns_request_process_essay USING btree (essay_id);


--
-- TOC entry 3254 (class 1259 OID 16802)
-- Name: bsns_request_process_essay_finish_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_finish_by_idx ON plims.bsns_request_process_essay USING btree (finish_by);


--
-- TOC entry 3255 (class 1259 OID 16803)
-- Name: bsns_request_process_essay_request_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_request_id_idx ON plims.bsns_request_process_essay USING btree (request_id);


--
-- TOC entry 3256 (class 1259 OID 16804)
-- Name: bsns_request_process_essay_request_id_num_order_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_request_id_num_order_id_idx ON plims.bsns_request_process_essay USING btree (request_id, num_order_id);


--
-- TOC entry 3257 (class 1259 OID 16805)
-- Name: bsns_request_process_essay_request_process_essay_status_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_request_process_essay_status_id_idx ON plims.bsns_request_process_essay USING btree (request_process_essay_status_id);


--
-- TOC entry 3258 (class 1259 OID 16806)
-- Name: bsns_request_process_essay_request_process_essay_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_request_process_essay_type_id_idx ON plims.bsns_request_process_essay USING btree (request_process_essay_type_id);


--
-- TOC entry 3259 (class 1259 OID 16807)
-- Name: bsns_request_process_essay_start_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_start_by_idx ON plims.bsns_request_process_essay USING btree (start_by);


--
-- TOC entry 3260 (class 1259 OID 16808)
-- Name: bsns_request_process_essay_support_master_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_support_master_id_idx ON plims.bsns_request_process_essay USING btree (support_master_id);


--
-- TOC entry 3261 (class 1259 OID 16809)
-- Name: bsns_request_process_essay_workflow_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_essay_workflow_id_idx ON plims.bsns_request_process_essay USING btree (workflow_id);


--
-- TOC entry 3240 (class 1259 OID 16810)
-- Name: bsns_request_process_finish_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_finish_by_idx ON plims.bsns_request_process USING btree (finish_by);


--
-- TOC entry 3243 (class 1259 OID 16811)
-- Name: bsns_request_process_request_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_request_id_idx ON plims.bsns_request_process USING btree (request_id);


--
-- TOC entry 3244 (class 1259 OID 16812)
-- Name: bsns_request_process_request_process_material_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_request_process_material_id_idx ON plims.bsns_request_process USING btree (request_process_material_id);


--
-- TOC entry 3245 (class 1259 OID 16813)
-- Name: bsns_request_process_request_process_payment_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_request_process_payment_id_idx ON plims.bsns_request_process USING btree (request_process_payment_id);


--
-- TOC entry 3246 (class 1259 OID 16814)
-- Name: bsns_request_process_request_process_status_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_request_process_status_id_idx ON plims.bsns_request_process USING btree (request_process_status_id);


--
-- TOC entry 3247 (class 1259 OID 16815)
-- Name: bsns_request_process_start_by_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_start_by_idx ON plims.bsns_request_process USING btree (start_by);


--
-- TOC entry 3248 (class 1259 OID 16816)
-- Name: bsns_request_process_workflow_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_process_workflow_id_idx ON plims.bsns_request_process USING btree (workflow_id);


--
-- TOC entry 3222 (class 1259 OID 16817)
-- Name: bsns_request_request_payment_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_request_payment_id_idx ON plims.bsns_request USING btree (request_payment_id);


--
-- TOC entry 3223 (class 1259 OID 16818)
-- Name: bsns_request_request_status_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_request_status_id_idx ON plims.bsns_request USING btree (request_status_id);


--
-- TOC entry 3224 (class 1259 OID 16819)
-- Name: bsns_request_request_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_request_type_id_idx ON plims.bsns_request USING btree (request_type_id);


--
-- TOC entry 3225 (class 1259 OID 16820)
-- Name: bsns_request_requested_user_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_request_requested_user_id_idx ON plims.bsns_request USING btree (requested_user_id);


--
-- TOC entry 3310 (class 1259 OID 16821)
-- Name: bsns_support_activity_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_support_activity_id_idx ON plims.bsns_support USING btree (activity_id);


--
-- TOC entry 3311 (class 1259 OID 16822)
-- Name: bsns_support_composite_sample_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_support_composite_sample_type_id_idx ON plims.bsns_support USING btree (composite_sample_type_id);


--
-- TOC entry 3312 (class 1259 OID 16823)
-- Name: bsns_support_crop_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_support_crop_id_idx ON plims.bsns_support USING btree (crop_id);


--
-- TOC entry 3313 (class 1259 OID 16824)
-- Name: bsns_support_essay_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_support_essay_id_idx ON plims.bsns_support USING btree (essay_id);


--
-- TOC entry 3314 (class 1259 OID 16825)
-- Name: bsns_support_request_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_support_request_id_idx ON plims.bsns_support USING btree (request_id);


--
-- TOC entry 3315 (class 1259 OID 16826)
-- Name: bsns_support_request_id_num_order_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_support_request_id_num_order_id_idx ON plims.bsns_support USING btree (request_id, num_order_id);


--
-- TOC entry 3316 (class 1259 OID 16827)
-- Name: bsns_support_request_process_essay_activity_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_support_request_process_essay_activity_id_idx ON plims.bsns_support USING btree (request_process_essay_activity_id);


--
-- TOC entry 3317 (class 1259 OID 16828)
-- Name: bsns_support_support_type_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_support_support_type_id_idx ON plims.bsns_support USING btree (support_type_id);


--
-- TOC entry 3318 (class 1259 OID 16829)
-- Name: bsns_support_workflow_id_idx; Type: INDEX; Schema: plims; Owner: -
--

CREATE INDEX bsns_support_workflow_id_idx ON plims.bsns_support USING btree (workflow_id);


--
-- TOC entry 3187 (class 1259 OID 16830)
-- Name: composite_sample_unique; Type: INDEX; Schema: plims; Owner: -
--

CREATE UNIQUE INDEX composite_sample_unique ON plims.bsns_composite_sample USING btree (composite_sample_type_id, composite_sample_group_id, num_order, reading_data_type_id);


--
-- TOC entry 3452 (class 2606 OID 16831)
-- Name: bsns_request_process_essay_activity_sample activity_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT activity_fk FOREIGN KEY (activity_id) REFERENCES plims.bsns_activity(activity_id);


--
-- TOC entry 3507 (class 2606 OID 61395)
-- Name: auth_task auth_task_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_task
    ADD CONSTRAINT auth_task_fk FOREIGN KEY (role_id) REFERENCES plims.auth_role(role_id);


--
-- TOC entry 3508 (class 2606 OID 61411)
-- Name: auth_user_by_role_institution_laboratory auth_user_by_role_institution_laboratory_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user_by_role_institution_laboratory
    ADD CONSTRAINT auth_user_by_role_institution_laboratory_fk FOREIGN KEY (user_id) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3509 (class 2606 OID 61416)
-- Name: auth_user_by_role_institution_laboratory auth_user_by_role_institution_laboratory_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user_by_role_institution_laboratory
    ADD CONSTRAINT auth_user_by_role_institution_laboratory_fk1 FOREIGN KEY (role_id) REFERENCES plims.auth_role(role_id);


--
-- TOC entry 3510 (class 2606 OID 61421)
-- Name: auth_user_by_role_institution_laboratory auth_user_by_role_institution_laboratory_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user_by_role_institution_laboratory
    ADD CONSTRAINT auth_user_by_role_institution_laboratory_fk2 FOREIGN KEY (institution_id) REFERENCES plims.bsns_institution(institution_id);


--
-- TOC entry 3511 (class 2606 OID 61426)
-- Name: auth_user_by_role_institution_laboratory auth_user_by_role_institution_laboratory_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user_by_role_institution_laboratory
    ADD CONSTRAINT auth_user_by_role_institution_laboratory_fk3 FOREIGN KEY (laboratory_id) REFERENCES plims.bsns_laboratory(laboratory_id);


--
-- TOC entry 3348 (class 2606 OID 16836)
-- Name: bsns_activity_by_essay bsns_activity_by_essay_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_activity_by_essay
    ADD CONSTRAINT bsns_activity_by_essay_fk FOREIGN KEY (activity_id) REFERENCES plims.bsns_activity(activity_id);


--
-- TOC entry 3349 (class 2606 OID 16841)
-- Name: bsns_activity_by_essay bsns_activity_by_essay_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_activity_by_essay
    ADD CONSTRAINT bsns_activity_by_essay_fk1 FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3344 (class 2606 OID 16846)
-- Name: bsns_activity bsns_activity_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_activity
    ADD CONSTRAINT bsns_activity_fk FOREIGN KEY (activity_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3345 (class 2606 OID 16851)
-- Name: bsns_activity bsns_activity_fk_1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_activity
    ADD CONSTRAINT bsns_activity_fk_1 FOREIGN KEY (activity_datasource_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3346 (class 2606 OID 16856)
-- Name: bsns_activity bsns_activity_fk_2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_activity
    ADD CONSTRAINT bsns_activity_fk_2 FOREIGN KEY (activity_property_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3347 (class 2606 OID 16861)
-- Name: bsns_activity bsns_activity_fk_3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_activity
    ADD CONSTRAINT bsns_activity_fk_3 FOREIGN KEY (activity_group_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3353 (class 2606 OID 16866)
-- Name: bsns_agent_condition bsns_agent_condition_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_condition
    ADD CONSTRAINT bsns_agent_condition_fk FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3354 (class 2606 OID 16871)
-- Name: bsns_agent_condition bsns_agent_condition_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_condition
    ADD CONSTRAINT bsns_agent_condition_fk1 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3355 (class 2606 OID 16876)
-- Name: bsns_agent_condition bsns_agent_condition_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_condition
    ADD CONSTRAINT bsns_agent_condition_fk2 FOREIGN KEY (request_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3356 (class 2606 OID 16881)
-- Name: bsns_agent_condition bsns_agent_condition_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_condition
    ADD CONSTRAINT bsns_agent_condition_fk3 FOREIGN KEY (composite_sample_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3357 (class 2606 OID 16886)
-- Name: bsns_agent_condition bsns_agent_condition_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_condition
    ADD CONSTRAINT bsns_agent_condition_fk4 FOREIGN KEY (request_process_essay_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3358 (class 2606 OID 16891)
-- Name: bsns_agent_condition bsns_agent_condition_fk5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_condition
    ADD CONSTRAINT bsns_agent_condition_fk5 FOREIGN KEY (agent_id) REFERENCES plims.bsns_agent(agent_id);


--
-- TOC entry 3352 (class 2606 OID 61438)
-- Name: bsns_agent_condition bsns_agent_condition_fk6; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_condition
    ADD CONSTRAINT bsns_agent_condition_fk6 FOREIGN KEY (laboratory_id) REFERENCES plims.bsns_laboratory(laboratory_id);


--
-- TOC entry 3350 (class 2606 OID 16896)
-- Name: bsns_agent bsns_agent_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent
    ADD CONSTRAINT bsns_agent_fk FOREIGN KEY (agent_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3490 (class 2606 OID 17552)
-- Name: bsns_multi_request_event bsns_agent_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_multi_request_event
    ADD CONSTRAINT bsns_agent_fk FOREIGN KEY (agent_id) REFERENCES plims.bsns_agent(agent_id);


--
-- TOC entry 3351 (class 2606 OID 16901)
-- Name: bsns_agent bsns_agent_fk_1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent
    ADD CONSTRAINT bsns_agent_fk_1 FOREIGN KEY (agent_group_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3359 (class 2606 OID 16906)
-- Name: bsns_composite_sample bsns_composite_sample_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_composite_sample
    ADD CONSTRAINT bsns_composite_sample_fk FOREIGN KEY (composite_sample_group_id) REFERENCES plims.bsns_composite_sample_group(composite_sample_group_id);


--
-- TOC entry 3360 (class 2606 OID 16911)
-- Name: bsns_composite_sample bsns_composite_sample_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_composite_sample
    ADD CONSTRAINT bsns_composite_sample_fk1 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3361 (class 2606 OID 16916)
-- Name: bsns_composite_sample bsns_composite_sample_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_composite_sample
    ADD CONSTRAINT bsns_composite_sample_fk2 FOREIGN KEY (composite_sample_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3362 (class 2606 OID 16921)
-- Name: bsns_composite_sample bsns_composite_sample_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_composite_sample
    ADD CONSTRAINT bsns_composite_sample_fk3 FOREIGN KEY (reading_data_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3497 (class 2606 OID 44922)
-- Name: bsns_consolidated_information bsns_consolidated_information_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_information
    ADD CONSTRAINT bsns_consolidated_information_fk1 FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3498 (class 2606 OID 44927)
-- Name: bsns_consolidated_information bsns_consolidated_information_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_information
    ADD CONSTRAINT bsns_consolidated_information_fk2 FOREIGN KEY (general_sample_id) REFERENCES plims.bsns_composite_sample(composite_sample_id);


--
-- TOC entry 3499 (class 2606 OID 44932)
-- Name: bsns_consolidated_information bsns_consolidated_information_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_information
    ADD CONSTRAINT bsns_consolidated_information_fk3 FOREIGN KEY (simple_sample_id) REFERENCES plims.bsns_composite_sample(composite_sample_id);


--
-- TOC entry 3500 (class 2606 OID 44937)
-- Name: bsns_consolidated_information bsns_consolidated_information_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_information
    ADD CONSTRAINT bsns_consolidated_information_fk4 FOREIGN KEY (indiviual_sample_id) REFERENCES plims.bsns_composite_sample(composite_sample_id);


--
-- TOC entry 3501 (class 2606 OID 44942)
-- Name: bsns_consolidated_information bsns_consolidated_information_fk5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_information
    ADD CONSTRAINT bsns_consolidated_information_fk5 FOREIGN KEY (release_status_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3502 (class 2606 OID 45107)
-- Name: bsns_consolidated_result bsns_consolidated_result_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_result
    ADD CONSTRAINT bsns_consolidated_result_fk1 FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3503 (class 2606 OID 45112)
-- Name: bsns_consolidated_result bsns_consolidated_result_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_result
    ADD CONSTRAINT bsns_consolidated_result_fk2 FOREIGN KEY (composite_sample_id) REFERENCES plims.bsns_composite_sample(composite_sample_id);


--
-- TOC entry 3504 (class 2606 OID 45117)
-- Name: bsns_consolidated_result bsns_consolidated_result_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_result
    ADD CONSTRAINT bsns_consolidated_result_fk3 FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3505 (class 2606 OID 45122)
-- Name: bsns_consolidated_result bsns_consolidated_result_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_consolidated_result
    ADD CONSTRAINT bsns_consolidated_result_fk4 FOREIGN KEY (agent_id) REFERENCES plims.bsns_agent(agent_id);


--
-- TOC entry 3364 (class 2606 OID 16926)
-- Name: bsns_cost bsns_cost_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_cost
    ADD CONSTRAINT bsns_cost_fk FOREIGN KEY (cost_currency_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3368 (class 2606 OID 16931)
-- Name: bsns_essay_by_workflow bsns_essay_by_workflow_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_essay_by_workflow
    ADD CONSTRAINT bsns_essay_by_workflow_fk FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3369 (class 2606 OID 16936)
-- Name: bsns_essay_by_workflow bsns_essay_by_workflow_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_essay_by_workflow
    ADD CONSTRAINT bsns_essay_by_workflow_fk1 FOREIGN KEY (workflow_id) REFERENCES plims.bsns_workflow(workflow_id);


--
-- TOC entry 3365 (class 2606 OID 16941)
-- Name: bsns_essay bsns_essay_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_essay
    ADD CONSTRAINT bsns_essay_fk FOREIGN KEY (essay_cost_id) REFERENCES plims.bsns_cost(cost_id);


--
-- TOC entry 3366 (class 2606 OID 16946)
-- Name: bsns_essay bsns_essay_fk_1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_essay
    ADD CONSTRAINT bsns_essay_fk_1 FOREIGN KEY (essay_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3367 (class 2606 OID 16951)
-- Name: bsns_essay bsns_essay_fk_2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_essay
    ADD CONSTRAINT bsns_essay_fk_2 FOREIGN KEY (essay_location_id) REFERENCES plims.bsns_location(location_id);


--
-- TOC entry 3506 (class 2606 OID 61172)
-- Name: bsns_laboratory bsns_laboratory_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_laboratory
    ADD CONSTRAINT bsns_laboratory_fk FOREIGN KEY (institution_id) REFERENCES plims.bsns_institution(institution_id);


--
-- TOC entry 3370 (class 2606 OID 16956)
-- Name: bsns_material bsns_material_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT bsns_material_fk FOREIGN KEY (composite_sample_id) REFERENCES plims.bsns_composite_sample(composite_sample_id);


--
-- TOC entry 3371 (class 2606 OID 16961)
-- Name: bsns_material bsns_material_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT bsns_material_fk1 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3372 (class 2606 OID 16966)
-- Name: bsns_material bsns_material_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT bsns_material_fk2 FOREIGN KEY (composite_sample_group_id) REFERENCES plims.bsns_composite_sample_group(composite_sample_group_id);


--
-- TOC entry 3373 (class 2606 OID 16971)
-- Name: bsns_material bsns_material_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT bsns_material_fk3 FOREIGN KEY (material_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3374 (class 2606 OID 16976)
-- Name: bsns_material bsns_material_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT bsns_material_fk4 FOREIGN KEY (material_unit_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3375 (class 2606 OID 16981)
-- Name: bsns_material bsns_material_fk5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT bsns_material_fk5 FOREIGN KEY (composite_sample_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3380 (class 2606 OID 61433)
-- Name: bsns_parameter bsns_parameter_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_parameter
    ADD CONSTRAINT bsns_parameter_fk FOREIGN KEY (institution_id) REFERENCES plims.bsns_institution(institution_id);


--
-- TOC entry 3386 (class 2606 OID 16986)
-- Name: bsns_reading_data bsns_reading_data_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3387 (class 2606 OID 16991)
-- Name: bsns_reading_data bsns_reading_data_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk1 FOREIGN KEY (request_id, num_order_id) REFERENCES plims.bsns_request_process(request_id, num_order_id);


--
-- TOC entry 3381 (class 2606 OID 16996)
-- Name: bsns_reading_data bsns_reading_data_fk10; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk10 FOREIGN KEY (workflow_id) REFERENCES plims.bsns_workflow(workflow_id);


--
-- TOC entry 3382 (class 2606 OID 17001)
-- Name: bsns_reading_data bsns_reading_data_fk11; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk11 FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3383 (class 2606 OID 17006)
-- Name: bsns_reading_data bsns_reading_data_fk12; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk12 FOREIGN KEY (activity_id) REFERENCES plims.bsns_activity(activity_id);


--
-- TOC entry 3384 (class 2606 OID 17011)
-- Name: bsns_reading_data bsns_reading_data_fk13; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk13 FOREIGN KEY (agent_id) REFERENCES plims.bsns_agent(agent_id);


--
-- TOC entry 3385 (class 2606 OID 17016)
-- Name: bsns_reading_data bsns_reading_data_fk14; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk14 FOREIGN KEY (symptom_id) REFERENCES plims.bsns_symptom(symptom_id);


--
-- TOC entry 3388 (class 2606 OID 17021)
-- Name: bsns_reading_data bsns_reading_data_fk15; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk15 FOREIGN KEY (reading_data_section_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3389 (class 2606 OID 17026)
-- Name: bsns_reading_data bsns_reading_data_fk16; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk16 FOREIGN KEY (reading_data_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3390 (class 2606 OID 17031)
-- Name: bsns_reading_data bsns_reading_data_fk17; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk17 FOREIGN KEY (composite_sample_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3391 (class 2606 OID 17036)
-- Name: bsns_reading_data bsns_reading_data_fk18; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk18 FOREIGN KEY (support_master_id) REFERENCES plims.bsns_support_master(support_master_id);


--
-- TOC entry 3392 (class 2606 OID 17041)
-- Name: bsns_reading_data bsns_reading_data_fk6; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk6 FOREIGN KEY (composite_sample_group_id) REFERENCES plims.bsns_composite_sample_group(composite_sample_group_id);


--
-- TOC entry 3393 (class 2606 OID 17046)
-- Name: bsns_reading_data bsns_reading_data_fk9; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT bsns_reading_data_fk9 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3406 (class 2606 OID 17051)
-- Name: bsns_request_detail bsns_request_detail_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_detail
    ADD CONSTRAINT bsns_request_detail_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3407 (class 2606 OID 17056)
-- Name: bsns_request_detail bsns_request_detail_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_detail
    ADD CONSTRAINT bsns_request_detail_fk1 FOREIGN KEY (requested_user) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3408 (class 2606 OID 17061)
-- Name: bsns_request_detail bsns_request_detail_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_detail
    ADD CONSTRAINT bsns_request_detail_fk2 FOREIGN KEY (consignee_user) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3409 (class 2606 OID 17066)
-- Name: bsns_request_detail bsns_request_detail_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_detail
    ADD CONSTRAINT bsns_request_detail_fk3 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3410 (class 2606 OID 17071)
-- Name: bsns_request_documentation bsns_request_documentation_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_documentation
    ADD CONSTRAINT bsns_request_documentation_fk1 FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3411 (class 2606 OID 17076)
-- Name: bsns_request_documentation bsns_request_documentation_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_documentation
    ADD CONSTRAINT bsns_request_documentation_fk2 FOREIGN KEY (file_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3412 (class 2606 OID 17081)
-- Name: bsns_request_documentation bsns_request_documentation_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_documentation
    ADD CONSTRAINT bsns_request_documentation_fk3 FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3413 (class 2606 OID 22399)
-- Name: bsns_request_documentation bsns_request_documentation_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_documentation
    ADD CONSTRAINT bsns_request_documentation_fk4 FOREIGN KEY (support_master_id) REFERENCES plims.bsns_support_master(support_master_id);


--
-- TOC entry 3414 (class 2606 OID 22404)
-- Name: bsns_request_documentation bsns_request_documentation_fk5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_documentation
    ADD CONSTRAINT bsns_request_documentation_fk5 FOREIGN KEY (activity_id) REFERENCES plims.bsns_activity(activity_id);


--
-- TOC entry 3401 (class 2606 OID 17086)
-- Name: bsns_request bsns_request_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request
    ADD CONSTRAINT bsns_request_fk FOREIGN KEY (requested_user_id) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3491 (class 2606 OID 17547)
-- Name: bsns_multi_request_event bsns_request_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_multi_request_event
    ADD CONSTRAINT bsns_request_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3402 (class 2606 OID 17091)
-- Name: bsns_request bsns_request_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request
    ADD CONSTRAINT bsns_request_fk1 FOREIGN KEY (owner_user_id) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3403 (class 2606 OID 17096)
-- Name: bsns_request bsns_request_fk_2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request
    ADD CONSTRAINT bsns_request_fk_2 FOREIGN KEY (request_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3404 (class 2606 OID 17101)
-- Name: bsns_request bsns_request_fk_3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request
    ADD CONSTRAINT bsns_request_fk_3 FOREIGN KEY (request_status_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3405 (class 2606 OID 17106)
-- Name: bsns_request bsns_request_fk_4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request
    ADD CONSTRAINT bsns_request_fk_4 FOREIGN KEY (request_payment_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3399 (class 2606 OID 61443)
-- Name: bsns_request bsns_request_fk_5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request
    ADD CONSTRAINT bsns_request_fk_5 FOREIGN KEY (laboratory_id) REFERENCES plims.bsns_laboratory(laboratory_id);


--
-- TOC entry 3400 (class 2606 OID 61448)
-- Name: bsns_request bsns_request_fk_6; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request
    ADD CONSTRAINT bsns_request_fk_6 FOREIGN KEY (institution_id) REFERENCES plims.bsns_institution(institution_id);


--
-- TOC entry 3437 (class 2606 OID 17111)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3438 (class 2606 OID 17116)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk1 FOREIGN KEY (request_id, num_order_id) REFERENCES plims.bsns_request_process(request_id, num_order_id);


--
-- TOC entry 3439 (class 2606 OID 17121)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk10; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk10 FOREIGN KEY (essay_id, activity_id) REFERENCES plims.bsns_activity_by_essay(essay_id, activity_id);


--
-- TOC entry 3440 (class 2606 OID 17126)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk11; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk11 FOREIGN KEY (composite_sample_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3441 (class 2606 OID 17131)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk3 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3442 (class 2606 OID 17136)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk4 FOREIGN KEY (workflow_id) REFERENCES plims.bsns_workflow(workflow_id);


--
-- TOC entry 3443 (class 2606 OID 17141)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk5 FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3444 (class 2606 OID 17146)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk6; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk6 FOREIGN KEY (activity_id) REFERENCES plims.bsns_activity(activity_id);


--
-- TOC entry 3445 (class 2606 OID 17151)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk7; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk7 FOREIGN KEY (control_location_id) REFERENCES plims.bsns_location(location_id);


--
-- TOC entry 3446 (class 2606 OID 17156)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk8; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk8 FOREIGN KEY (control_user_id) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3447 (class 2606 OID 17161)
-- Name: bsns_request_process_essay_activity bsns_request_process_essay_activity_fk9; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT bsns_request_process_essay_activity_fk9 FOREIGN KEY (request_process_essay_activity_status_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3467 (class 2606 OID 17166)
-- Name: bsns_request_process_essay_agent bsns_request_process_essay_agent_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_agent
    ADD CONSTRAINT bsns_request_process_essay_agent_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3468 (class 2606 OID 17171)
-- Name: bsns_request_process_essay_agent bsns_request_process_essay_agent_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_agent
    ADD CONSTRAINT bsns_request_process_essay_agent_fk1 FOREIGN KEY (request_id, num_order_id) REFERENCES plims.bsns_request_process(request_id, num_order_id);


--
-- TOC entry 3469 (class 2606 OID 17176)
-- Name: bsns_request_process_essay_agent bsns_request_process_essay_agent_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_agent
    ADD CONSTRAINT bsns_request_process_essay_agent_fk3 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3470 (class 2606 OID 17181)
-- Name: bsns_request_process_essay_agent bsns_request_process_essay_agent_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_agent
    ADD CONSTRAINT bsns_request_process_essay_agent_fk4 FOREIGN KEY (workflow_id) REFERENCES plims.bsns_workflow(workflow_id);


--
-- TOC entry 3471 (class 2606 OID 17186)
-- Name: bsns_request_process_essay_agent bsns_request_process_essay_agent_fk5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_agent
    ADD CONSTRAINT bsns_request_process_essay_agent_fk5 FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3472 (class 2606 OID 17191)
-- Name: bsns_request_process_essay_agent bsns_request_process_essay_agent_fk6; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_agent
    ADD CONSTRAINT bsns_request_process_essay_agent_fk6 FOREIGN KEY (agent_id) REFERENCES plims.bsns_agent(agent_id);


--
-- TOC entry 3473 (class 2606 OID 17196)
-- Name: bsns_request_process_essay_agent bsns_request_process_essay_agent_fk7; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_agent
    ADD CONSTRAINT bsns_request_process_essay_agent_fk7 FOREIGN KEY (composite_sample_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3425 (class 2606 OID 17201)
-- Name: bsns_request_process_essay bsns_request_process_essay_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT bsns_request_process_essay_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3453 (class 2606 OID 17206)
-- Name: bsns_request_process_essay_activity_sample bsns_request_process_essay_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT bsns_request_process_essay_fk FOREIGN KEY (request_process_essay_id) REFERENCES plims.bsns_request_process_essay(request_process_essay_id);


--
-- TOC entry 3426 (class 2606 OID 17211)
-- Name: bsns_request_process_essay bsns_request_process_essay_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT bsns_request_process_essay_fk1 FOREIGN KEY (request_id, num_order_id) REFERENCES plims.bsns_request_process(request_id, num_order_id);


--
-- TOC entry 3427 (class 2606 OID 17216)
-- Name: bsns_request_process_essay bsns_request_process_essay_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT bsns_request_process_essay_fk2 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3428 (class 2606 OID 17221)
-- Name: bsns_request_process_essay bsns_request_process_essay_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT bsns_request_process_essay_fk3 FOREIGN KEY (workflow_id) REFERENCES plims.bsns_workflow(workflow_id);


--
-- TOC entry 3429 (class 2606 OID 17226)
-- Name: bsns_request_process_essay bsns_request_process_essay_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT bsns_request_process_essay_fk4 FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3430 (class 2606 OID 17231)
-- Name: bsns_request_process_essay bsns_request_process_essay_fk5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT bsns_request_process_essay_fk5 FOREIGN KEY (composite_sample_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3431 (class 2606 OID 17236)
-- Name: bsns_request_process_essay bsns_request_process_essay_fk6; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT bsns_request_process_essay_fk6 FOREIGN KEY (request_process_essay_status_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3432 (class 2606 OID 17241)
-- Name: bsns_request_process_essay bsns_request_process_essay_fk7; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT bsns_request_process_essay_fk7 FOREIGN KEY (request_process_essay_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3433 (class 2606 OID 17251)
-- Name: bsns_request_process_essay bsns_request_process_essay_fk9; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT bsns_request_process_essay_fk9 FOREIGN KEY (support_master_id) REFERENCES plims.bsns_support_master(support_master_id);


--
-- TOC entry 3415 (class 2606 OID 17256)
-- Name: bsns_request_process bsns_request_process_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT bsns_request_process_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3416 (class 2606 OID 17261)
-- Name: bsns_request_process bsns_request_process_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT bsns_request_process_fk1 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3417 (class 2606 OID 17266)
-- Name: bsns_request_process bsns_request_process_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT bsns_request_process_fk2 FOREIGN KEY (workflow_id) REFERENCES plims.bsns_workflow(workflow_id);


--
-- TOC entry 3418 (class 2606 OID 17271)
-- Name: bsns_request_process bsns_request_process_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT bsns_request_process_fk3 FOREIGN KEY (composite_sample_group_id) REFERENCES plims.bsns_composite_sample_group(composite_sample_group_id);


--
-- TOC entry 3419 (class 2606 OID 17276)
-- Name: bsns_request_process bsns_request_process_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT bsns_request_process_fk4 FOREIGN KEY (request_process_status_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3420 (class 2606 OID 17281)
-- Name: bsns_request_process bsns_request_process_fk5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT bsns_request_process_fk5 FOREIGN KEY (request_process_material_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3421 (class 2606 OID 17286)
-- Name: bsns_request_process bsns_request_process_fk6; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT bsns_request_process_fk6 FOREIGN KEY (request_process_payment_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3494 (class 2606 OID 44773)
-- Name: bsns_agent_sample_reviewer bsns_sample_by_agent_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_sample_reviewer
    ADD CONSTRAINT bsns_sample_by_agent_fk1 FOREIGN KEY (composite_sample_id) REFERENCES plims.bsns_composite_sample(composite_sample_id);


--
-- TOC entry 3495 (class 2606 OID 44778)
-- Name: bsns_agent_sample_reviewer bsns_sample_by_agent_fk2; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_sample_reviewer
    ADD CONSTRAINT bsns_sample_by_agent_fk2 FOREIGN KEY (agent_id) REFERENCES plims.bsns_agent(agent_id);


--
-- TOC entry 3496 (class 2606 OID 44783)
-- Name: bsns_agent_sample_reviewer bsns_sample_by_agent_fk3; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_agent_sample_reviewer
    ADD CONSTRAINT bsns_sample_by_agent_fk3 FOREIGN KEY (request_process_essay_id) REFERENCES plims.bsns_request_process_essay(request_process_essay_id);


--
-- TOC entry 3478 (class 2606 OID 17291)
-- Name: bsns_support bsns_support_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT bsns_support_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3479 (class 2606 OID 17296)
-- Name: bsns_support bsns_support_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT bsns_support_fk1 FOREIGN KEY (request_id, num_order_id) REFERENCES plims.bsns_request_process(request_id, num_order_id);


--
-- TOC entry 3480 (class 2606 OID 17301)
-- Name: bsns_support bsns_support_fk4; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT bsns_support_fk4 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3481 (class 2606 OID 17306)
-- Name: bsns_support bsns_support_fk5; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT bsns_support_fk5 FOREIGN KEY (workflow_id) REFERENCES plims.bsns_workflow(workflow_id);


--
-- TOC entry 3482 (class 2606 OID 17311)
-- Name: bsns_support bsns_support_fk6; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT bsns_support_fk6 FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3483 (class 2606 OID 17316)
-- Name: bsns_support bsns_support_fk7; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT bsns_support_fk7 FOREIGN KEY (activity_id) REFERENCES plims.bsns_activity(activity_id);


--
-- TOC entry 3484 (class 2606 OID 17321)
-- Name: bsns_support bsns_support_fk8; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT bsns_support_fk8 FOREIGN KEY (support_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3485 (class 2606 OID 17326)
-- Name: bsns_support bsns_support_fk9; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT bsns_support_fk9 FOREIGN KEY (composite_sample_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3492 (class 2606 OID 17542)
-- Name: bsns_multi_request_event bsns_support_master_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_multi_request_event
    ADD CONSTRAINT bsns_support_master_fk FOREIGN KEY (support_master_id) REFERENCES plims.bsns_support_master(support_master_id);


--
-- TOC entry 3488 (class 2606 OID 17331)
-- Name: bsns_workflow_by_crop bsns_workflow_by_crop_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_workflow_by_crop
    ADD CONSTRAINT bsns_workflow_by_crop_fk FOREIGN KEY (workflow_id) REFERENCES plims.bsns_workflow(workflow_id);


--
-- TOC entry 3489 (class 2606 OID 17336)
-- Name: bsns_workflow_by_crop bsns_workflow_by_crop_fk1; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_workflow_by_crop
    ADD CONSTRAINT bsns_workflow_by_crop_fk1 FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3394 (class 2606 OID 17341)
-- Name: bsns_reading_data business_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT business_fk FOREIGN KEY (support_id) REFERENCES plims.bsns_support(support_id);


--
-- TOC entry 3422 (class 2606 OID 17346)
-- Name: bsns_request_process check_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT check_by_fk FOREIGN KEY (check_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3434 (class 2606 OID 17351)
-- Name: bsns_request_process_essay check_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT check_by_fk FOREIGN KEY (check_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3448 (class 2606 OID 17356)
-- Name: bsns_request_process_essay_activity check_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT check_by_fk FOREIGN KEY (check_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3454 (class 2606 OID 17361)
-- Name: bsns_request_process_essay_activity_sample check_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT check_by_fk FOREIGN KEY (check_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3455 (class 2606 OID 17366)
-- Name: bsns_request_process_essay_activity_sample composite_sample_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT composite_sample_fk FOREIGN KEY (composite_sample_id) REFERENCES plims.bsns_composite_sample(composite_sample_id);


--
-- TOC entry 3379 (class 2606 OID 17371)
-- Name: bsns_material_group composite_sample_group_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material_group
    ADD CONSTRAINT composite_sample_group_fk FOREIGN KEY (composite_sample_group_id) REFERENCES plims.bsns_composite_sample_group(composite_sample_group_id);


--
-- TOC entry 3456 (class 2606 OID 17376)
-- Name: bsns_request_process_essay_activity_sample composite_sample_group_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT composite_sample_group_fk FOREIGN KEY (composite_sample_group_id) REFERENCES plims.bsns_composite_sample_group(composite_sample_group_id);


--
-- TOC entry 3457 (class 2606 OID 17381)
-- Name: bsns_request_process_essay_activity_sample composite_sample_type_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT composite_sample_type_fk FOREIGN KEY (composite_sample_type_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3458 (class 2606 OID 17386)
-- Name: bsns_request_process_essay_activity_sample crop_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT crop_fk FOREIGN KEY (crop_id) REFERENCES plims.bsns_crop(crop_id);


--
-- TOC entry 3459 (class 2606 OID 17391)
-- Name: bsns_request_process_essay_activity_sample essay_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT essay_fk FOREIGN KEY (essay_id) REFERENCES plims.bsns_essay(essay_id);


--
-- TOC entry 3423 (class 2606 OID 17396)
-- Name: bsns_request_process finish_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT finish_by_fk FOREIGN KEY (finish_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3435 (class 2606 OID 17401)
-- Name: bsns_request_process_essay finish_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT finish_by_fk FOREIGN KEY (finish_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3449 (class 2606 OID 17406)
-- Name: bsns_request_process_essay_activity finish_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT finish_by_fk FOREIGN KEY (finish_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3460 (class 2606 OID 17411)
-- Name: bsns_request_process_essay_activity_sample finish_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT finish_by_fk FOREIGN KEY (finish_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3486 (class 2606 OID 42604)
-- Name: bsns_support fk_request_process_essay; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT fk_request_process_essay FOREIGN KEY (request_process_essay_id) REFERENCES plims.bsns_request_process_essay(request_process_essay_id);


--
-- TOC entry 3493 (class 2606 OID 42617)
-- Name: bsns_multi_request_event fk_request_process_essay_status_id; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_multi_request_event
    ADD CONSTRAINT fk_request_process_essay_status_id FOREIGN KEY (request_process_essay_status_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3376 (class 2606 OID 17416)
-- Name: bsns_material material_group_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT material_group_fk FOREIGN KEY (material_group_id) REFERENCES plims.bsns_material_group(material_group_id);


--
-- TOC entry 3363 (class 2606 OID 17421)
-- Name: bsns_composite_sample material_group_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_composite_sample
    ADD CONSTRAINT material_group_fk FOREIGN KEY (material_group_id) REFERENCES plims.bsns_material_group(material_group_id);


--
-- TOC entry 3377 (class 2606 OID 17426)
-- Name: bsns_material material_registered_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT material_registered_fk FOREIGN KEY (registered_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3378 (class 2606 OID 17431)
-- Name: bsns_material material_status_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_material
    ADD CONSTRAINT material_status_fk FOREIGN KEY (material_status_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3475 (class 2606 OID 17436)
-- Name: bsns_status request_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_status
    ADD CONSTRAINT request_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3461 (class 2606 OID 17441)
-- Name: bsns_request_process_essay_activity_sample request_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT request_fk FOREIGN KEY (request_id) REFERENCES plims.bsns_request(request_id);


--
-- TOC entry 3474 (class 2606 OID 17446)
-- Name: bsns_request_process_essay_agent request_process_assay_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_agent
    ADD CONSTRAINT request_process_assay_fk FOREIGN KEY (request_process_essay_id) REFERENCES plims.bsns_request_process_essay(request_process_essay_id);


--
-- TOC entry 3395 (class 2606 OID 17451)
-- Name: bsns_reading_data request_process_assay_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT request_process_assay_fk FOREIGN KEY (request_process_essay_id) REFERENCES plims.bsns_request_process_essay(request_process_essay_id);


--
-- TOC entry 3450 (class 2606 OID 17456)
-- Name: bsns_request_process_essay_activity request_process_assay_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT request_process_assay_fk FOREIGN KEY (request_process_essay_id) REFERENCES plims.bsns_request_process_essay(request_process_essay_id);


--
-- TOC entry 3396 (class 2606 OID 17461)
-- Name: bsns_reading_data request_process_essay_activity_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT request_process_essay_activity_fk FOREIGN KEY (request_process_essay_activity_id) REFERENCES plims.bsns_request_process_essay_activity(request_process_essay_activity_id);


--
-- TOC entry 3487 (class 2606 OID 17466)
-- Name: bsns_support request_process_essay_activity_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_support
    ADD CONSTRAINT request_process_essay_activity_fk FOREIGN KEY (request_process_essay_activity_id) REFERENCES plims.bsns_request_process_essay_activity(request_process_essay_activity_id);


--
-- TOC entry 3462 (class 2606 OID 17471)
-- Name: bsns_request_process_essay_activity_sample request_process_essay_activity_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT request_process_essay_activity_fk FOREIGN KEY (request_process_essay_activity_id) REFERENCES plims.bsns_request_process_essay_activity(request_process_essay_activity_id);


--
-- TOC entry 3397 (class 2606 OID 17476)
-- Name: bsns_reading_data request_process_essay_agent_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT request_process_essay_agent_fk FOREIGN KEY (request_process_essay_agent_id) REFERENCES plims.bsns_request_process_essay_agent(request_process_essay_agent_id);


--
-- TOC entry 3398 (class 2606 OID 17481)
-- Name: bsns_reading_data request_process_essay_sample_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_reading_data
    ADD CONSTRAINT request_process_essay_sample_fk FOREIGN KEY (request_process_essay_activity_sample_id) REFERENCES plims.bsns_request_process_essay_activity_sample(request_process_essay_activity_sample_id);


--
-- TOC entry 3463 (class 2606 OID 17486)
-- Name: bsns_request_process_essay_activity_sample request_process_essay_sample_status_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT request_process_essay_sample_status_fk FOREIGN KEY (request_process_essay_activity_sample_status_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3464 (class 2606 OID 17491)
-- Name: bsns_request_process_essay_activity_sample request_process_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT request_process_fk FOREIGN KEY (request_id, num_order_id) REFERENCES plims.bsns_request_process(request_id, num_order_id);


--
-- TOC entry 3476 (class 2606 OID 17496)
-- Name: bsns_status request_status_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_status
    ADD CONSTRAINT request_status_fk FOREIGN KEY (request_status_id) REFERENCES plims.bsns_parameter(parameter_id);


--
-- TOC entry 3424 (class 2606 OID 17501)
-- Name: bsns_request_process start_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process
    ADD CONSTRAINT start_by_fk FOREIGN KEY (start_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3436 (class 2606 OID 17506)
-- Name: bsns_request_process_essay start_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay
    ADD CONSTRAINT start_by_fk FOREIGN KEY (start_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3451 (class 2606 OID 17511)
-- Name: bsns_request_process_essay_activity start_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity
    ADD CONSTRAINT start_by_fk FOREIGN KEY (start_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3465 (class 2606 OID 17516)
-- Name: bsns_request_process_essay_activity_sample start_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT start_by_fk FOREIGN KEY (start_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3477 (class 2606 OID 17521)
-- Name: bsns_status status_by_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_status
    ADD CONSTRAINT status_by_fk FOREIGN KEY (status_by) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3343 (class 2606 OID 17526)
-- Name: auth_user_refresh_tokens user_refresh_tokens_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.auth_user_refresh_tokens
    ADD CONSTRAINT user_refresh_tokens_fk FOREIGN KEY (urf_userid) REFERENCES plims.auth_user(user_id);


--
-- TOC entry 3466 (class 2606 OID 17531)
-- Name: bsns_request_process_essay_activity_sample workflow_fk; Type: FK CONSTRAINT; Schema: plims; Owner: -
--

ALTER TABLE ONLY plims.bsns_request_process_essay_activity_sample
    ADD CONSTRAINT workflow_fk FOREIGN KEY (workflow_id) REFERENCES plims.bsns_workflow(workflow_id);


-- Completed on 2023-03-14 12:57:07

--
-- PostgreSQL database dump complete
--
