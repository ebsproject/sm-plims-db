--liquibase formatted sql

--changeset postgres:create_public.log_scripts_id_seq context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-4267 PLIMS-DB: Add required sequence public.log_scripts_id_seq



CREATE SEQUENCE public.log_scripts_id_seq
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;



--rollback DROP
--rollback SEQUENCE
--rollback DROP COLUMN
--rollback public.log_scripts_id_seq