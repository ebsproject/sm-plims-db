#!/usr/bin/env bash
#NOTE: This is the default EBS DB credentials for automation's purposes. It is implied that in production systems, it will be changed immediately after deployment.
sudo -u postgres psql -c "create user ebsuser with superuser password '3nt3rpr1SE!' valid until 'infinity';"
