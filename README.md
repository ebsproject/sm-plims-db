# PLIMS

# Service Management Database

This is the database module of the Service Management Applications to PLIMS.

#### Dockerfile and config.sh
Contains all the containerization steps. The config.sh is the entrypoint and you'll find all the database provisioning in there.

#### Build/liquibase directory
Contains all the liquibase scripts and binaries.

#### Build/util
Contains all utility scripts to make automation easier

## Using this container

Usage can be classified into two types: database development and general usage. The following environment variables/parameters are available (shown below with their respective default values) and can be set during `docker run` invocation:

```bash
postgres_local_auth_method=md5
postgres_host_auth_method=md5
postgres_listen_address=*
db_user=ebsuser
db_pass=3nt3rpr1SE!
db_name=sm_plims_db
pg_driver=postgresql-42.2.10.jar
lq_contexts=general,schema,template
lq_labels=clean
```

### Using Docker Run (more complex but more flexibility)

**Steps**

* Make sure your repository is up to date with remote (ie. `git pull --all`)
* Write your code, ex. for liquibase, make sure the SQL files are in build/liquibase/changesets directory and specified in a changelog XML (see [Database Management Guideline](https://ebsproject.atlassian.net/wiki/spaces/DB/pages/104235022/Database+Change+Management))
* Build the image. Make sure you are in the root directory of this repository, then run

```bash  
docker build --force-rm=true -t sm-plims-db .
```

* If the build succeeds, you should now have the docker image locally. You can then start and initialize the container.
	* Do not persist data (whenever container is removed via `docker rm`, the data goes away with it): 
	```bash
	docker run --detach --name sm-plims-db -h sm-plims-db -p 5432:5432 --health-cmd="pg_isready -U postgres || exit 1" -it sm-plims-db:latest
	```

* Wait a minute or two. Feel free to check the status of the schema migration via `docker logs sm-plims-db`.
* You now have a running Postgres 13 on port 5432 with all the latest changes. You can either connect to it to port 5432 from outside the container, or go inside the container and check via psql

```bash
docker exec -ti sm-plims-db bash
su - postgres
psql
```